Med erfaring fra høydekryptering1 kjenner vi algoritmen for Hill Cipher både for kryptering og dekryptering.

Den har en svakhet; hvis man kjenner mange nok par med klartekst-chiffertekst så er det mulig å regne ut nøkkelen `K`. Dette heter et kjent klartekstangrep (known plaintext attack).

Vi får oppgitt parvis `C_i = P_i*K (mod N)`. Det at disse parene ikke er sekventsielt etter hverandre har ikke noe å si, vi kan sette de sammen så vi får en plaintekst med korresponderende shiffertekst:
```
P = P_0 || P_1 || .. || P_5
C = C_0 || C_1 || .. || C_5
```

Nå kan vi utlede nøkkelen `K`:
```
  C = P*K    (mod N)

# snur om på ligningen
P*K = C      (mod N)

# multipliserer med P^-1 på begge sider
P*K*P^1 = C*P^1 (mod N)
  K = C*P^-1 (mod N)
```

Nå ser vi at vi finner `K` ved å multiplisere shifferteksten med multiplikativ invers av klarteksten. Nå som `K` er kjent kan vi kjøre samme løsning som for høydekryptering1 ved å multiplisere `K^1` med hele shifferteksten:
```
# C,P til 5x5 matriser
C = np.array(list([alphabet.index(i) for i in C])).reshape(5,5).T
P = np.array(list([alphabet.index(i) for i in P])).reshape(5,5).T

# finner K=C*P^-1
from sympy import Matrix
inverse_P = Matrix(P).inv_mod(len(alphabet))
key = (np.dot(C, np.array(inverse_P)) % len(alphabet)).T

# samme som for høydekryptering1
inverse_key = Matrix(key).inv_mod(len(alphabet))
print(encrypt(ciphertext, np.array(inverse_key)))
```

Klarteksten:
```
In_classical_cryptography,_the_Hill_cipher_is_a_polygraphic_substitution_cipher_based_on_linear_algebra._Invented_by_Lester_S._Hill_in_1929,_it_was_the_first_polygraphic_cipher_in_which_it_was_practical_(though_barely)_to_operate_on_more_than_three_symbols_at_once._The_following_discussion_assumes_an_elementary_knowledge_of_matrices._EGG{et_kjent_klartekstangrep_p@_hillcipher_trenger_n_par_med_klartekst-krypteringstekst!}
```

# Flagg

EGG{et_kjent_klartekstangrep_p@_hillcipher_trenger_n_par_med_klartekst-krypteringstekst!}
