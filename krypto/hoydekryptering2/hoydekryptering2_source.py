import numpy as np

from secret2 import MESSAGE, FLAG

# Super secret alphabet
alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\'()*+,-./:;?@[\\]^_`{|}~"

def encrypt(plaintext, key):
    plaintext = [alphabet.index(l) for l in plaintext]
    assert len(plaintext) % key.shape[0] == 0

    ciphertext = np.array(plaintext)
    ciphertext.resize(int(ciphertext.shape[0]/key.shape[0]), key.shape[0])
    ciphertext = np.matmul(ciphertext, key)
    ciphertext = np.remainder(ciphertext, len(alphabet)).flatten()

    return "".join([alphabet[int(c)] for c in ciphertext])


import random
KEY = ""
for _ in range(25):
    KEY += random.choice(alphabet)

encrypt_key = np.array([alphabet.index(k) for k in KEY])
encrypt_key.resize(5, 5)

assert encrypt_key.shape[0] == encrypt_key.shape[1]
assert np.linalg.det(encrypt_key) != 0

ciphertext = encrypt(MESSAGE+FLAG, encrypt_key)
print(ciphertext)

print("\nLeaked parts of the plaintext ...")
LEAKS_AT = [        5*0,        5*3,        5*12,        5*19,        5*24,  ]
for l in LEAKS_AT:
    print(l, MESSAGE[l:l+5])

