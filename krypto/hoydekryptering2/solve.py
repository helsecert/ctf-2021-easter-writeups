import numpy as np

# Super secret alphabet
alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\'()*+,-./:;?@[\\]^_`{|}~"

def encrypt(plaintext, key):
    plaintext = [alphabet.index(l) for l in plaintext]
    assert len(plaintext) % key.shape[0] == 0

    ciphertext = np.array(plaintext)
    ciphertext.resize(int(ciphertext.shape[0]/key.shape[0]), key.shape[0])
    ciphertext = np.matmul(ciphertext, key)
    ciphertext = np.remainder(ciphertext, len(alphabet)).flatten()

    return "".join([alphabet[int(c)] for c in ciphertext])

ciphertext = open("hoydekryptering2_output.txt","r").read().split("\n")[0].strip()
C,P = "", ""
LEAKS_AT = [        5*0,        5*3,        5*12,        5*19,        5*24,  ]
for l in LEAKS_AT:
    C += ciphertext[l:l+5]

p = """0 In_cl
15 yptog
60 subst
95 _alge
120 ter_S""".split("\n")
for x in p:
    P += x.split(" ")[1]


# C,P til 5x5 matriser
C = np.array(list([alphabet.index(i) for i in C])).reshape(5,5).T
P = np.array(list([alphabet.index(i) for i in P])).reshape(5,5).T

# finner K=C*P^-1
from sympy import Matrix
inverse_P = Matrix(P).inv_mod(len(alphabet))
key = (np.dot(C, np.array(inverse_P)) % len(alphabet)).T

# samme som for høydekryptering1
inverse_key = Matrix(key).inv_mod(len(alphabet))
print(encrypt(ciphertext, np.array(inverse_key)))
