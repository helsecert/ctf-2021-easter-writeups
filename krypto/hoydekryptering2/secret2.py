FLAG = "EGG{et_kjent_klartekstangrep_p@_hillcipher_trenger_n_par_med_klartekst-krypteringstekst!}"

MESSAGE = ("In classical cryptography, the Hill cipher is a polygraphic substitution cipher based on linear algebra. Invented by Lester S. Hill in 1929, it was the first polygraphic cipher in which it was practical (though barely) to operate on more than three symbols at once. The following discussion assumes an elementary knowledge of matrices. ").replace(" ","_")

