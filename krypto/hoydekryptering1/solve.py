import numpy as np

# Super secret alphabet
alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\'()*+,-./:;?@[\\]^_`{|}~"

def encrypt(plaintext, key):
    plaintext = [alphabet.index(l) for l in plaintext]
    assert len(plaintext) % key.shape[0] == 0

    ciphertext = np.array(plaintext)
    ciphertext.resize(int(ciphertext.shape[0]/key.shape[0]), key.shape[0])
    ciphertext = np.matmul(ciphertext, key)
    ciphertext = np.remainder(ciphertext, len(alphabet)).flatten()

    return "".join([alphabet[int(c)] for c in ciphertext])


KEY = "super_secret_key"
encrypt_key = np.array([alphabet.index(k) for k in KEY])
encrypt_key.resize(4, 4)


# Solve
from sympy import Matrix
inverse_key = Matrix(encrypt_key).inv_mod(len(alphabet))

ciphertext = open("hoydekryptering1_output.txt","r").read().strip()
print(encrypt(ciphertext, np.array(inverse_key)))
