Oppgaven handler om Hill Cipher. Kryptering er gjort ved at en nøkkel `K` og en klartekst `P` blir multiplisert modulo `N` for å skape en chiffertekst `C`. N er størrelsen på alfabetet:
```
  C = P*K   (mod N)
```

Dekryptering er samme operasjon som kryptering, men med invers av nøkkelen. Vi må her gjøre multiplikativ invers av matrisen `K` slik vi gjør i oppgaven Matte1.
```
  P = C*K^1 (mod N)
```

Vi kan bruke et bibliotek som sympy eller sage for å finne multiplikativ invers av en matrise (det er litt komplisert å gjøre det med penn og papir). Løsningen er derfor:
```
from sympy import Matrix
inverse_key = Matrix(encrypt_key).inv_mod(len(alphabet))

ciphertext = open("hoydekryptering1_output.txt","r").read().strip()
print(encrypt(ciphertext, np.array(inverse_key)))
```

# Flag
EGG{polygrafisk_substitusjonskryptering_fra_1929_med_modulAEr_multiplikativ_invers!}
