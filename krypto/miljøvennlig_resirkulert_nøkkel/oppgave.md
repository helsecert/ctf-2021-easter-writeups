For å overføre mange viktige meldinger har jeg valgt en engangsnøkkel som skal gi perfekt sikkerhet.

For å ta vare på miljøet ved å spare verden for strøm har jeg valgt å gjenbruke nøkkelen.

Jeg kan ikke tenke meg at dette skal være noe problem!

Jeg vurderte å bruke et ord som nøkkel, for eksempel NORSK HELSENETT, DETECT og CYBERATTACKS, men da ville det jo bli for enkelt å finne igjen disse ordene i meldingene.

