Dette er et klassisk crib-drag angrep mot gjenbrukt one-time pad (OTP) som da snarere blir many-time pad: meldingene er XORet med samme nøkkel.

Meldingene var (random order):
```
FOLLOWING ARE FIVE IMPORTANT MESSAGES FROM NORSK HELSENETT TO YOU
OUR INFRASTRUCTURE NEEDS TO BE SECURED FROM THREAT ACTORS
DISCOVERING CYBERATTACKS IS OF PARAMOUNT IMPORTANCE
WE MUST DETECT THE THREAT ACTORS BEFORE MISSION COMPLETION
ALL YOUR LOGS ARE BELONGS TO HELSECERT
KEEP THE EGG{AC2EC21A9621CE962CF8C75F7BAA86C7} SECRET
```

Oppgaven oppgir at alle meldingene er med store bokstaver og mellomrom, men flagget inneholder også tall. Det var ikke meningen at dette skulle skape forvirring.

Angrepet utnytter noen egenskaper i XOR:
```
kryptert_melding0 = melding0 XOR key
kryptert_melding1 = melding1 XOR key

kryptert_melding0 XOR kryptert_melding1 = melding0 XOR key XOR melding1 XOR key

# key XOR key = 0, derfor:
kryptert_melding0 XOR kryptert_melding1 = melding0 XOR key XOR melding1 XOR key = melding0 XOR melding1
``` 

Hvis vi nå vet noen av ordene i enten melding0 eller melding1 så kan vi XOR'e dette med `melding0 XOR melding1 XOR <ord>`, og dermed finne innholdet i den andre meldingen.

Alternativt er å finne deler av key ved å XOR'e en kryptert melding med et ord som finnes i meldingen:
```
kryptert_melding0 = melding0 XOR key

kryptert_melding0 = melding0 XOR key XOR melding0' (deler av den) = key' (deler av den)
```

Nå som vi er kjent med hvordan vi "knekker" kryptoen trenger vi klartekst ord (eller å tippe de). Det er oppgitt 3 ord i oppgaveteksten, så vi starter med disse.

Cribdrag gjøres ved at vi velger et ord vi tror kan finnes i meldingen, og "drar" ordet langs den krypterte meldingen mens vi kjører XOR. Vi kan starte med ordet `DETECT` (som er 6 byte langt) og forsøke dette på de krypterte meldingene i posisjon `0..5`, så `1..6`, `2..7` osv.

Hvordan vet vi om vi fant noe? Vi kan bruke øynene og lese output, vi kan bruke ordbøker eller vi kan teste om det som kommer ut kun er ASCII printbart. I python kan vi bruke `enchant` med `en_US` dictionary. Vi må sjekke deler av outputten mot ordboka siden vi nok ikke får ut fullstendige ord. Det er ikke tilfeldig at meldingene i oppgaven er på engelsk :-)


Vi cribdragger ` NORSK HELSENETT ` og finner `6C7} SECRET`, `M THREAT ACTORS` og `SSION COMPLETION`. For `DETECT` finner vi `E EGG{A`, `LOGS` mens `CYBERATTACKS` gir oss `G{AC2EC21A9621`, `GS ARE BELONGS`, `RE FIVE IMPORT` og `RUCTURE NEEDS` (her ser vi allerede starten og slutten på flagget: `EGG{AC2EC21A9621???6C7}`).

Med vill gjetting kan vi neste runde prøve `MISSION COMPLETION`, `STRUCTURE NEEDS` og `LOGS ARE BELONGS`. Disse gir oss nye ord. Vi gjentar dette til vi greier å hente ut egget.

Her er output som ble brukt for å sjekke at oppgaven er løsbar. Mellomrommet i ordene er symboliserer hver iterasjon med innhold som er hentet ut:
```
parital_words = [
    b" NORSK HELSENETT ",
    b" DETECT",
    b" CYBERATTACKS ",

    b" MISSION COMPLETION",
    b"STRUCTURE NEEDS",
    b" LOGS ARE BELONGS ",

    b" ARE FIVE IMPORANT",
    b"EGG{AC2EC21A9621CE96",
    b"DETECT THE THREAT ACTORS ",

    b"G ARE FIVE IMPORTANT MESSAGE",

    b"ASTRUCTURE NEEDS TO BE SECURE",
    b"ASTRUCTURE NEEDS TO BE SECURED",
    b" LOGS ARE BELONGS TO HELSECERT ",  # :-)

    b"ING CYBERATTACKS IS OF PARAMOUNT ",
    b"DETECT THE THREAT ACTORS BEFORE ",

    b"G ARE FIVE IMPORTANT MESSAGES FROM ",
    b"ASTRUCTURE NEEDS TO BE SECURED FROM ",

    b"ING CYBERATTACKS IS OF PARAMOUNT IMPORTAN",
    b"DETECT THE THREAT ACTORS BEFORE MISSION COMPLET",


    b"EGG{AC2EC21A9621CE962CF8C75F7BAA86C7} SECRET"
]
```

Flagget er EGG{AC2EC21A9621CE962CF8C75F7BAA86C7}
