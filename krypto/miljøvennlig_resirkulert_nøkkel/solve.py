import enchant
import string
import os
import random

def xor(a, b):
    l = min(len(a), len(b))
    return bytes(x ^ y for x, y in zip(a[0: l], b[0: l]))

cts = []
with open("gjenbruk_output.txt","r") as f:
    for line in f.readlines():
        cts.append(bytes.fromhex(line))

lengths = list(map(len, cts))
CRIB_LENGTH = max(lengths)


charset = string.ascii_uppercase + string.digits + " {}"


def allowed(a):
    return all(c in charset for c in a)


valid = enchant.Dict("en_US")


def attack_otp(word):
    for offset in range(0, CRIB_LENGTH - len(word)):
        for i, ptpt in enumerate(cts):
            otp = xor(ptpt[offset: offset+len(word)], word)
            for j, ptpt2 in enumerate(cts):
                if i == j:
                    continue

                c = cts[j]
                pt = (xor(c[offset: offset+len(word)], otp)).decode()
                if allowed(pt) and len(pt) > 4:
                    # scan partial to find english words
                    INTERNAL_LEN = 5
                    for i in range(0, len(pt)-INTERNAL_LEN):
                        for j in range(i+INTERNAL_LEN, len(pt)):
                            partial = pt[i:j]
                            if valid.check(partial):
                                print("   ", offset, pt)
                                break
                        else:
                            continue
                        break
                    else:
                        print(offset, pt)


parital_words = [
    b" NORSK HELSENETT ",
    b" DETECT",
    b" CYBERATTACKS ",

    b" MISSION COMPLETION",
    b"STRUCTURE NEEDS",
    b" LOGS ARE BELONGS ",

    b" ARE FIVE IMPORANT",
    b"EGG{AC2EC21A9621CE96",
    b"DETECT THE THREAT ACTORS ",

    b"G ARE FIVE IMPORTANT MESSAGE",

    b"ASTRUCTURE NEEDS TO BE SECURE",
    b"ASTRUCTURE NEEDS TO BE SECURED",
    b" LOGS ARE BELONGS TO HELSECERT ",  # :-)

    b"ING CYBERATTACKS IS OF PARAMOUNT ",
    b"DETECT THE THREAT ACTORS BEFORE ",

    b"G ARE FIVE IMPORTANT MESSAGES FROM ",
    b"ASTRUCTURE NEEDS TO BE SECURED FROM ",

    b"ING CYBERATTACKS IS OF PARAMOUNT IMPORTAN",
    b"DETECT THE THREAT ACTORS BEFORE MISSION COMPLET",


    b"EGG{AC2EC21A9621CE962CF8C75F7BAA86C7} SECRET"
]


for i, crib in enumerate(parital_words):
    crib_word = parital_words[i]
    print(">", crib_word)
    attack_otp(crib_word)
    print()
