# Oppgave:

## Passordknekking 1

En passordhash av typen MSCACHEv2/DCC2 er snappet opp gjennom LLMNR-spoofing. Hashcat har støtte for å knekke denne typen passordhasher, men det går fryktelig treigt.

Hash som skal knekkes:

```
$DCC2$10240$#kylling#9eb4ad793886bb81bb24daf3cf90ba4e
```

Fra før av har man sett at brukeren har brukt følgende passord på andre tjenester, og kan kanskje ane et visst mønster:

```
Sommer1A0f0k(0
Sommer4B1g1k)0
Sommer7H1h0k(1
Sommer2E0f0k)1
Vinter9Z1h1k(1
Sommer0E0f0k)1
Sommer2J1h0k(0
Sommer7K1f0k(1
Vinter3N0h1k(0
Sommer8M1g1k)1
Vinter2D0g0k)0
Sommer4Q1f1k(1
Vinter6D0h1k)0
```

Klarer du å knekke hashen?

# Løsning:

Vi ser at brukeren har `Vinter` eller `Sommer` som prefiks og deretter et suffiks som har et ganske bestemt mønster. Vi lager en ordliste (tekstfil) med de to ordene i filen prefix.txt.

Tegn nummer 0 i suffikset virker som helt arbitrært tall og tegn 1 som en tilfeldig stor bokstav. Tegn 2, 4 og 7 er enten `0` eller `1`, tegn 3 er `f`, `g` eller `h`, tegn 5 er alltid `k` og tegn 6 er en parentes.

```
Tegn 0: Tall
Tegn 1: Stor bokstav
Tegn 2: 0 eller 1
Tegn 3: f, g eller h
Tegn 4: 0 eller 1
Tegn 5: k
Tegn 6: ( eller )
Tegn 7: 0 eller 1
```

Vi lager en maske for endelsen basert på dette. ?d betyr et siffer (0-9), ?u er "uppercase letter", ?l er lowercase letter, ?s er spesialtegn. Vi kan også lage egne tegnklasser for [01] og [fgh] som vi refererer til med ?1 og ?2. Attack mode 6 i hashcat lar oss bruke en ordliste som prefiks og en maske for suffiks. For å sette sammen det hele kjører vi følgende hashcat-kommando:

hashcat -m 2100 hash.dcc2 -1 01 -2 fgh -3 '()' -a 6 prefix.txt ?d?u?1?2?1k?s?1
