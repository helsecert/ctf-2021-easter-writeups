# Passordknekking 1

En passordhash av typen MSCACHEv2/DCC2 er snappet opp gjennom LLMNR-spoofing. Hashcat har støtte for å knekke denne typen passordhasher, men det går fryktelig treigt.

Hash som skal knekkes:

```
$DCC2$10240#kylling#9eb4ad793886bb81bb24daf3cf90ba4e
```

Fra før av har man sett at brukeren har brukt følgende passord på andre tjenester, og kan kanskje ane et visst mønster:

```
Sommer1A0f0k(0
Sommer4B1g1k)0
Sommer7H1h0k(1
Sommer2E0f0k)1
Vinter9Z1h1k(1
Sommer0E0f0k)1
Sommer2J1h0k(0
Sommer7K1f0k(1
Vinter3N0h1k(0
Sommer8M1g1k)1
Vinter2D0g0k)0
Sommer4Q1f1k(1
Vinter6D0h1k)0
```

Klarer du å finne passordet?
