MD5 er en utdatert kryptografisk hashfunksjon som tar inn en melding på et vilkårlig antall bits (fra og med 0 bits og oppover) og spytter ut en sjekksum på 128 bits. Sjekksummen av den korteste mulige meldingen, altså en tom melding, skriver vi som 32 heksadesimale siffer: `d41d8cd98f00b204e9800998ecf8427e`.

Vi har brukt MD5 på en veldig kort melding og fått ut følgende første 24 siffer av sjekksum: `df06638b392a1ad0a95da127`

Meldingen er svært kort, atskilling kortere enn 16 bits. Klarer du å finne de siste åtte sifrene av sjekksummen? Lever EGG{<8 heksadesimale siffer>}
