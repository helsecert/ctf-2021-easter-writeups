# Oppgave

## MD5

MD5 er en utdatert kryptografisk hashfunksjon som tar inn en melding på et vilkårlig antall bits (fra og med 0 bits og oppover) og spytter ut en sjekksum på 128 bits. Sjekksummen av den korteste mulige meldingen, altså en tom melding, skriver vi som 32 heksadesimale siffer: `d41d8cd98f00b204e9800998ecf8427e`.

Vi har brukt MD5 på en veldig kort melding og fått ut følgende første 24 siffer av sjekksum: `df06638b392a1ad0a95da127`

Meldingen er svært kort, mindre enn tre bytes. Klarer du å finne de siste åtte sifrene av sjekksummen? Lever EGG{<8 heksadesimale siffer>}

# Løsning:

Siden meldingen er mindre enn tre bytes lang kan vi sjekke alle mulige meldinger på én byte og alle mulige meldinger på to bytes. Ingen av disse stemmer overens med oppgitt sjekksum.

Men en melding trenger ikke være et helt antall bytes. Vi finner en implementasjon i ønsket språk på https://rosettacode.org/wiki/MD5/Implementation og gjør den om til å støtte meldinger på et arbitrært antall bits. Vi ser da at en melding på kun to bits, `10`, gir ønsket output.

Python-kode med omgjort MD5-implementasjon ligger i `md5.py`.
