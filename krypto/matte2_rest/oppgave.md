En kinesisk general har altfor mange soldater til å kunne telle alle sammen. Hvordan skal han vite hvor stor hæren er?

En kløktig matematiker vet svaret! Han ber generalen stille opp soldatene i rekker med en bestemt lengde. Hvis f. eks. generalen har 23 soldater og stiller disse opp i rekker med lengde 3 så vil man få en rest på 2 soldater. La oss for moro skyld benevne lengden på rekkene som `n[i]` mens vi kan kalle de som ble til rest for `s[i]`. I eksemplet over ville vi fått `n[0]=3` og `s[0]=2`.

Det er fra før estimert at antall soldater kan representeres på færre enn 210 bits.

Matematikeren bestiller oppstilling av soldatene i forskjellige rekker med opptelling av rest:
```
s[0] = 730185355840085704378
n[0] = 954557211877289119789
s[1] = 311836825142240843993
n[1] = 838313404031611232357
s[2] = 77245018134820932374
n[2] = 875707149083085815411
```

Nå vet matematikeren eksakt hvor mange soldater generalen har i hæren sin.

Flagget finner du som ASCII representasjonen av antall soldater.
