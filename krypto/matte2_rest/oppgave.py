# https://en.wikipedia.org/wiki/Secret_sharing_using_the_Chinese_remainder_theorem
from Crypto.Util.number import bytes_to_long, getPrime
import numpy

S = bytes_to_long(b"EGG{CRT_3r_n3s73n_m@g1sk!}")

nbits = (int(S).bit_length() // 3)+1
n = [getPrime(nbits) for _ in range(3)]
N = numpy.prod(n) 
s = [S%n[i] for i in range(len(n))]
assert S < N

for i in range(len(n)):
    print(f"s[{i}] = {s[i]}")
    print(f"n[{i}] = {n[i]}")

# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
from functools import reduce
def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod

def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1

S_ = chinese_remainder(n,s)
assert S == S_

from Crypto.Util.number import long_to_bytes
print(long_to_bytes(S_))

