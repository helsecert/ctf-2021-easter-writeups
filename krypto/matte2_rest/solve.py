s,n = [0]*3, [0]*3
s[0] = 730185355840085704378
n[0] = 954557211877289119789
s[1] = 311836825142240843993
n[1] = 838313404031611232357
s[2] = 77245018134820932374
n[2] = 875707149083085815411

# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
from functools import reduce
def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod

def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1

from Crypto.Util.number import long_to_bytes
S_ = chinese_remainder(n,s)
print(long_to_bytes(S_))

