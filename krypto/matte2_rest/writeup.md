I oppgaven omtales en kinesisk general og i tittelen har vi ordet `rest`. Dette peker mot CRT:
```
  Kinesisk restteorem  eller  chinese remainder theorem
```

Den fungerer slik:
La `n_i` være en mengde naturlige tall som er parvis relativt primiske.
La `a_i` være en mengde naturlige tall som er `0 <= a < n_i`.
Det vil nå finnes _en_ unik løsning `x`, `0 <= x < N`, hvor `N` er produktet av
`n_i` og hvor `a_i` er euklids divisjon av `x` over `n_i` (for alle i).

Vi kan starte med å teste at n_i er parvis relativt primiske ved:
```
assert gcd(n[0], n[1]) == 1
assert gcd(n[1], n[2]) == 1
assert gcd(n[2], n[0]) == 1
```
Vi kan også være sikre på at de er parvis relativt primiske hvis alle `n_i` er primtall!

Videre ser vi at `a_i < n_i` (for alle i) og da kan vi benytte oss av CRT for å finne løsningen.

Man kan finne ferdig implementasjoner av CRT i f.eks. python:
 https://rosettacode.org/wiki/Chinese_remainder_theorem#Python

Eller man kan bruke Sage
```
sage: from Crypto.Util.number import long_to_bytes
sage: s,n = [0]*3, [0]*3
....: s[0] = 730185355840085704378
....: n[0] = 954557211877289119789
....: s[1] = 311836825142240843993
....: n[1] = 838313404031611232357
....: s[2] = 77245018134820932374
....: n[2] = 875707149083085815411
....:
sage: flagg = long_to_bytes(crt(s,n))
b"EGG{CRT_3r_n3s73n_m@g1sk!}"
```
