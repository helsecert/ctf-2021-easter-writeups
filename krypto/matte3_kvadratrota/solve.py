from Crypto.Util.number import bytes_to_long, long_to_bytes, getPrime

p= 9124597104758540080509291029676122374984377441887824449832944547628900282228841849713949013869826508940227230479894829537848432784148500372100266463819447
a= 283849800649093313510494175418755497823945890813696818393522811641016385469212918363581359374155332176958058700548924862371286274930622752510028849706134

def legendre(a, p):
    return pow(a, (p - 1) // 2, p)
 
def tonelli(n, p):
    assert legendre(n, p) == 1, "not a square (mod p)"
    q = p - 1
    s = 0
    while q % 2 == 0:
        q //= 2
        s += 1
    if s == 1:
        return pow(n, (p + 1) // 4, p)
    for z in range(2, p):
        if p - 1 == legendre(z, p):
            break
    c = pow(z, q, p)
    r = pow(n, (q + 1) // 2, p)
    t = pow(n, q, p)
    m = s
    t2 = 0
    while (t - 1) % p != 0:
        t2 = (t * t) % p
        for i in range(1, m):
            if (t2 - 1) % p == 0:
                break
            t2 = (t2 * t2) % p
        b = pow(c, 1 << (m - i - 1), p)
        r = (r * b) % p
        c = (b * b) % p
        t = (t * c) % p
        m = i
    return r

def recursive_mod_sqrt(a, p, i):
    # finn begge røttene
    try:
        roots = [tonelli(a,p)]
        roots.append(p-roots[0])
        print("roots", i, 2**i)
    except:        
        return # ikke kvadrat, stopp

    # sjekk om vi fant et flagg
    for root in roots:
        if b"EGG" in long_to_bytes(root):
            print(long_to_bytes(root).decode())
            exit()

    # test begge røttene
    for root in roots:
        recursive_mod_sqrt(root, p, i+1)

recursive_mod_sqrt(a,p, 0)

