from Crypto.Util.number import bytes_to_long, long_to_bytes, getPrime

p = getPrime(512)
m = bytes_to_long(b"EGG{6r47ul3r3r_du_f4n7_37_fl466}")
assert m < p
a = pow(m, 256, p)
print("p=", p)
print("a=", a)


def legendre(a, p):
    return pow(a, (p - 1) // 2, p)
 
def tonelli(n, p):
    assert legendre(n, p) == 1, "not a square (mod p)"
    q = p - 1
    s = 0
    while q % 2 == 0:
        q //= 2
        s += 1
    if s == 1:
        return pow(n, (p + 1) // 4, p)
    for z in range(2, p):
        if p - 1 == legendre(z, p):
            break
    c = pow(z, q, p)
    r = pow(n, (q + 1) // 2, p)
    t = pow(n, q, p)
    m = s
    t2 = 0
    while (t - 1) % p != 0:
        t2 = (t * t) % p
        for i in range(1, m):
            if (t2 - 1) % p == 0:
                break
            t2 = (t2 * t2) % p
        b = pow(c, 1 << (m - i - 1), p)
        r = (r * b) % p
        c = (b * b) % p
        t = (t * c) % p
        m = i
    return r

def check(a, p):
    r = tonelli(a,p)   
    if b"EGG" in long_to_bytes(r):
        print(long_to_bytes(r))
        exit()
    if b"EGG" in long_to_bytes(p-r):
        print(long_to_bytes(p-r))
        exit()
    check(r,p)
    check(p-r,p)
check(a,p)

