Hvis vi hadde hatt et tall `a=m^n` uten modulo så ville det vært enkelt
å finne a. Det er bare å ta nth'e rota. Med modulo må vi bruke en annen algoritme.

Vi kan google "modular square root". Vi finner Tonelli-Shanks algoritmen. Den ligger
blant annet på rosettacode:
  https://rosettacode.org/wiki/Tonelli-Shanks_algorithm

Med `m^n` har vi `n=2^x`. Vi kjenner ikke x, men vi ser at eksponenten er 2, 4, 8, 16, 32 .. og oppover.
Siden dette er en CTF og den skal være løsbar vil den ikke være "veldig høy".

For å finne `m` må vi ikke bare ta modulær kvadratrot en gang, men mange ganger etterhverandre. Det som er 
viktig er å avgjøre om man skal forsette å ta kvadratrota av et tall. Dette kan vi finne ut ved å sjekke
om et tall har en kvadratisk rest modulo n. Til dette bruker vi Legendre symbol (som også ligger i toppen
i python koden for Tonelli-Shanks på rosettacode).

For hver rot må vi sjekke både positiv og negativ, og hvis tallet ikke har en kvadratisk rest modulo n lik 1 så tar ikke med den videre.

Moduloen er relativt sett ganske liten (512bit) så løsningen kommer raskt (<1s).

Med tonelli-shanks fra rosetta kan en naiv algoritme for å finne løsning kan være:
```
def recursive_mod_sqrt(a, p, i):
    # finn begge røttene
    try:
        roots = [tonelli(a,p)]
        roots.append(p-roots[0])
        print("roots", i, 2**i)
    except:
        return # ikke kvadrat, stopp

    # sjekk om vi fant et flagg
    for root in roots:
        if b"EGG" in long_to_bytes(root):
            print(long_to_bytes(root).decode())
            exit()

    # test begge røttene
    for root in roots:
        recursive_mod_sqrt(root, p, i+1)

recursive_mod_sqrt(a,p, 0)
```

Output
```
roots 0 1
roots 1 2
roots 2 4
roots 3 8
roots 4 16
roots 5 32
roots 6 64
roots 7 128
EGG{6r47ul3r3r_du_f4n7_37_fl466}
```
