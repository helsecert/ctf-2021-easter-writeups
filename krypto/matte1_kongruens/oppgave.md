I modulær aritmetikk kan vi finne invers *x* av et tall *a*, slik at produktet av *a* og *x* modulo *p* blir kongruent med 1, hvor *p* er et primtall og *a* og *p* er innbyrdes primiske.
​
`a*x ≡ 1 (mod p)` 
​
Her får du to tall:
```
p = 88619509055522082453204780866094153179701708680210152273575985657978607113243
x = 38751234384983559040347094325349107850289644025269636239010846543908547616067
```
​
Flagget finner du som ASCII representasjonen av tallet `a`.
