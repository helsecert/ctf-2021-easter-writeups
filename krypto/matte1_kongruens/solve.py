from Crypto.Util.number import inverse, long_to_bytes
p = 88619509055522082453204780866094153179701708680210152273575985657978607113243
x = 38751234384983559040347094325349107850289644025269636239010846543908547616067
a = inverse(x, p)
print(a)
print(long_to_bytes(a))

import math
def egcd(a,b):
    r = [a,b]
    q = []
    s = [1,0]
    t = [0,1]
    while r[-1] > 0:
        q.append(math.floor(r[-2]/r[-1]))
        r.append(r[-2] - (q[-1]*r[-1]))
        s.append(s[-2] - (q[-1]*s[-1]))
        t.append(t[-2] - (q[-1]*t[-1]))
    return r[-2], s[-2], t[-2]

assert egcd(x, p)[1] == a
