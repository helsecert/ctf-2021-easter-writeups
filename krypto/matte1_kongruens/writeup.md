Vi må anta at `p,a` er relativt primiske, altså største felles faktor er 1,
ellers vil vi ikke finne en unik løsning. Uten at de er relativt primiske kan
vi risikere at det enten ikke finnes en løsning, eller at det finnes mange.
Når de er relativt primiske er vi sikre på at det kun finnes en unik løsning.

Siden vi får oppgitt flag_invers vil vi finne `flag = flag_invers^-1 mod p`.
I modulær aritmetikk kan vi kun ha positive heltall og derfor vil det ikke
fungere å ta `1/flag_invers` siden vi får en brøk. Vi må bruke `modulær multiplikativ invers`.

En funfact er at python har kommet med støtte for å gjøre nettopp `pow(a,-1,p)` i py3.8:
den vil støtte negativ eksponent hvis base tallet er relativt primisk med modulusen.

For de av oss som fremdeles ikke kjører py3.8 kan vi bruke et bibliotek for å løse oppgaven,
som pycryptodome's `Crypto.Util.number.inverse`, `gmpy2's invert`, `Sagemath`, --
eller vi kan lese om `euklids utvidede algoritme` og implementere noe sjøl. Den er ikke så
alt for komplisert, og den er superrask selv på store tall. Det er fin læring å sette seg inn
i hvordan euklids og euklids utvided algoritme fungerer.


# solve
```
from Crypto.Util.number import inverse, long_to_bytes
p = 88619509055522082453204780866094153179701708680210152273575985657978607113243
x = 38751234384983559040347094325349107850289644025269636239010846543908547616067
a = inverse(x, p)
print(long_to_bytes(a))


# eller manuelt

import math
def egcd(a,b):
    r = [a,b]
    q = []
    s = [1,0]
    t = [0,1]
    while r[-1] > 0:
        q.append(math.floor(r[-2]/r[-1]))
        r.append(r[-2] - (q[-1]*r[-1]))
        s.append(s[-2] - (q[-1]*s[-1]))
        t.append(t[-2] - (q[-1]*t[-1]))
    return r[-2], s[-2], t[-2]

assert egcd(x, p)[1] == a
```

# dataformater: long, bytes, hex, ascii -- hææ??

Det har vært flere spørsmål rundt levering av flagget. Det står en linje i bunn av matteoppgavene som
hinter til at man må konvertere tallet til bytes for å lese flagget. Det er ikke "det store tallet" 
som skal leveres.

I denne oppgaven fikk vi tallet `1544960715111083613978461948837277169691682173` og poenget er jo da å
konvertere dette til bytes slik at man finner `EGG{..}`:
```
>>> from Crypto.Util.number import bytes_to_long, long_to_bytes
>>> print(long_to_bytes(1544960715111083613978461948837277169691682173))
b'EGG{moro_med_matte}'
```

Dette kan kanskje være nytt for mange, å bevege seg mellom ulike dataformater. Vi kan ikke gjøre matte
på en tekststreng, den må konverteres til/fra et stort tall. Eksempel er når man bruker RSA for å kryptere
en melding (google it!).

Hvis vi har en tekststreng "hello" kan den representeres både som et stort heltall (long),
hexadesimale tegn (0123456789abcdef), "bytes" og som en lesbar streng. Ofte er bytes
av en variabel også lesbar, iallefall de bytsene som kan printes ut (dvs ASCII).

Der man tidligere brukte "import binascii" og "hexlify, unhexlify" kan man i dagens python bruke innebygd støtte
med `bytes.fromhex("68656c6c6f")` og `<byte objekt>.hex()`. Vi navigerer mellom string og bytes med `.encode()` og `.decode()`.
Vi ser også at vi kan skrive en streng direkte som bytes objekt ved å prefixe strengen med `b".."` som ved `b"hello"`.
Dette er da ekvivalent med å skrive `"hello".encode()`.

```
>>> "hello".encode()
b'hello'
>>> b"hello".decode()
'hello'
>>> "hello".encode().hex()
'68656c6c6f'
>>> bytes.fromhex("68656c6c6f")
b'hello'
>>> b"hello" == "hello".encode()
True

>>> from Crypto.Util.number import bytes_to_long, long_to_bytes
>>> bytes_to_long(b"hello")
448378203247
>>> long_to_bytes(448378203247)
b'hello'
```

Konverteringen frem og tilbake må gjøres presist og riktig for å unngå feil. Det er mulig å
gjøre dette på mange måter, men hvis du bruker python's pycryptodome lib så er du helt sikker
på at dette ikke er feilkilden når du konverterer mellom long og bytes. Det kan derfor være 
fint å huske denne til fremtidige kryptooppgaver:
```
from Crypto.Util.number import bytes_to_long, long_to_bytes
```
