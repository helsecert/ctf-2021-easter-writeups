#Oppgave

## Kyllinghjerne

En påskekylling er påkjørt under kryssing av glatt vintervei og har omkommet! :-( Denne påskekyllingen var det eneste vesenet i hele verden som huska det ene EGGet. EGGet var ti tegn, og hvis du viste kyllingen en plakat med tallet 0, tegna den første tegn i EGGet. Viste du tallet 1, tegna den andre tegn i EGGet, osv.

Kyllingen kommer ikke tilbake, men kanskje finnnes det håp for EGGet? En analytiker har tatt vare på kyllinghjernen og ekstrahert og digitalisert et fungerende nevralt nettverk fra deler av den. Nevralnettet ligger i filen `chicken.h5`. De har også digitalisert noen tallplakater som kyllingen ble trent opp med. Disse ligger i filen `input.txt`.

For å lese inn input.txt til et kyllinghjernevennlig format får du oppgitt følgende oneliner i Python:

```
with open("input.txt") as f: inputs = [[list([(ch=='X')-0.5] for ch in f.readline()[:28]) for b in range(28)] for i in range(10)]
```

Kan du utføre påskekyllingens siste ønske og levere EGGet?

# Løsning:
Oppgaven går ut på å laste inn et nevralnett med tensorflow/keras, sende input gjennom det og hente EGGet ut fra output.

Vi kjører `strings` på fila og skjønner at den er laget med tensorflow og keras, siden begge er nevnt der. Vi installer tensorflow og skriver to linjer kode for å importere tensorflow og laste inn nevralnettet fra fil. Ved å sende inn oppgitt input fra oppgaven, får vi ut 10x10 floats per input. Threshold på 0.5 for om en gitt piksel er svart eller hvit gir flagget ut i leselig format.

Vi ender opp med totalt sju linjer pythonkode for å få en lettlest output:

```
from tensorflow.keras.models import load_model

# Python-linje for inputlesing oppgitt i oppgavetekst:
with open("input.txt") as f: inputs = [[list([(ch=='X')-0.5] for ch in f.readline()[:28]) for b in range(28)] for i in range(10)]

for d in inputs:
   for l in d:
      print(l)

# Last inn ferdigtrent kyllinghjerne fra fil
model = load_model('chicken.h5')

# Send inn alle inputs, 0 til 9
outputs = model.predict(inputs)

# Output per input er et array med 10x10 floats. Vi prøver threshold på 0.5 for om piksel er sort eller hvit:
for p in outputs:
    for row in p:
        print("".join(" X"[x > 0.5] for x in row))
```
