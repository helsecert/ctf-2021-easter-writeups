# Kyllinghjerne

En påskekylling er påkjørt under kryssing av glatt vintervei og har omkommet! :-( Denne påskekyllingen var det eneste vesenet i hele verden som huska det ene EGGet. EGGet var ti tegn, og hvis du viste kyllingen en plakat med tallet 0, tegna den første tegn i EGGet. Viste du tallet 1, tegna den andre tegn i EGGet, osv.

Kyllingen kommer ikke tilbake, men kanskje finnnes det håp for EGGet? En analytiker har tatt vare på kyllinghjernen og ekstrahert og digitalisert et fungerende nevralt nettverk fra deler av den. Nevralnettet ligger i filen `chicken.h5`. De har også digitalisert noen tallplakater som kyllingen ble trent opp med. Disse ligger i filen `input.txt`.

For å lese inn input.txt til et kyllinghjernevennlig format får du oppgitt følgende oneliner i Python:

```
with open("input.txt") as f: inputs = [[list([(ch=='X')-0.5] for ch in f.readline()[:28]) for b in range(28)] for i in range(10)]
```

Kan du utføre påskekyllingens siste ønske og levere EGGet?
