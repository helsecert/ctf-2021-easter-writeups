from tensorflow.keras.models import load_model

# Python-linje for inputlesing oppgitt i oppgavetekst:
with open("input.txt") as f: inputs = [[list([(ch=='X')-0.5] for ch in f.readline()[:28]) for b in range(28)] for i in range(10)]

for d in inputs:
   for l in d:
      print(l)

# Last inn ferdigtrent kyllinghjerne fra fil
model = load_model('chicken.h5')

# Send inn alle inputs, 0 til 9
outputs = model.predict(inputs)

# Output per input er et array med 10x10 floats. Vi prøver threshold på 0.5 for om piksel er sort eller hvit:
for p in outputs:
    for row in p:
        print("".join(" X"[x > 0.5] for x in row))
