# Source: https://gist.github.com/gvx/975276
# Rewritten to run min_sum with multiprocessing.Pool
from itertools import product
from multiprocessing import Pool
from functools import partial
import time

possible = [''.join(p) for p in product('ABCDEF', repeat=4)]
results = [(right, wrong) for right in range(5) for wrong in range(5 - right) if not (right == 3 and wrong == 1)]


def score(self, other):
    first = len([speg for speg, opeg in zip(self, other) if speg == opeg])
    return first, sum([min(self.count(j), other.count(j)) for j in 'ABCDEF']) - first


def solve(scorefun):
    return attempt(set(possible), scorefun, True)


def min_sum(S, x):
    return min(sum(1 for p in S if score(p, x) != res) for res in results)


_p = Pool(4)


def attempt(S, scorefun, first=False):
    if first:
        a = 'AABB'
    elif len(S) == 1:
        a = S.pop()
    else:
        t = time.time()
        if True:
            z = _p.map(partial(min_sum, S), [x for x in possible])
            x = zip(possible, z)
            x = max(x, key=lambda z: z[1])
            print("a=", x[0])
            a = x[0]
        else:
            a = max(possible, key=lambda x: min(sum(1 for p in S if score(p, x) != res) for res in results))
        print("tid", time.time()-t, "sekunder")

    print("guess=", a)
    sc = scorefun(a)

    if sc != (4, 0):
        S.difference_update(set(p for p in S if score(p, a) != sc))
        return attempt(S, scorefun)

    return a
