Spillet er kodeknekkespillet fra 70'tallet: Master Mind. Vi har her brukt det originale spillet med 6 fargede brikker. En skjult tilfeldig kombinasjon lages og man har et antall forsøk, og tid, på å finne riktig kode.

Undervis får man anvisninger:

- svarte flagg: riktig brikke (tegn, eller farge) på riktig plass
- hvite flagg: riktig brikke (tegn, eller farge) men feil plassert.

Spillet har 3 nivåer med maks antall forsøk og tid

- lærling 12forsøk 5min
- svenn 9forsøk 45s
- kodeknekker 5forsøk 15s

# Lærling

Kan løses manuelt vet at man prøver seg frem. Man kan nok bruke flere naive algoritmer for å finne løsningen når man tåler 11 feil.
En enkel løsning er å forsøke de første 6 rundene med hver farge kun for å vite antallet. Så har man 5 runder på å finne ut hvor de står.

Se writeup_nivå1.png

# Svenn

Kan fremdeles løses manuelt, men man kan få problemer med tiden.

# Kodeknekker

Med 5 forsøk og 15 sekunder kan man ikke lengre løse det manuelt (uten utrolig flaks!). Det er `6^4 = 1296` ulike kombinasjoner, så det er jo faktisk mulig å bare tippe :)

Løsningen vi var ute etter var å bruke `Donald Knuth's five-guess algorithm`. Den ligger på wikipedia:
    https://en.wikipedia.org/wiki/Mastermind_(board_game)#Worst_case:_Five-guess_algorithm

Det finnes mange implementasjoner i mange programmeringsspråk. Noen kjører veldig raskt, noen bruker tid. Antagelig var 15s på siste level for god tid da noen greide å manuelt løse den ved å bruke https://www.dcode.fr/mastermind-solver.

Som eksempel kan man bruke https://gist.github.com/gvx/975276 som er litt omskrevet for å kjøre minimax med multiprocessing.Pool. Solve kan da se slik ut, og bruker ca 8s per level. En annen deltaker brukte en .cpp implementasjon som tok rundene på vesentlig kortere tid.
```
Som lærling har du mange forsøk og god tid!
Du får 12 forsøk og 300 sekunder på å tippe riktig kode.
guess= AABB
score(svart,hvit): (1, 0)
a= ACDD
tid 6.485560894012451 sekunder
guess= ACDD
score(svart,hvit): (0, 2)
a= CACE
tid 1.0266225337982178 sekunder
guess= CACE
score(svart,hvit): (1, 1)
a= ADEA
tid 0.16305041313171387 sekunder
guess= ADEA
score(svart,hvit): (2, 0)
guess= CDEB
score(svart,hvit): (4, 0)
[+] attempts= 5 code= CDEB 🐣🐤🐥🐇
[+] flagg= EGG{M4S73rMInd_3r_f1n_hj3rn37r1m}
rundetid 8.269708633422852

Kanskje du vil prøve noe litt vanskeligere?

En svenn bør kunne greie dette på litt kortere tid!
Du får 9 forsøk og 45 sekunder på å tippe riktig kode.

guess= AABB
score(svart,hvit): (0, 0)
a= CCDE
tid 6.473424911499023 sekunder
guess= CCDE
score(svart,hvit): (1, 2)
a= CDED
tid 0.9970004558563232 sekunder
guess= CDED
score(svart,hvit): (3, 0)
a= AACF
tid 0.1147165298461914 sekunder
guess= AACF
score(svart,hvit): (1, 0)
guess= CDCD
score(svart,hvit): (4, 0)
[+] attempts= 5 code= CDCD 🐣🐤🐣🐤
[+] flagg= EGG{4u70m47153r7_M4S73rMInd}
rundetid 8.516543626785278

Kanskje du vil prøve noe litt vanskeligere?

En ekte kodeknekker bruker ikke lang tid eller mange forsøk!
Du får 5 forsøk og 15 sekunder på å tippe riktig kode.

guess= AABB
score(svart,hvit): (1, 0)
a= ACDD
tid 6.40716552734375 sekunder
guess= ACDD
score(svart,hvit): (1, 0)
a= BEBC
tid 0.9132049083709717 sekunder
guess= BEBC
score(svart,hvit): (1, 2)
a= CAAF
tid 0.11303234100341797 sekunder
guess= CAAF
score(svart,hvit): (1, 1)
guess= ECBF
score(svart,hvit): (4, 0)
[+] attempts= 5 code= ECBF 🐥🐣🐇🥚
[+] flagg= EGG{I_1977_l05t3_Don4ld_Knu7h_M4S73rMInd_m3d_m4x_FEM_fors0k}
rundetid 8.287549495697021
```
