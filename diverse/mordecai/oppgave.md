# mordecai1
Kan du knekke min skjulte kode?

nc challenges.ctfd.io 30035

# mordecai2
Med litt erfaring greier du sikkert denne også?

Du må først løse mordecai1 (samme host/port)


# mordecai3
Bare en ekte kodeknekker finner dette flagget!

Du må først løse mordecai1+2 (samme host/port)
