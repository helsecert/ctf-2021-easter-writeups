#!/usr/bin/env python
import signal
import sys
import numpy as np
import random


def timeout_handler(*args):
    print('\n⌛ tikk... takk... klokka går')
    sys.exit(0)


farger = ['🐰', '🐇', '🐣', '🐤', '🐥', '🥚']
bokstaver = list("ABCDEF")
SVART = '🏴'
HVIT = '🏳️'

FEIL = '🤷'
RIKTIG_KOMBINASJON = [0]*4


def max_forsok():
    print("😡 for mange forsøk, koden min var", "".join(RIKTIG_KOMBINASJON))
    sys.exit(0)


def feil():
    taunts = [
        "SQL injection funker ikke mot en ekte mester.",
        "Du må vinne spillet for å fortjene et flagg.",
        "Spillet er inputvalidert. Prøv igjen.",
        "Kanskje du skal lese oppgaveteksten en gang til?",
        "Kanskje du skal prøve riktig verdier?",
        "Dette er ikke en brute-force oppgave.",
        "Hacking hjelper ikke her, kun kodeknekking.",
        "Med riktig input kan du vinne et flagg!",
        "Input er KUN 4 emojis. Uten space.",
        "Bare en mesterhjerne kan slå meg!",

    ]
    print("😡 feil!", random.choice(taunts))
    sys.exit(0)


def solve_message(tittel, flaggfil):
    flagg = open(flaggfil, "rb").read().decode()
    print(f"👌 en {tittel} fortjener et flagg: {flagg}")


def ny_runde(n):
    global RIKTIG_KOMBINASJON
    RIKTIG_KOMBINASJON = list(np.random.choice(farger, n, replace=True))


def info():
    print('''
     __  __                      _                         _           __  __                _
    |  \/  |                    | |                       (_)         |  \/  |              | |
    | \  / |   ___    _ __    __| |   ___    ___    __ _   _   ___    | \  / |   ___   ___  | |_    ___   _ __
    | |\/| |  / _ \  | '__|  / _` |  / _ \  / __|  / _` | | | / __|   | |\/| |  / _ \ / __| | __|  / _ \ | '__|
    | |  | | | (_) | | |    | (_| | |  __/ | (__  | (_| | | | \__ \   | |  | | |  __/ \__ \ | |_  |  __/ | |
    |_|  |_|  \___/  |_|     \__,_|  \___|  \___|  \__,_| |_| |___/   |_|  |_|  \___| |___/  \__|  \___| |_|
    (original utgave)

    ''')

    print('''
    Kan du tippe min skjulte kombinasjon? Jeg har trekt fire tegn fra %s med tilbakelegging. Rekkefølgen er viktig.
    Etter hvert forsøk får du et %s for hvert riktige tegn som også er på riktig plass, og et %s for hvert riktige tegn, men som er på feil plass.
    ''' % ("  ".join(farger), SVART, HVIT))


def score(self, other):
    first = len([speg for speg, opeg in zip(self, other) if speg == opeg])
    return first, sum([min(self.count(j), other.count(j)) for j in 'ABCDEF']) - first


def from_emoji(x):
    return [bokstaver[farger.index(x)] for x in x]


def to_emoji(x):
    return [farger[bokstaver.index(x)] for x in x]


def spill(forsok, tid):
    kombinasjon = input('send 4 tegn> ').strip()

    # kun emojis
    for farge in kombinasjon:
        if not farge in farger:
            return feil()

    # bare 4stk
    kombinasjon = list(kombinasjon)
    if len(kombinasjon) != 4:
        return feil()

    # returner anvisninger på score
    sc = score(from_emoji(RIKTIG_KOMBINASJON), from_emoji(kombinasjon))
    anvisninger = SVART*sc[0] + HVIT*sc[1]
    print("{}\t{}".format("".join(kombinasjon), "".join(anvisninger)))

    if kombinasjon == RIKTIG_KOMBINASJON:
        return True
    return False


def main():
    signal.signal(signal.SIGALRM, timeout_handler)
    info()

    runder = [
        ["lærling", 12, 300, "Som lærling har du mange forsøk og god tid!", "flag1.txt"],
        ["svenn", 9, 45, "En svenn bør kunne greie dette på litt kortere tid!", "flag2.txt"],
        ["kodeknekker", 5, 15, "En ekte kodeknekker bruker ikke lang tid eller mange forsøk!", "flag3.txt"],
    ]

    for r, runde in enumerate(runder):
        tittel, forsok, tid, taunt, flagg = runde
        print(taunt)
        print("Du får %d forsøk og %d sekunder på å tippe riktig kode.\n" % (forsok, tid))
        ny_runde(4)
        signal.alarm(0)
        signal.alarm(tid)

        while True:
            if spill(forsok, tid) == True:
                break

            forsok -= 1
            if forsok == 0:
                max_forsok()

        solve_message(tittel, flagg)

        if r < len(runder)-1:
            print("Kanskje du vil prøve noe litt vanskeligere?\n")
            continue

        sys.exit(0)


try:
    main()
except:
    pass
