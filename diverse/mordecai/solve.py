from tqdm import tqdm
from itertools import product
import re
from pwn import *
import time

# https://gist.github.com/gvx/975276
import mastermind


farger = ['🐰', '🐇', '🐣', '🐤', '🐥', '🥚']
bokstaver = list("ABCDEF")
SVART = '🏴'
HVIT = '🏳️'


def from_emoji(x):
    return [bokstaver[farger.index(x)] for x in x]


def to_emoji(x):
    return [farger[bokstaver.index(x)] for x in x]


# return process("./challenge.py")
# io = remote("localhost", 12345, level="error")
io = remote("challenges.ctfd.io", 30035, level="error")


def play(c):
    io.recvuntil(">")
    io.sendline("".join(c))
    flags = io.recvline().decode().strip().split("\t")
    if len(flags) < 2:
        return 0, 0
    flags = flags[1]
    return flags.count(SVART), flags.count(HVIT)


def runde():
    code = mastermind.solve(scorefun)
    print("[+] attempts=", attempts, "code=", code, "".join(to_emoji(code)))
    io.recvuntil("fortjener et flagg: ")
    flagg = io.recvline().strip().decode()
    print("[+] flagg=", flagg)


attempts = 0


def scorefun(x):
    global attempts
    attempts += 1

    # translate letters to emojis
    #sc = play([farger[bokstaver.index(x)] for x in x])
    sc = play(to_emoji(x))
    print("score(svart,hvit):", sc)
    return sc


io.recvuntil(" feil plass.")
io.recvline()

for i in range(3):
    s = time.time()
    attempts = 0

    print(io.recvline().decode().strip())
    print(io.recvline().decode().strip())
    print(io.recvline().decode().strip())
    if i > 0:
        print(io.recvline().decode())

    runde()
    print("rundetid", time.time() - s)
    print()

