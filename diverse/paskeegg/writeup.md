# Oppgave
## Påskeegg

Påskeegg kommer i flere former, farger og mønstre. Her er det likevel ett egg som ser mer interessant ut enn de andre.

# Løsning

Vi ser at det ene parameteret i constructoren `Egg()` heter `rotation`. Ved å forandre og refreshe index.html kan vi rotere alle eggene og se mer av flagget.

For litt speedup kan vi sette antialiasing til 1.

Ikke-obfuskert kode for påskeegg-raytraceren finnes på https://github.com/tryeng/easteregg/
