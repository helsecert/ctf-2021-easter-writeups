exec(open("coords.txt").read())

# randomize pixels
import random, time
random.shuffle(coords)

# just print the coords
while len(coords)>0:
    coord = coords.pop()
    print(f"{coord[0]} {coord[1]}")
    time.sleep(0.005)
exit()
