from pwn import *
import pygame

#io = process("python run.py", shell=True)
io = remote("challenges.ctfd.io", 30030, level="error")

pygame.init()
BLACK = (0,0,0)
MAX_X = 400
MAX_Y = 30
display_surface = pygame.display.set_mode((MAX_X, MAX_Y))
display_surface.fill([255,255,255]) # white background
x,y = 0,0
while True:
    if pygame.event.poll().type == pygame.QUIT:
        break

    try:
        line = io.readline()
    except:
        break
   
    x,y = line.decode().strip().split(" ")
    x,y = int(x), int(y)
    display_surface.set_at((x,y), BLACK)
    pygame.display.update()

pygame.image.save(display_surface, './flag.png')
pygame.quit()
