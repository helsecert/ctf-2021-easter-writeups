import javax.net.ssl.*;
import java.io.*;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

public class Solution {
    public static byte[] data = "{\"get_flag\":1}".getBytes();
    public static byte[] S = new byte[256];
    

    public static void main(String[] args) {
        Arrays.fill(S, (byte)0x00);
        TrustManager trustAllCerts = new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            X509Certificate[] certs, String authType) {
                    }
                };
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{trustAllCerts}, new java.security.SecureRandom());
            SSLSocket ssls = (SSLSocket) sc.getSocketFactory().createSocket("challenges.ctfd.io", 30015);
            ssls.setSoTimeout(3000);
            ObjectOutputStream outs = new ObjectOutputStream(ssls.getOutputStream());
            ObjectInputStream ins = new ObjectInputStream(ssls.getInputStream());
            outs.writeObject(S);
            byte[] d = data;
            outs.writeObject(d);
            outs.flush();
            Boolean status = (Boolean) ins.readObject();
            if (status) {
                System.out.println("[!] Got the flag, but looks like it's encrypted...");
                byte[] in_1 = (byte[]) ins.readObject();
                System.out.println(in_1.length);
                byte[] in_2 = (byte[]) ins.readObject();
                System.out.println(in_2.length);
                System.out.println(new String(crypt(in_1, in_2)));
            } else {
                System.out.println("[!] I guess I didn't want a flag...");
            }

            outs.close();
            ins.close();
            ssls.close();

        } catch (UnknownHostException e) {
            System.out.println("Unable to contact host :'(");
        } catch (GeneralSecurityException | ClassNotFoundException | IOException e) {
            System.out.println("Error: " + e);
        }
    }

    public static byte[] crypt(byte[] S, byte[] data) {
        byte[] res = new byte[data.length];
        int i = 0;
        int j = 0;
        byte tmp;
        byte k;
        for (int x = 0; x < data.length; x++) {
            i = ((i + 1) % 256);
            j = ((j + (S[i] & 0xFF)) % 256);
            tmp = S[j];
            S[j] = S[i];
            S[i] = tmp;
            k = S[((S[i] & 0xFF) + (S[j] & 0xFF)) % 256];
            res[x] = (byte) (data[x] ^ k);
        }

        return res;
    }
}