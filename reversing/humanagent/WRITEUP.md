HUMANAGENT
==========

Her får man utlevert filen `minecraft_hack.jar` og bedt om å finne flagget.

Heldigvis for oss finnes det mange gode Java-decompilere, f.eks. [Procyon](https://github.com/ststeiger/procyon) og [Krakatau](https://github.com/Storyyeller/Krakatau). I denne writeupen er filen dekompilert med `Krakatau`.

Vi begynner med å pakke ut filen - en .jar-fil er bare en ZIP-fil - slik at vi får lest ut manifestet. Dette forteller oss hvor `main`-funksjonen ligger.

```
$ cat extracted/META-INF/MANIFEST.MF
Manifest-Version: 1.0
Main-Class: agent.human.Main

```

Vi dekompilerer og tar en titt på `agent/human/Main.java`. Her ser vi at flere funksjoner heter `ALLATORIxDEMO` som forteller oss at filen er obfuskert med [Allatori](http://www.allatori.com):

```java
System.out.println(agent.human.K.ALLATORIxDEMO("h.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A\u0007A-B
-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B.h.B-B-B-B-A.B.B-B.B-B-A.B.A.B.A.B.A-B.A.B-B-B-B-A\u0007A-B-B-B-A-A-A-
B-A-B-A-A-B.B-A-A-A-A-B.B-B-B-B-B.h.B-B-B-B.A.B.B-B.B-B.A.B-A-B.B.B.A-B-A-B-B-B-B-A\u0007A-B-B-B-A-A-A.A-A.A-A-A-B
.B-A.A-A-A-A.A-B-B-B-B.h.B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-A\u0007A--o\u0004x\u0011n\u0003y\u000bb\u00
0c-\u0000tBL\u000ea\u0003y\r\u007f\u000b--o\u0004x\u0011n\u0003y\r\u007fB{U#U-&H/BB.h.B-B-B-B-B-B-B-B-B-B-B-B-B-B-
B-B-B-B-B-B-B-B-B-A\u0007A-B-B-B-B-B-\ny\u0016}X\"Mz\u0015zLl\u000ea\u0003y\r\u007f\u000b#\u0001b\u000f-B-B-B-B-B-
B.h.B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-B-A\u0007A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.A.h"));
System.out.println(agent.human.K.ALLATORIxDEMO("V#J'C6-1Y#_6D,J?"));
agent.human.m.ALLATORIxDEMO();
new Thread((Runnable)(Object)new agent.human.c(new agent.human.K())).start();
agent.human.Main.ALLATORIxDEMO(agent.human.Main.ALLATORIxDEMO(), agent.human.c.ALLATORIxDEMO);
```

Vi ser at argument til `System.out.println` kommer fra `agent.human.K.ALLATORIxDEMO`, som dekrypterer input:

```java
public static String ALLATORIxDEMO(String s) {
    int i = s.length();
    char[] a = new char[i];
    int i0 = i - 1;
    while(true) {
        if (i0 >= 0) {
            int i1 = s.charAt(i0);
            int i2 = i0 + -1;
            a[i0] = (char)(int)(char)(i1 ^ 98);
            if (i2 >= 0) {
                i0 = i2 + -1;
                a[i2] = (char)(int)(char)((int)s.charAt(i2) ^ 13);
                continue;
            }
        }
        return new String(a);
    }
}
```

Streng-krypteringen til `Allatori` er rimelig enkel å reimplementere, og bruker to konstanter for XORing. I et sample kan man finne flere slike funksjoner som bruker forskjellige konstanter. Noen varianter vil også bruke klasse- og metodenavn på metoden som kaller dekryptering som nøkkel. Det er ingen slike i denne oppgaven.

Dersom vi kjører filen vil den snakke ut mot `challenges.ctfd.io` på port 30015. Den skriver til konsoll en variant av sangen "99 Bottles of Beer". Etter noen sekunders kjøring skriver den "[!] I guess I didn't want a flag...". 

Vi kan dekryptere alle strenger i samplet (reimplementere algoritmen over og kjøre på alle strenger - vi har to funksjoner som dekrypterer strenger som bruker forskjellige konstanter) og finner denne strengen i filen `c.java`. Rett over finner vi strengen "[!] Got the flag, but looks like it's encrypted...".

I samme fil finner vi også strengen `{"get_flag":0}`.

Vi ser at denne strengen sendes som parameter til en funksjon i `Main.java` og resultatet sendes, som et objekt, over nettverket:

```java
java.io.ObjectOutputStream a3 = new java.io.ObjectOutputStream(a2.getOutputStream());
java.io.ObjectInputStream a4 = new java.io.ObjectInputStream(a2.getInputStream());
byte[] a5 = agent.human.Main.ALLATORIxDEMO();
a3.writeObject((Object)a5);
a3.writeObject((Object)agent.human.Main.ALLATORIxDEMO(a5, ALLATORIxDEMO));
```

I `Main.java` finner vi en funksjon som genererer en tilfeldig krypteringsnøkkel på 16 tegn:

```java
public static byte[] c() {
    String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    byte[] a = new byte[16];
    int i = 0;
    int i0 = 0;
    while(i < 16) {
        int i1 = s.getBytes()[new java.util.Random().nextInt(s.length())];
        i = i0 + 1;
        a[i0] = (byte)i1;
        i0 = i;
    }
    return a;
}
```

En funksjon som implementerer `KSA` fra [RC4](https://en.wikipedia.org/wiki/RC4):

```java
public static byte[] ALLATORIxDEMO() {
    byte[] a = new byte[256];
    byte[] a0 = agent.human.Main.c();
    int i = 0;
    int i0 = 0;
    while(i < 256) {
        int i1 = (byte)i0;
        i = i0 + 1;
        a[i0] = (byte)i1;
        i0 = i;
    }
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    while(i2 < 256) {
        i3 = (i3 + ((int)a[i4] & 255) + (int)a0[i4 % 16]) % 256;
        int i5 = a[i3];
        a[i3] = (byte)(int)a[i4];
        int i6 = (byte)i5;
        i2 = i4 + 1;
        a[i4] = (byte)i6;
        i4 = i2;
    }
    return a;
}
```

Og en funksjon som tar inn `S` og data og krypterer / dekrypterer data (kryptering / dekryptering er symmetrisk, `RC4` bruker samme `S` for begge, i samme state):

```java
public static byte[] ALLATORIxDEMO(byte[] a, byte[] a0) {
    byte[] a1 = new byte[a0.length];
    int i = 0;
    int i0 = 0;
    int i1 = 0;
    int i2 = 0;
    while(i < a0.length) {
        i0 = (i0 + 1) % 256;
        i1 = (i1 + ((int)a[i0] & 255)) % 256;
        int i3 = a[i1];
        a[i1] = (byte)(int)a[i0];
        a[i0] = (byte)i3;
        int i4 = a[(((int)a[i0] & 255) + ((int)a[i1] & 255)) % 256];
        int i5 = (byte)((int)a0[i2] ^ i4);
        i = i2 + 1;
        a1[i2] = (byte)i5;
        i2 = i;
    }
    return a1;
}
```

Kombinert med det vi så i `c.class`, ser vi nå at vi genererer `S`, sender `S` over nettverket som et serialisert Java-objekt. Vi bruker `S` for å kryptere strengen `{"get_flag":0}`, og sender det krypterte bytearrayet over nettverket (som et serialisert Java-objekt).

Vi ser videre at vi leser ut en Boolean fra nettverket:

```java
if (((Boolean)a4.readObject()).booleanValue()) {
    System.out.println("[!] Got the flag, but looks like it's encrypted...");
    byte[] a6 = (byte[])a4.readObject();
    byte[] a7 = (byte[])a4.readObject();
} else {
    System.out.println("[!] I guess I didn't want a flag...");
```

Dersom verdien vi leser ut er `true` leser vi ut to bytearrays til, men ingenting mer skjer med dem i koden.

La oss skrive litt kode som sender `{"get_flag":1}` til serveren i stedet og se hva som skjer. Vi må altså:

1. Generere `S`.
2. Sende `S` til serveren.
3. Kryptere riktig streng, og sende denne til serveren.
4. Lese ut et Boolean-objekt.
5. Lese ut to bytearrays.
6. ???

```java
import javax.net.ssl.*;
import java.io.*;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

public class Solution {
    public static byte[] data = "{\"get_flag\":1}".getBytes();
    public static byte[] S = new byte[256];
    

    public static void main(String[] args) {
        Arrays.fill(S, (byte)0x00);
        TrustManager trustAllCerts = new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            X509Certificate[] certs, String authType) {
                    }
                };
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{trustAllCerts}, new java.security.SecureRandom());
            SSLSocket ssls = (SSLSocket) sc.getSocketFactory().createSocket("challenges.ctfd.io", 30015);
            ssls.setSoTimeout(3000);
            ObjectOutputStream outs = new ObjectOutputStream(ssls.getOutputStream());
            ObjectInputStream ins = new ObjectInputStream(ssls.getInputStream());
            outs.writeObject(S);
            byte[] d = data;
            outs.writeObject(d);
            outs.flush();
            Boolean status = (Boolean) ins.readObject();
            if (status) {
                System.out.println("[!] Got the flag, but looks like it's encrypted...");
                byte[] in_1 = (byte[]) ins.readObject();
                byte[] in_2 = (byte[]) ins.readObject();
            } else {
                System.out.println("[!] I guess I didn't want a flag...");
            }

            outs.close();
            ins.close();
            ssls.close();

        } catch (UnknownHostException e) {
            System.out.println("Unable to contact host :'(");
        } catch (GeneralSecurityException | ClassNotFoundException | IOException e) {
            System.out.println("Error: " + e);
        }
    }
}
```

Som dere ser her så [har me juksa litt](https://no.wikipedia.org/wiki/Ingrid_Espelid_Hovig): hvis vi bruker en `S` med bare nullbytes (`Arrays.fill(S, (byte)0x00)`) kan vi sende data i klartekst og det dekrypteres til seg selv.

Kjører vi koden over viser det seg at vi får ut `true` fra serveren, men hva er i dataene vi får ut?
Hvis vi printer lengden på dem ser vi at vi får ut et array på 256 bytes og ett på 37. Dette lukter en ny `S` og et kryptert flagg!

Vi implementerer `PRGA` fra `RC4` og forsøker å dekryptere dataene:

```java
public static byte[] crypt(byte[] S, byte[] data) {
    byte[] res = new byte[data.length];
    int i = 0;
    int j = 0;
    byte tmp;
    byte k;
    for (int x = 0; x < data.length; x++) {
        i = ((i + 1) % 256);
        j = ((j + (S[i] & 0xFF)) % 256);
        tmp = S[j];
        S[j] = S[i];
        S[i] = tmp;
        k = S[((S[i] & 0xFF) + (S[j] & 0xFF)) % 256];
        res[x] = (byte) (data[x] ^ k);
    }

    return res;
}
```

Vi endrer litt på koden vår:
```java
if (status) {
    System.out.println("[!] Got the flag, but looks like it's encrypted...");
    byte[] in_1 = (byte[]) ins.readObject();
    byte[] in_2 = (byte[]) ins.readObject();
    System.out.println(new String(crypt(in_1, in_2)));
} else {
    System.out.println("[!] I guess I didn't want a flag...");
}
```

```bash
$ javac Solution.java
$ java Solution
[!] Got the flag, but looks like it's encrypted...
EGG{ff9f37f9b649a0f83f291d59efe0eead}
```