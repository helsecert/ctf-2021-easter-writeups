Kontorarbeider Herman von Osterliebhaber merker plutselig at noe er galt med jobblaptoppen hans. Den interne IT-avdelingen finner filen `minecraft_hack.jar`, og Herman må innrømme at han lånte laptoppen til sin sønn som er en ivrig gamer.

Ved kjøring i sandkasse ser det ut som om filen prøver å koble til et sted over TLS, men det ser ikke ut som om den mottar helt riktige data. Kanskje den sender noe feil?

Kan du analysere filen og lure serveren til å sende riktige data tilbake?

Passord til fil: infected
