Denne oppgaven er kanskje litt rørete(pun intended). Det som har skjedd i stekeprosessen er at hver ingrediens deles opp i ei liste med bokstaver som konverteres til ascii decimal verdien. Eks. `[69, 103, 103, 101, 112, 108, 111, 109, 109, 101]`, deretter til hexadecimal Eks. `[45, 67, 67, 65, 70, 6c, 6f, 6d, 6d, 65]`.
Dette gjøres også med "Salt" som i første omgang er `"noSaltToday"` og videre blir "Salt" forrige ingrediens. 

*Oppgave*
```js
const stekeovn = s => {
    const kutt = text => text.split('').map(c => c.charCodeAt(0)); // [69, 103, 103, 101, 112, 108, 111, 109, 109, 101]
    const fordoy = n => ("0" + Number(n).toString(16)).substr(-2); // [45, 67, 67, 65, 70, 6c, 6f, 6d, 6d, 65]
    const krydre = krydder => kutt(s).reduce((a,b) => a ^ b, krydder); // [45, 67, 67, 65, 70, 6c, 6f, 6d, 6d, 65] ^ "Salt"
    
    return s => s.split('')
        .map(kutt)
        .map(krydre)
        .map(fordoy)
        .join('');
}

var ingredienser = [
  "???",
  "???",
  "???",
  "???",
  "???",
  "???",
  "???"
]
for(let i = 0; i < ingredienser.length; i++){
  var steke = stekeovn(ingredienser[i-1] ? ingredienser[i-1] : 'noSaltToday') 
  ingredienser[i] = steke(ingredienser[i])
 
}

console.log(ingredienser)
```
*Løsning Python*
```python
import functools
kake = [  
    '290b0b091c0003010109',  
    '113b283e3f373537373f',  
    '43f764',  
    '07353820',  
    '586d78786d7a',  
    '163a2928322b3a35',  
    '1416162a19343d3d3e2c'  
]  
kake = ["noSaltToday"] + kake
for i in range(0, len(kake)-1):  
    xorkey = functools.reduce(lambda a,b : a^b, list(map(ord, list(kake[i]))))  
    out = "".join([chr(k^xorkey) for k in bytes.fromhex(kake[i+1])])  
    print(out)
```

*Løsning Javascript*
```js
const dekrypter = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0)); //Gjør om en streng til ascii decimal array "text" => [116, 101, 120, 116]
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code); // XOR salt[i] ^ ingrediens[i]
    return encoded => encoded.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16)) //Fra hexadecimal
        .map(applySaltToChar) //XOR
        .map(charCode => String.fromCharCode(charCode)) //Fra ascii decimal => ascii
        .join(''); // Array => streng
}

let kake = [
  'noSaltToday', // Vi legger til "noSaltToday" i kake lista
  '290b0b091c0003010109',
  '113b283e3f373537373f',
  '43f764',
  '07353820',
  '586d78786d7a',
  '163a2928322b3a35',
  '1416162a1f30213e3d343e3f223a303a342c'
]

for(let i = 0; i < kake.length-1; i++){
  let dekrypt = dekrypter(kake[i])
  console.log(dekrypt(kake[i+1]))
}
```
