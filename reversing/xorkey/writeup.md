Vi har XOR kryptert et flagg og oppgaven er å finne xornøkkelen (for å dekryptere flagget).

Det er to løsninger:
- reverse algoritmen
- patche binarien slik at den printer ut nøkkelen.

Det krypterte flagget ligger i binarien som hex `flag: 6c727a3c2a373e064a414f222115346a6113100c08416a3321081c515a230d58`.

# Primtall som hint
Siden alle flagg i CTFen starter med og slutter med `EGG{}` kan vi finne de 4 første og den siste byten i xornøkkelen:
```
>>> from pwn import xor
>>> xor(bytes.fromhex("6c727a3c58"), b"EGG{}")
b')5=G%'
```

Her ser vi allerede tegn på at xornøkkelen er ASCII printbar. Dette er en viktig observasjon som vi får bruk for senere for å validere om starten på xornøkkelen er riktig. Noen gjenkjente kanskje allerede her at disse ASCII tegnene har ASCII verdier som også er primtall:
```
>>> from Crypto.Util.number import isPrime
>>> isPrime(ord(")"))
1
>>> isPrime(ord("5"))
1
>>> isPrime(ord("="))
1
>>> isPrime(ord("G"))
1
>>> isPrime(ord("%"))
1
``` 

I binarien finner vi 3 funksjoner hvor funksjonen på `0x08049000` sjekker om et tall fra argumentet (esp+0xc) er et primtall. Vi kan se det både i ASM, men kanskje enklere i Ghidra. Input blir sjekka om det er modulo en tall i rekken `2, 3, 5, 7, 11, 13, 19, 23 ..`. Dette er en primitiv form for å sjekke om et heltall er et primtall:
```
undefined4 FUN_08049000(undefined4 param_1,undefined4 param_2,uint param_3)
{
  uint i;
  
  if ((param_3 % 2 != 0) && (param_3 % 3 != 0)) {
    i = 5;
    while( true ) {
      if ((int)param_3 < (int)(i * 2)) {
        return 1;
      }
      if ((param_3 % i == 0) || (param_3 % (i + 2) == 0)) break;
      i = i + 6;
    }
  }
  return 0;
}
```

Ekvivalenten i python er:
```python
def isPrime(k):
    if k % 2 == 0:
        return False
    if k % 3 == 0:
        return False

    i = 5
    while i*i <= k:
        if k % i == 0:
            return False
        if k % (i + 2) == 0:
            return False
        i += 6

    return True
```

Det ligger også en liste i .data (DAT_0804b0a5) som kun inneholder primtall `2,3,5,7,9,11,13,17,19,23,29,31` som benyttes i keygen funksjonen på `0x080490db`


# Alt1: reverse algoritmen for å lage xornøkkel

I Ghidra finner vi 4 funksjoner:
```
entry
FUN_08049000
FUN_08049033
FUN_080490db
```

entry har en if-kondisjon som kaller FUN_08049033.

FUN_08049000 er en funksjon som tester om et tall er et primtall eller ikke.

FUN_08049033 som er en lurefunskjon. Den printer bare ut "Hello Mr. Anderson, we miss you"

FUN_080490db er selve nøkkelgeneratoren. En if-kondisjon i bunn, som alltid er sann, overskriver nøkkelen med `dette_er_ikke_riktig_XOR_n0kkel!`. Hvis vi fjerner denne og rydder litt opp i variablene ser det slik ut (ligger en int 0x80 som skriver ut key i bunn):
```
void FUN_080490db(void)

{
  code *pcVar1;
  int key_len;
  uint is_prime;
  int next_key;
  int keyPtr;
  int primes_arr_ptr;
  int key_ptr;

  key_len = 0x20;
  primes_arr_ptr = 3;
  next_key = 0x20;
  key_ptr = 0;
  do {
    keyPtr = key_ptr;
    next_key = next_key + *(int *)(&DAT_0804b0a5 + primes_arr_ptr * 4);
    while( true ) {
      is_prime = FUN_08049000();
      if (true) {
        is_prime = is_prime ^ 1;
      }
      if (is_prime != 0) break;
      next_key = next_key + 1;
      if (0x7e < next_key) {
        next_key = 0x20;
        primes_arr_ptr = primes_arr_ptr + 1;
      }
    }
    *(int *)(&DAT_0804b084 + keyPtr) = next_key;
    key_len = key_len + -1;
    key_ptr = keyPtr + 1;
  } while (key_len != 0);
  (&DAT_0804b085)[keyPtr] = 10;

  ...  
}
```

Algoritmen bygger en xornøkkel ved å velge ut ASCII tegn mellom 0x20 og 0x7e hvor ASCII verdien er et primtall. Mengden mulige tegn i xornøkkel er derfor kun `%)+/5;=CGIOSYaegkm`.

Den appender `next_key` til xornøkkelen (`key_ptr`) hvis next_key er et primtall. Hvis next_key overgår 0x7e starter den på nytt med 0x20. Basen i økningen fra foregående next_key til neste styres av listen med primtall (`DAT_0804b0a5` som nevnt tidligere) via `primes_arr_ptr`. Dette for å unngå at nøkkelen blir for repeterende.

Det ble lagt inn en liten luring i nøkkelgenereringen, nemmelig `if (true) { is_prime ^= 0x1 }`. Dette er en felle som XORer resultatet fra is_prime med 0x1. Den må fjernes for at algoritmen skal fungere riktig. Det er kanskje enklere å se dette direkte i assemblyen enn i disassembleren i Ghidra. Hvordan kan vi vite det? Hvis vi ikke fjerner dette vil ikke de første 4 bytene i xornøkkelen sammenfalle med det vi fant tidligere, altså at xornøkkelen skal starte med `)5=G`.

Vi kan gjøre en test for å se om vi får ut den samme starten som vi fant tidligere. Husk at `primes_arr_ptr` starter på det fjerde elementet `7`. Vi kan finne det neste primtallet etter `next_key = 0x20 + 7`. Dette er første byte i xornøkkelen. Neste byte finner vi ved å legge til neste element fra listen med primtall `9` og søke etter neste primtall.
```
>>> from Crypto.Util.number import isPrime
>>> next_key = 0x20
>>> for i in range(0x7e):
...     if isPrime(next_key + 7 + i):
...         break
...
>>> next_key = next_key + 7 + i
>>> print(chr(next_key))
')'
>>> for i in range(0x7e):
...     if isPrime(next_key + 9 + i):
...         break
...
>>> chr(next_key + 9 + i)
'5'
```

Vi ser allerede at vi nå har fått ut `)5` som sammenfaller godt. Xornøkkelen finner vi ved å kjøre algoritmen lik lengden av flagget (32dec/0x20). Vi ser at nøkkelen slutter med samme tegn `%` som vi fant tidligere.
```
)5=GOYak%/;GSak%5COam%5CSaq%;Oa%
```

Her er algoritmen i python før den ble skrevet i ASM:
```python
def keygen(l):
    k = 0x20
    key = ""
    add = [2,3,5,7,9,11,13,17,19,23,29,31]
    ai = 3
    for f in range(l):
        k += add[ai]
        while isPrime(k) == False:
            k += 1
            if k > 0x7e:
                k = 0x20
                ai += 1
        key += chr(k)
    return key
```


# Alt2: patche slik at nøkkelen printes ut

Oppsummert:
1) en if i entry0 er alltid sann og hopper derfor over `call`
2) call fra entry0 peker til feil funksjon, den printer bare ut `Hello Mr. Anderson, we miss you`
3) (riktig funksjon) keygen har en if som alltid er usann i bunn som overskriver nøkkel med `dette_er_ikke_riktig_XOR_n0kkel!`
4) en if som alltid er usann i selve keygen-algoritmen gjør at resultatet fra isPrime blir XORet med 0x1.

Hvis disse 4 tingene patches vil binarien skrive ut riktig nøkkel.

Alternativet er å patche kun 3) og 4) og kalle keygen funksjonen på `0x080490db` direkte i en debugger (gdb,radare2++).


## Patch1
I entry0 ligger det en `je` som skipper et funksjonskall. Den bruker et minneområde (ds:0x804b084) som ikke er satt, og er derfor 0x0. je (jump if equal) vil derfor hoppe over call og ingen ting skjer, og programmet går til exit.
```
 80492ee:       8a 15 84 b0 04 08       mov    dl,BYTE PTR ds:0x804b084
 80492f4:       80 fa 00                cmp    dl,0x0
 80492f7:       74 05                   je     80492fe <_start+0x67>
 80492f9:       e8 35 fd ff ff          call   8049033 <_start-0x264>

```

Vi kan f.eks. patche cmp til å heller sjekke 0x1. Da blir sjekken `jump if equal 0x0 == 0x1` som er usann, og vi får EIP til 0x80492f9.
```
✗ r2 -d ./xorkey
...
s 0x080492f4+2 
wv1 0x01
write
exit
```

## Patch2
Nå som call kjøres mot `0x8049033` treffer vi en funksjon som setter 32byte i key og printer ut `Hello Mr. Anderson, we miss you`.

```
✗ ./xorkey 
Flagget er xor-kryptert med nøkkelen i binaryen.
Hello Mr. Anderson, we miss you
```

Hvis vi ser mer i fila vil vi finne at det er mye kode som er urørt. Vi kan finne keygen enten i Ghidra eller vi kan bare lese ASM'en direkte ut fra objdump. Det er to function prologs, og den ene er den som call peker mot (0x8049033) mens den andre på adresse `0x80490db` blir aldri kjørt.
```
✗ objdump -drwC -Mintel xorkey | grep "push   ebp" -A 1
 8049033:       55                      push   ebp
 8049034:       89 e5                   mov    ebp,esp
--
 80490db:       55                      push   ebp
 80490dc:       89 e5                   mov    ebp,esp
```

Vi kan følge flyten dit ved å patche `call`. ASM `e8 35 fd ff ff` disassembler til `call 0xfffffd3a`. Call med `e8` bruker et relativt offset. Vi kan legge til forskjellen på adressene `(0x80490db - 0x8049033 =) 168` for å få `(0xfffffd3a + 168 =) 0xfffffddd`, og patche dette til `e8 dd fd ff ff` (husk endianess!)

```
s 0x080492f9+1
wv4 0xfffffddd
write
exit

✗ ./xorkey      
Flagget er xor-kryptert med nøkkelen i binaryen.
dette_er_ikke_riktig_XOR_n0kkel!
```

## Patch3
Neste nøkkel er jo ikke riktig, som den sier. Hvis vi følger flyten inn i funksjonen `0x80490db` ser vi at det er en kondisjon på `0x08049194` som overskriver nøkkelen hvis `0x1f != 0x88`. Ved å patche slik at disse blir lik hopper vi over denne biten.
```
 8049192:       31 ff                   xor    edi,edi
 8049194:       b2 1f                   mov    dl,0x1f
 8049196:       80 fa 88                cmp    dl,0x88
 8049199:       0f 84 e0 00 00 00       je     804927f <_start-0x18>
 
s 0x08049194+1
wv1 0x88
write
exit

✗ ./xorkey 
Flagget er xor-kryptert med nøkkelen i binaryen.
'.6>ELT[bipw~
```

Vi får nå ut "noe", men det er ikke 32byte lesbart. Vi kunne pipet kjøringen av xorkey til f.eks. xxd eller hexdump for å se hele 32byte nøkkelen. 

Man kan tenke seg at dette må være riktig xornøkkel, men den er feil. Hvordan kan vi vite det? Som tidligere nevnt er det flere hint og ting man kunne funnet ut for å se dette: xornøkkel må være ASCII printbar og den skal starte med `)5=G`. Hvis vi forsøker å bruke xornøkkelen som kommer ut nå, vil vi ikke få flagget til å starte med `EGG{`.

Videre analyser i algoritmen gjør at vi finner den siste kondisjonen ved `0x08049156` som forårsaker at nøkkelen er feil. En if-kondisjon som alltid er usann ved `0x42 != 0x41` gjør at svaret (`eax`) fra kallet til `0x8049000` (dette er en `isPrime` funksjonen) blir XORet med 1. Hvis man åpner Ghidra og ser på koden der vil man se en `if (true) { ..}`. Vi patcher ut denne og finner xornøkkel og flagget:
```
 8049151:       e8 aa fe ff ff          call   8049000 <_start-0x297>
 8049156:       b2 42                   mov    dl,0x42
 8049158:       80 fa 41                cmp    dl,0x41
 804915b:       74 07                   je     8049164 <_start-0x133>
 804915d:       bf 01 00 00 00          mov    edi,0x1
 8049162:       31 f8                   xor    eax,edi

s 0x08049156+1
wv1 0x41
write
exit

✗ ./xorkey 
Flagget er xor-kryptert med nøkkelen i binaryen.
)5=GOYak%/;GSak%5COam%5CSaq%;Oa%

✗ strings xorkey | grep flag
flag: 6c727a3c2a373e064a414f222115346a6113100c08416a3321081c515a230d58

✗ python
>>> from pwn import xor
>>> print(xor(bytes.fromhex("6c727a3c2a373e064a414f222115346a6113100c08416a3321081c515a230d58"), b")5=GOYak%/;GSak%5COam%5CSaq%;Oa%"))
b'EGG{en_montert_OTP_med_primtall}'
```

# ASM

```asm
global _start

section .flag
    f dd "flag: 6c727a3c2a373e064a414f222115346a6113100c08416a3321081c515a230d58",0xa

section .viktig_melding
    velkommen dd "Flagget er xor-kryptert med nøkkelen i binaryen.", 0xa

section .data
    key times 0x21 db 0   ; we store the 32byte key + 1 byte newline
    a dd 2,3,5,7,9,11,13,17,19,23,29,31

section .text
    global _start

_is_prime:                ; arg1: number @ esp+0cx
    ; modulo A/B     = Q   remainder R
    ; modulo eax/ebx = eax remainder edx
    ; divident eax
    ; divisor  ebx
    ; quotient eax
    ; remainder edx 
   
    mov eax, [esp+0xc]    ; divident eax = number
    mov ebx, 2            ; divisor  ebx = 2
    xor edx, edx          ; must be set to zero before DIV instr
    div ebx               ; quotient eax, remainder edx
    cmp edx, 0             
    je _prime_false       ; number % 2 == 0, return false

    mov eax, [esp+0xc]
    mov ebx, 3
    xor edx, edx
    div ebx
    cmp edx, 0
    je _prime_false       ; number % 3 == 0, return false

    mov ecx, 5            ; i = 5
    jmp _while

_decoy_f1:
    push ebp          ; function prologue
    mov ebp,esp 
    mov byte[key+1], 0x65
    mov byte[key+20], 0x77
    mov byte[key+17], 0x6e
    mov byte[key+28], 0x79
    mov byte[key+21], 0x65
    mov byte[key+15], 0x73
    mov byte[key+8], 0x2e


    jmp _decoy_f2

_decoy_f4:
    mov byte[key+9], 0x20
    mov byte[key+7], 0x72
    mov byte[key+0], 0x48
    mov byte[key+5], 0x20
    mov byte[key+12], 0x64
    mov byte[key+4], 0x6f
    jmp _print

    ; while i*i <= number (esp+0xc)
_while:
    mov eax, ecx
    mov ebx, 2
    mul ebx               ; ebx = i*i
    mov ebx, eax
    mov eax, [esp+0xc]    ; eax = number
    cmp ebx, eax
    jg _prime_true        ; i*i > k, skip, return true
 
    ;mov eax, [esp+0xc]    ; eax is already number
    mov ebx, ecx
    xor edx, edx
    div ebx
    cmp edx, 0
    je _prime_false       ; number % i == 0, return false 

    mov eax, [esp+0xc]
    mov ebx, ecx
    add ebx, 2            ; ebx = i + 2
    xor edx, edx
    div ebx
    cmp edx, 0             
    je _prime_false       ; number % (i+2) == 0, return false

    add ecx, 6            ; i += 6
    jmp _while            ; continue

_prime_true:
        xor eax,eax
        mov eax, 1            ; true
        ret
 
_prime_false:
        xor eax, eax          ; false
        ret 

_keygen_start:
    push ebp          ; function prologue
    mov ebp,esp 
   
    mov eax, 32       ; key_length
    mov ebx, 3        ; primes_arr (pointer)
    mov ecx, 0x20     ; k (pointer)
    mov edx, 0        ; key (pointer)

    jmp _outer_loop

    _decoy_f2:
        mov byte[key+18], 0x2c
        mov byte[key+23], 0x6d
        mov byte[key+25], 0x73
        mov byte[key+14], 0x72
        mov byte[key+27], 0x20
        mov byte[key+26], 0x73
        mov byte[key+19], 0x20
        mov byte[key+11], 0x6e
        mov byte[key+31], 0xa
        mov byte[key+6], 0x4d
        mov byte[key+13], 0x65
        jmp _decoy_f3

    _outer_loop:
        add ecx, [a + 4*ebx]    ; k += primes_arr[ai]

        _inner_loop:

            push edx
            push ecx
            push ebx
            push eax
            call _is_prime ; false (eax = 0) or true (eax = 1)

            ; lock: toggle return from isPrime
            mov dl, 0x42
            cmp dl, 0x41
            je _no_toggle
            mov edi, 1       ; toggle LSB in eax
            xor eax, edi
             
            _no_toggle:
            cmp eax, 0        
           
            pop eax
            pop ebx
            pop ecx
            pop edx


            jne _set          ; is_prime(k) == true, jmp _set

            add ecx, 1        ; i += 1 

            mov esi, 0x7e     ; let's borrow esi :D
            cmp ecx, esi
            jle _inner_loop   ; i <= 0x7e, continue

            mov ecx, 0x20     ; start over
            inc ebx           ; next primes_arr
            jmp _inner_loop

    _set:
        mov [key + edx], ecx ; set key[edx]
        inc edx              ; inc key pointer
        dec eax              ; i -= 1    
        jnz _outer_loop      ; if key_length > 0, continue

    ; add newline
    mov byte [key + edx], 10

    ; lock: overwrite the key
    xor edi,edi
    mov dl, 0x1f
    cmp dl, 0x88
    je _print
	    mov byte [key + 0], 0x64
	    mov byte [key + 1], 0x65
	    mov byte [key + 2], 0x74
	    mov byte [key + 3], 0x74
	    mov byte [key + 4], 0x65
	    mov byte [key + 5], 0x5f
	    mov byte [key + 6], 0x65
	    mov byte [key + 7], 0x72
	    mov byte [key + 8], 0x5f
	    mov byte [key + 9], 0x69
	    mov byte [key + 10], 0x6b
	    mov byte [key + 11], 0x6b
	    mov byte [key + 12], 0x65
	    mov byte [key + 13], 0x5f
	    mov byte [key + 14], 0x72
	    mov byte [key + 15], 0x69
	    mov byte [key + 16], 0x6b
	    mov byte [key + 17], 0x74
	    mov byte [key + 18], 0x69
	    mov byte [key + 19], 0x67
	    mov byte [key + 20], 0x5f
	    mov byte [key + 21], 0x58
	    mov byte [key + 22], 0x4f
	    mov byte [key + 23], 0x52
	    mov byte [key + 24], 0x5f
	    mov byte [key + 25], 0x6e
	    mov byte [key + 26], 0x30
	    mov byte [key + 27], 0x6b
	    mov byte [key + 28], 0x6b
	    mov byte [key + 29], 0x65
	    mov byte [key + 30], 0x6c
	    mov byte [key + 31], '!'

    _print:
        ; debug print
        mov eax, 4
        mov ebx, 1
        mov ecx, key
        mov edx, 0x21
        int 0x80

    leave             ; function epilogue
    ret

_start:
    ; syscall write STDOUT velkommen
    mov eax, 4
    mov ebx, 1
    mov ecx, velkommen
    mov edx, 0x35
    int 0x80
   
    jmp _check

_decoy_f3:
    mov byte[key+24], 0x69
    mov byte[key+22], 0x20
    mov byte[key+2], 0x6c
    mov byte[key+3], 0x6c
    mov byte[key+29], 0x6f
    mov byte[key+16], 0x6f
    mov byte[key+10], 0x41
    mov byte[key+30], 0x75
    jmp _decoy_f4

_check:
    ; lock: wrong function
    xor edi,edi
    mov dl, [key+0]
    cmp dl, 0
    je _exit

    call _decoy_f1

_exit:
    ; syscall exit(0)
    mov eax, 1
    xor ebx,ebx
    int 0x80
```

# Flag

EGG{en_montert_OTP_med_primtall}

