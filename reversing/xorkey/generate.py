from Crypto.Util.number import isPrime as iP
from pwn import xor

FLAG="EGG{en_montert_OTP_med_primtall}"

def isPrime(k):
    if k % 2 == 0:
        return False
    if k % 3 == 0:
        return False

    i = 5
    while i*i <= k:
        if k % i == 0:
            return False
        if k % (i + 2) == 0:
            return False
        i += 6
    return True

def keygen(l, primeFn):
    k = 0x20
    key = ""
    primes_arr = [2,3,5,7,9,11,13,17,19,23,29,31]
    ai = 3
    for _ in range(l):
        k += primes_arr[ai]
        while isPrime(k) == False:
            k += 1
            if k > 0x7e:
                k = 0x20
                ai += 1
        key += chr(k)
    return key

key = keygen(len(FLAG), isPrime)
key2 = keygen(len(FLAG), iP)
print("key: ", key, len(key))
print("key2:", key, len(key))
key3 = ")5=GOYak%/;GSak%5COam%5CSaq%;Oa%" # from binary
print("     ", key3, len(key3))
assert key == key2
assert key == key3

print("flag:", FLAG, len(key))
enc_flag = xor(FLAG, key)
print("enc_flag:", enc_flag.hex(), len(enc_flag))
#flg = enc_flag.hex()
#print(", ".join([str(hex(x)) for x in flg]))
print("dec_flag:", xor(enc_flag, key))


decoys = []
for i, b in enumerate(b"Hello Mr. Anderson, we miss you\x0a"):
    decoys.append(f"    mov byte [key+{i}], {hex(b)}")
import random
random.shuffle(decoys)
for d in decoys:
    print(d)

for i, b in enumerate(b"dette_er_ikke_riktig_XOR_n0kkel"):
    print(f"    mov byte [key + {i}], {hex(b)}")
