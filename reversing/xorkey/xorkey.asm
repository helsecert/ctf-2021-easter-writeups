global _start

section .flag
    f dd "flag: 6c727a3c2a373e064a414f222115346a6113100c08416a3321081c515a230d58",0xa

section .viktig_melding
    velkommen dd "Flagget er xor-kryptert med nøkkelen i binaryen.", 0xa

section .data
    key times 0x21 db 0   ; we store the 32byte key + 1 byte newline
    a dd 2,3,5,7,9,11,13,17,19,23,29,31
;    d times 3 dd 0

section .text
    global _start

_is_prime:                ; arg1: number @ esp+0cx
    ; modulo A/B     = Q   remainder R
    ; modulo eax/ebx = eax remainder edx
    ; divident eax
    ; divisor  ebx
    ; quotient eax
    ; remainder edx 
   
    mov eax, [esp+0xc]    ; divident eax = number
    mov ebx, 2            ; divisor  ebx = 2
    xor edx, edx          ; must be set to zero before DIV instr
    div ebx               ; quotient eax, remainder edx
    cmp edx, 0             
    je _prime_false       ; number % 2 == 0, return false

    mov eax, [esp+0xc]
    mov ebx, 3
    xor edx, edx
    div ebx
    cmp edx, 0
    je _prime_false       ; number % 3 == 0, return false

    mov ecx, 5            ; i = 5
    jmp _while

_decoy_f1:
    push ebp          ; function prologue
    mov ebp,esp 
    mov byte[key+1], 0x65
    mov byte[key+20], 0x77
    mov byte[key+17], 0x6e
    mov byte[key+28], 0x79
    mov byte[key+21], 0x65
    mov byte[key+15], 0x73
    mov byte[key+8], 0x2e


    jmp _decoy_f2

_decoy_f4:
    mov byte[key+9], 0x20
    mov byte[key+7], 0x72
    mov byte[key+0], 0x48
    mov byte[key+5], 0x20
    mov byte[key+12], 0x64
    mov byte[key+4], 0x6f
    jmp _print

    ; while i*i <= number (esp+0xc)
_while:
    mov eax, ecx
    mov ebx, 2
    mul ebx               ; ebx = i*i
    mov ebx, eax
    mov eax, [esp+0xc]    ; eax = number
    cmp ebx, eax
    jg _prime_true        ; i*i > k, skip, return true
 
    ;mov eax, [esp+0xc]    ; eax is already number
    mov ebx, ecx
    xor edx, edx
    div ebx
    cmp edx, 0
    je _prime_false       ; number % i == 0, return false 

    mov eax, [esp+0xc]
    mov ebx, ecx
    add ebx, 2            ; ebx = i + 2
    xor edx, edx
    div ebx
    cmp edx, 0             
    je _prime_false       ; number % (i+2) == 0, return false

    add ecx, 6            ; i += 6
    jmp _while            ; continue

_prime_true:
        xor eax,eax
        mov eax, 1            ; true
        ret
 
_prime_false:
        xor eax, eax          ; false
        ret 

_keygen_start:
    push ebp          ; function prologue
    mov ebp,esp 
   
    mov eax, 32       ; key_length
    mov ebx, 3        ; primes_arr (pointer)
    mov ecx, 0x20     ; k (pointer)
    mov edx, 0        ; key (pointer)

    jmp _outer_loop

    _decoy_f2:
        mov byte[key+18], 0x2c
        mov byte[key+23], 0x6d
        mov byte[key+25], 0x73
        mov byte[key+14], 0x72
        mov byte[key+27], 0x20
        mov byte[key+26], 0x73
        mov byte[key+19], 0x20
        mov byte[key+11], 0x6e
        mov byte[key+31], 0xa
        mov byte[key+6], 0x4d
        mov byte[key+13], 0x65
        jmp _decoy_f3

    _outer_loop:
        add ecx, [a + 4*ebx]    ; k += primes_arr[ai]

        _inner_loop:

            push edx
            push ecx
            push ebx
            push eax
            call _is_prime ; false (eax = 0) or true (eax = 1)

            ; lock: toggle return from isPrime
            mov dl, 0x42
            cmp dl, 0x41
            je _no_toggle
            mov edi, 1       ; toggle LSB in eax
            xor eax, edi
             
            _no_toggle:
            cmp eax, 0        
           
            pop eax
            pop ebx
            pop ecx
            pop edx


            jne _set          ; is_prime(k) == true, jmp _set

            add ecx, 1        ; i += 1 

            mov esi, 0x7e     ; let's borrow esi :D
            cmp ecx, esi
            jle _inner_loop   ; i <= 0x7e, continue

            mov ecx, 0x20     ; start over
            inc ebx           ; next primes_arr
            jmp _inner_loop

    _set:
        mov [key + edx], ecx ; set key[edx]
        inc edx              ; inc key pointer
        dec eax              ; i -= 1    
        jnz _outer_loop      ; if key_length > 0, continue

    ; add newline
    mov byte [key + edx], 10

    ; lock: overwrite the key
    xor edi,edi
    mov dl, 0x1f
    cmp dl, 0x88
    je _print
	    mov byte [key + 0], 0x64
	    mov byte [key + 1], 0x65
	    mov byte [key + 2], 0x74
	    mov byte [key + 3], 0x74
	    mov byte [key + 4], 0x65
	    mov byte [key + 5], 0x5f
	    mov byte [key + 6], 0x65
	    mov byte [key + 7], 0x72
	    mov byte [key + 8], 0x5f
	    mov byte [key + 9], 0x69
	    mov byte [key + 10], 0x6b
	    mov byte [key + 11], 0x6b
	    mov byte [key + 12], 0x65
	    mov byte [key + 13], 0x5f
	    mov byte [key + 14], 0x72
	    mov byte [key + 15], 0x69
	    mov byte [key + 16], 0x6b
	    mov byte [key + 17], 0x74
	    mov byte [key + 18], 0x69
	    mov byte [key + 19], 0x67
	    mov byte [key + 20], 0x5f
	    mov byte [key + 21], 0x58
	    mov byte [key + 22], 0x4f
	    mov byte [key + 23], 0x52
	    mov byte [key + 24], 0x5f
	    mov byte [key + 25], 0x6e
	    mov byte [key + 26], 0x30
	    mov byte [key + 27], 0x6b
	    mov byte [key + 28], 0x6b
	    mov byte [key + 29], 0x65
	    mov byte [key + 30], 0x6c
	    mov byte [key + 31], '!'

    _print:
        ; debug print
        mov eax, 4
        mov ebx, 1
        mov ecx, key
        mov edx, 0x21
        int 0x80

    leave             ; function epilogue
    ret

_start:
    ; syscall write STDOUT velkommen
    mov eax, 4
    mov ebx, 1
    mov ecx, velkommen
    mov edx, 0x35
    int 0x80
   
    jmp _check

_decoy_f3:
    mov byte[key+24], 0x69
    mov byte[key+22], 0x20
    mov byte[key+2], 0x6c
    mov byte[key+3], 0x6c
    mov byte[key+29], 0x6f
    mov byte[key+16], 0x6f
    mov byte[key+10], 0x41
    mov byte[key+30], 0x75
    jmp _decoy_f4

_check:
    ; lock: wrong function
    xor edi,edi
    mov dl, [key+0]
    cmp dl, 0
    je _exit

    call _decoy_f1

_exit:
    ; syscall exit(0)
    mov eax, 1
    xor ebx,ebx
    int 0x80
