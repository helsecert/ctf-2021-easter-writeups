CLEAR CARACAL
=============

Flagget ligger kryptert i en utlevert PE32+-fil og man har mange veier til mål.


Statisk
-------
~~Hvorfor vil du deg selv så v--~~ Statisk analyse er kjempegøy!

Avhengig av hvilket verktøy man bruker risikerer man at verktøyet ikke korrekt identifiserer `main`-funksjonen i filen, men dumper deg rett til entry point som er kode lenket inn av MinGW: [`crt0`](https://en.wikipedia.org/wiki/Crt0).

For denne (og de andre PE32+-filene i CTFen) kan vi gå til det siste funksjonkallet i den andre funksjonen som kalles fra entry point for å finne `main`-funksjonen.

Alternativt kan vi jobbe bakover - se på interessante imports i `IAT`. Her ser vi flere funksjoner vi kan bruke for å jobbe oss bakover til mer spennende deler av koden, som for eksempel `InternetOpenA` og `HttpSendRequestA`. Vi ser at disse kalles fra samme funksjon, og det viser seg at dette er `main`-funksjonen vi finner når vi går gjenom `crt0` også. 
Når vi kikker på denne finner vi en interessant loop rett før `HttpSendRequestA`:

![Interessant loop](loop_decrypt.png)

Algoritmen her er [RC4](https://en.wikipedia.org/wiki/RC4), men vi finner ingen [KSA](https://en.wikipedia.org/wiki/RC4#Key-scheduling_algorithm_(KSA)) (Key-scheduling Algorithm) i filen. Det kan virke som om `S` er forhåndsgenerert og vi må dekryptere et buffer direkte med den, dvs. implementere RC4s [PRGA](https://en.wikipedia.org/wiki/RC4#Pseudo-random_generation_algorithm_(PRGA)):

```python
def decrypt(sbox, data):
    i = 0
    j = 0
    for x in range(len(data)):
        i = (i + 1) % 256
        j = (j + sbox[i]) % 256
        sbox[i], sbox[j] = sbox[j], sbox[i]
        k = sbox[(sbox[i] + sbox[j]) % 256]
        buf[x] = ord(buf[x]) ^ k
    return buf
```

Heldigvis for oss er både `S` og bufferet den dekrypterer lett å identifisere basert på hvilket buffer som endres, og hvilket buffer man henter / genererer verdier fra (`unk_404020` og `byte_404060` i screenshot over).

Vi henter ut de aktuelle verdiene, dekrypterer og finner flagget.

Debugger
-------
Som vi ser i avsnittet over dekrypteres flagget rett før det sendes. Vi kan sette breakpoints på et punkt der flagget er dekryptert, som f.eks. på kallet til `HttpSendRequestA`. Gjør vi det ser vi at flagget ligger pent dekryptert i minne.

Den enkleste måten å løse oppgaven på er kombinasjonen av å se litt på assembly / dekompilert kode for å identifisere interessante buffere, og en debugger for å hente ut dekryptert data.

Fange opp nettverkstrafikk
-------
Når filen kjøres vil den forsøke å koble opp mot domenet `financials-quarterly.lab` på port 80 og kjøre en `POST` til URLen `/reports/2020/Q4/aggregate_financials.html`. Dersom man fanger opp trafikken (f.eks. ed bruk av Inetsim el.l.) ser man at data i `POST` er flagget:

`EGG{4aad279bb0a28b80cc32a61cd35b1121}`.
