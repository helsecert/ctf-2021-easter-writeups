package main

import "fmt"

var N = 69
var X = 71
var J = 71
var F = 123
var Q = 51
var G = 55
var H = 95
var Y = 102
var D = 108
var O = 64
var T = 103
var R = 103
var A = 95
var V = 49
var E = 95
var K = 103
var Z = 48
var M = 125
var FLAGG = [...]int{ N,X,J,F,Q,G,H,Y,D,O,T,R,A,V,E,K,Z,M }
var LEET = 1337

func main() {
	fmt.Println("Her er flagget:")
	if LEET == 1336 {
		for i := 0; i < len(FLAGG); i++ {
			fmt.Print(string(FLAGG[i]))
		}
		fmt.Print("\n")
	} else {
                fmt.Printf("Hmm.. flagget er kanskje glemt?\n")
	}
}
