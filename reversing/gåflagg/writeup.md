En enklere RE oppgave. En if-block i main gjør at flagget ikke printes.
Kan hentes ut på flere måter:
- Patche if-en med en NOP
- Hente ut `main.FLAGG` direkte

Bruker `radare2` til dette.
```
r2 -d ./gåflagg
[0x00464820]> aaa    # denne tar litt tid
...
[0x00464820]> s sym.main.main                                                                                          
[0x0049aea0]> pdf
...
│      │╎   0x0049af2a      48813dd3710a.  cmp qword [obj.main.LEET], 0x538 ; [0x542108:8]=0x539 ; "9\x05"
│     ┌───< 0x0049af35      0f851a010000   jne 0x49b055
│     ││╎   0x0049af3b      31c0           xor eax, eax
...
│   ╎│││╎   0x0049af4a      488d0d0fa80a.  lea rcx, obj.main.FLAGG     ; 0x545760 ; "E"
...
```

# Patching
```
[0x0049aea0]> s 0x0049af35
[0x0049aea0]> pd 2
│     ┌───< 0x0049af35      0f851a010000   jne 0x49b055
│     ││╎   0x0049af3b      31c0           xor eax, eax
[0x0049af35]> wao nop
[0x0049af35]> pd 7
│           0x0049af35      90             nop
│           0x0049af36      90             nop
│           0x0049af37      90             nop
│           0x0049af38      90             nop
│           0x0049af39      90             nop
│           0x0049af3a      90             nop
│           0x0049af3b      31c0           xor eax, eax
[0x00464c9a]> dc
(2498243) Created thread 2498341
Her er flagget:
EGG{37_fl@gg_1_g0}
```


# Dumpe main.FLAGG
```
[0x00464820]> px @ obj.main.FLAGG
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x00545120  4500 0000 0000 0000 4700 0000 0000 0000  E.......G.......
0x00545130  4700 0000 0000 0000 7b00 0000 0000 0000  G.......{.......
0x00545140  3300 0000 0000 0000 3700 0000 0000 0000  3.......7.......
0x00545150  5f00 0000 0000 0000 6600 0000 0000 0000  _.......f.......
0x00545160  6c00 0000 0000 0000 4000 0000 0000 0000  l.......@.......
0x00545170  6700 0000 0000 0000 6700 0000 0000 0000  g.......g.......
0x00545180  5f00 0000 0000 0000 3100 0000 0000 0000  _.......1.......
0x00545190  5f00 0000 0000 0000 6700 0000 0000 0000  _.......g.......
0x005451a0  3000 0000 0000 0000 7d00 0000 0000 0000  0.......}.......
0x005451b0  0000 0000 0000 0000 0000 0000 0000 0000  ................
```
