#!/bin/bash
rm src 2>/dev/null
rm gåflagg 2>/dev/null

docker build --no-cache -t goflag .
docker container create --name temp goflag
docker container cp temp:/out/src ./
docker container rm temp

mv src gåflagg
./gåflagg
