import random
import string

letters = list(string.ascii_uppercase)
random.shuffle(letters)

v = []
l = []
for i, b in enumerate(b"EGG{37_fl@gg_1_g0}"):
    v.append(f"var {letters[i]} = {b}")
    l.append(letters[i])

for x in v:
    print(x)

print("var FLAGG = [...]int{", ",".join(l), "}")
