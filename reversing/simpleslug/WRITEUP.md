SIMPLE SLUG
===========

Oppgaven er rimelig enkel - flagget ligger i klartekst i filen man får utlevert. Enkleste måten å finne flagget på er å kjøre `strings` på filen og se etter / grep'e etter `EGG`.

```
$ strings simple_slug.exe | grep EGG
EGG{f310fa99cb53a28c80f1ca556f21aaba}
```