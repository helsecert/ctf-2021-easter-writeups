NICKELPUNCH
===========

Flagget ligger kryptert i en utlevert PE32+-fil.


Statisk
-------

Avhengig av hvilket verktøy man bruker risikerer man at verktøyet ikke korrekt identifiserer `main`-funksjonen i filen, men dumper deg rett til entry point som er kode lenket inn av MinGW: [`crt0`](https://en.wikipedia.org/wiki/Crt0).

For denne (og de andre PE32+-filene i CTFen) kan vi gå til det siste funksjonkallet i den andre funksjonen som kalles fra entry point for å finne `main`-funksjonen.

Alternativt kan vi jobbe bakover - se på interessante imports i `IAT`. Her ser vi flere funksjoner vi kan bruke for å jobbe oss bakover til mer spennende deler av koden, som for eksempel `InternetOpenA`, `HttpSendRequestA`, `GlobalMemoryStatusEx` og `GetDiskFreeSpace`. Dersom vi begynner å grave rundt kall til funksjonene som importeres fra `Wininet.dll` ser vi:

1) Funksjonskallene ligger i samme funksjon (vi kan kalle denne "nettverksfuksjonen").
2) Interessante verdier (hostnavn, URL) er argumenter til denne funksjonen.

Dersom vi ser på funksjonen som kaller nettverksfunksjonen har vi funnet stedet der argumentene til nettverksfunksjonen bygges opp.

Dersom vi følger argumentene oppover i disassembly ser vi at de kommer fra tre funksjonskall til samme funksjon (`sub_401730`), men med litt forskjellige parametere - ett parameter som kommer fra en tidligere funksjon (`sub_401800`) og en tallkonstant (`0xCC`, `0xC1`, `0xC2`):

![Kall til 0x401800](401800.png)
![kall til 0x401730](401730.png)

I `sub_401800` finner vi en dekrypteringrutine - RC4, men akkurat som i `ClearCaracal` finner vi ingen `KSA`, kun `PRGA` og dekryptering. `S` ligger forhåndsgenerert i filen og brukes for dekryptering:

```python
def decrypt(sbox, data):
    i = 0
    j = 0
    for x in range(len(data)):
        i = (i + 1) % 256
        j = (j + sbox[i]) % 256
        sbox[i], sbox[j] = sbox[j], sbox[i]
        k = sbox[(sbox[i] + sbox[j]) % 256]
        buf[x] = ord(buf[x]) ^ k
    return buf
```

Ved å gå bakover fra dekrypteringsloopen i `sub_401800` får vi identifisert de to bufrene som brukes (`S` og data som skal dekrypteres), kopiere ut disse og dekryptere selv:

![Bufrene, navngitt](decrypt.png)

Bufferet vi dekrypterer ser ut til å være en konfigurasjonstruktur som inneholder flagget. Flagget kopieres aldri ut og bufferet overskrives med null-bytes når det som trengs er hentet ut.

Debugger
-------
Den enkleste måten å finne flagget på er å identifisere interessant kode i en disassembler / decompiler og bruke en debugger for å se på hva som dekrypteres. Merk at denne filen på noen steder sjekker både tilgjengelig minne, diskplass og om `IsBeingDebugged` returnerer true. Hvis en av disse sjekkene feiler, kalles `ExitProcess`.