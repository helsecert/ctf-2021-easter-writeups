#!/bin/bash

cp ../flagg.txt ./
bash boil_files.sh

sudo docker build --no-cache -t skallsjokk .
#sudo docker build -t skallsjokk .

# local test
docker rm -f skallsjokk
sudo docker run -dit --rm -p 8100:8100 --name skallsjokk skallsjokk

# test
sleep 2
bash test.sh
