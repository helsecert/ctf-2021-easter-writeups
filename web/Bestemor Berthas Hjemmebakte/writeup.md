Oppgaven er løselig relativt raskt, men krever litt leting blant en del rød sild.

1. Ta en nærmere titt på personvern-banneret nederst på siden, Her har man i tillegg til "Godta" knappen også en lenke blant teksten "Trykk [her](#) for å lese mer.". Denne leder til en modal.
2. Når man har modalen åpen scroller man ned til bunnen, hvor man har en "Flag" checkbox. Huk av denne.
    * "Ok" knappen sender en POST request til `/cookieconsent` dersom "Flag" er huket av. 
3. Hent deretter ut flagget som settes som en cookie. Flagget kan leses i DevTools under `Application>Cookies` eller `Network>cookieconsent response`
