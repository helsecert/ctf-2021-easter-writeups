Norsk Helsenett SF ble formelt stiftet 1. juli 2009 av Helse- og omsorgsdepartementet (HOD). Flagget er `EGG{2009}`

Dette finner man mange plasser, eksempelvis:
  https://nhn.no/media/1171/arsrapport-nhn-2009.pdf
  https://www.nhn.no/om-oss/vaar-historie/
  https://no.wikipedia.org/wiki/Norsk_Helsenett
