Riktig svar her var 239 milliarder.

Riktig svar finner vi på regjeringen.no:
  https://www.regjeringen.no/no/dokumenter/prop.-1-s-20202021/id2768429/?ch=1 

I `Kapittel 2 Profilen i budsjettforslaget` finner vi `Helse- og omsorgsdepartementets samlede budsjettforslag er om lag 239,7 mrd. kroner, jf. tabell 2.1`. Avrundet nedover blir det 239 milliarder.

På denne oppgaven var det sykt mange forsøk på alt mulig rart, både med og uten komma. Viktig å lese oppgaveteksten!
