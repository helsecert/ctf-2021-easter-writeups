Det er nok mange ulike kilder for å finne denne informasjonen. Den beste er å lese oppdragsbrevet fra Helse- og omsorgsdepartementet (HOD) til Norsk HElsenett SF:
  https://www.nhn.no/media/3435/nhn-oppdragsbrev-2021.pdf

Her finner vi en tabell med 3 poster (i 1000kr), altså 151 633 000kr + 504 884 000kr + 1 000 000kr = 657 517 000 kr.

Svaret var ønsket på "nærmeste millioner kroner, avrundet nedover". Riktig flagg er `EGG{657}`.
