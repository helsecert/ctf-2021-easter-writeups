Siden vår byråkratiske stat har blitt digitalisert ligger det mange offentlige dokumenter på nett. Her kan små og store påskekyllinger lese om hva de litt eldre og mer etablerte 🐔 har bestemt for allmennheten.

Hvor stor var den **totale** tildelingen fra Helse- og omsorgsdepartementet til Norsk Helsenett SF for 2021?

Flagget er `EGG{<nærmeste millioner kroner, avrundet nedover>}`
