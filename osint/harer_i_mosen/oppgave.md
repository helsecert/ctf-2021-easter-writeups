# Oppgave 1: Hvem?
En alarm i sensornettet har trigget på mistenksomme filoverføringer og en av påskekyllingene har fanget opp tilhørende nettverkstrafikk. Påskekyllingen ser at det er en ukjent enhet som har overført bildefiler, men sliter med  å finne ut hvem som eier denne. Kan du hjelpe til med å finne ut hvem som står bak?

* *Hint: Last ned filene som har blitt sendt over nettverket. Finnes det noe data her som kan indikere hvem som står bak?*

Flagg: `EGG{H4R4LD_H4R3PU5}`


# Oppgave 2: Hvor?
Ved nærmere inspeksjon virker som innholdet i filoverføringen kan ha lokasjonsdata. Påskekyllingen ser trist ut og forteller at han var syk den dagen de hadde geografi på kyllingskolen. Klarer du å finne navnet på bygget som bildene ble tatt fra? (Lever svaret på formatet EGG{svar})

* *Hint 1: Kanskje lokasjonsdataene ikke gjenspeiler hvor bildene er tatt fra, men noe man ser i bildene?*
* *Hint 2: Basert på klokkeslettene kan det se ut som bildene har blitt tatt i arbeidstid. Hva kan dette bety?*

Flagg: `EGG{Abels Hus}` (case insensitive)



# Oppgave 3: Hva?
En av påskekyllingene ser på bildene og reagerer plutselig. "Her er det noe muffens, men jeg klarer ikke helt å sette fingeren på det." Han anbefaler at du analyserer bildene nærmere for å se om det er noe som skiller seg ut. En annen påskekylling nikker og legger til at vi enda ikke vet hvor bildene har blitt sendt. De lurer fælt på hva som egentlig har skjedd her. Kan du se om du finner noe som kan peke på hvor bildene har blitt lastet opp?

* *Hint 1: Er det noe som skiller seg ut i bildenes metadata?*
* *Hint 2: Er det mulig å gjemme ting i bildefiler?*
* *Hint 3: Pcap-filen inneholder noe kryptert TLS-trafikk. Kan det være mulig å dekryptere denne?*

Flagg: `EGG{H4R3BL0GG3N}`
