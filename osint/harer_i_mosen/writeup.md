# Oppgave 1: Hvem?
1. Åpne pcap-filen i Wireshark. Gå til File -> Export objects -> HTTP -> Lagre de 3 bildefilene som har blitt overført i klartekst.
2. Kjør exiftool på bildene. Se etter author -> `EGG{H4R4LD H4R3PU5}`.
* Alternativt kan man også se dette i selve pcap-en -> Follow -> HTTP stream -> `<pdf:Author>EGG{H4R4LD H4R3PU5}</pdf:Author>`.

Flagg: `EGG{H4R4LD H4R3PU5}`


# Oppgave 2: Hvor?
1. Se etter koordinater i exifdata -> Sett disse inn i Google maps.
    * (Her må man erstatte "deg" med symbolet `°` for at Google maps skal akseptere koordinatene)
    * Koordinatene peker til Gråkallen radarhode, Moholt studentby og Nidarosdomen.
2. Se etter Focus Distance i exifdata. Denne er oppgitt i mm. 
3. Bruk distansen og koordinatene til å tegne en sirkel med radius for de tre lokasjonene og finn krysspunktet, som er kontoret hvor HelseCERT holder til.
4. Ved å se på Street View utenfor kontoret kan man se at det står "Abels Hus" på bygget.
* Alternativt kan man se på jobbannonsen og lese der at vi holder til på "Abels Hus".

Flagg: `EGG{Abels Hus}` (case insensitive)


# Oppgave 3: Hva?
1. Se etter forskjeller i exifdata til bildene -> ett av bildene mangler "Nothing to see here..."
2. Kjør `[steghide exctract -sf <bilde.jpg>]` på dette bildet -> ikke skriv inn noe passord -> man får ut ThisLooksSuspicios.key (hinter til at dette er en TLS-nøkkel).
3. Nøkkelen importeres i pcap-filen: File -> Edit -> Preferences -> Protocols -> TLS -> RSA keys list -> Edit
```
    IP address: 172.18.0.3
    Port: 443
    Protocol: http
    Key file: ThisLooksSuspicios.key
```
4. TLS trafikken dekrypteres og man kan se hva som har blitt overført før bildene
5. Gå til packet 57 -> Follow -> TLS Stream -> man får ut bloggteksten og her står navnet på bloggen -> `EGG{H4R3BL0GG3N}`

Flagg: `EGG{H4R3BL0GG3N}`
