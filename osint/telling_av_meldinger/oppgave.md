I Norsk Helsenett er det mange ivrige påskekyllinger som liker å telle meldinger. Faktisk er de så begeistret for meldingstelling at de har laget en egen tjeneste for dette, åpent tilgjengelig for alle.

Totalt hvor mange eResept-meldinger (for hele landet) ble utvekslet mellom ulike aktører over helsenettet for hele 2020?

Flagget er `EGG{<antall millioner, avrundet nedover>}`
