Her er det flere pekepinner i retning `meldingsteller`. Et kjapt googlesøk på `melding teller` med/uten nhn eller norsk helsenett gjør at man _bør_ finne https://meldingsteller.nhn.no/.

Meldingsteller er en offentlig tjeneste for å lage rapporter på tallgrunnlag for antall elektroniske meldinger som sender mellom aktører på helsenettet.

Man går på `Lag rapport`:
- velger `Periode=01.01.2020 til 31.12.2020`
- velger `Tidsinndeling=Total`
- velger `Meldingskategori=eResept-meldinger` (får en popup-modal for å velge meldingskategoriene)
- trykker `lag filter`
- trykker `forhåndsvis rapport`

Her får man svaret `Totalt antall meldinger = 102 654 980`. Oppgaven ønsker "antall milliner, avrundet nedover". Riktig flagg er `EGG{102}`.
