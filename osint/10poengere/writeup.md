Det var 5 oppgaver som gav 10 poeng:
- Discord
- Twitter
- Oppdraget
- Nasjonale e-helseløsninger
- LinkedIn

Discord, Twitter og LinkedIn løses ved å gå på disse sosiale mediene og hente flagget. Flaggene var:
```
Discord:  EGG{lykke_til!} 
Twitter:  EGG{helsectf_er_g0y}
LinkedIn: EGG_helsectf -> EGG{helsectf}
```

Oppdraget til HelseCERT er å oppdage, forebygge og håndtere `cyberangrep`. Altså `EGG{cyberangrep}`.

Norsk Helsenett SF utvikler, forvalter og drifter mange nasjonale e-helseløsninger som `helsenorge, kjernejournal e-resept, grunndata` med fler. Riktig svar var en av disse i `EGG{..}`, f.eks. `EGG{kjernejournal}`.
